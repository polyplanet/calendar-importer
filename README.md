Calendar Importer
=================

Dependency Plugins:
-------------------
 - AdvancedCustomFields Pro
 - ACF OpenStreetMap Field
 - ACF QuickEdit Fields (optional)



Test calendar URLs:
-------------------
 - https://www.schulferien.eu/downloads/ical4.php?land=HH&type=0&year=2018
 - https://p51-calendars.icloud.com/published/2/hFclNDj2vdM5JoodT2o7DyLSotL7i_o8q3CqelNy6vvBHqhaE7m1YaEOLLicvf6n8_uMYSus3h8uJAtpdx2JzHAJmqLA2W5tWnHL2p-6RP0
 - https://calendar.google.com/calendar/ical/tjbrt9lhh47p8tbridjj9be964%40group.calendar.google.com/public/basic.ics
 - https://calendar.google.com/calendar/ical/e4svhgdllon4t6upe8f2urcupg%40group.calendar.google.com/public/basic.ics
 - https://www.mozilla.org/media/caldata/GermanHolidays.ics
 - Events with Locations  
 https://owncloud.flyingletters.net/remote.php/dav/public-calendars/Z8OUT289Z4KAZPCF?export
 - BSB Nexcloud Test  
 https://owncloud.flyingletters.net/remote.php/dav/public-calendars/5N40CUPGOBGBTEVX?export
 - https://outlook.live.com/owa/calendar/00000000-0000-0000-0000-000000000000/587e8c03-c03c-4afd-b4f6-afe580347c45/cid-734ADF9F881E7723/calendar.ics

TODO:
-----

ical-Libs:
----------
 - Parse iCal: https://github.com/zcontent/icalendar
 - Recurring Events: https://github.com/rlanvin/php-rrule


Tweaks
------
0.2.30
# fixing broken ml-slider slideshows
```shell
wp eval "foreach ( get_posts(['post_type'=>'ml-slider','post_status'=>'any','posts_per_page'=>-1,'fields'=>'ids']) as \$id ) if ( ! term_exists(strval(\$id),'ml-slider') ) wp_insert_term(strval(\$id),'ml-slider');" --url=<URL>
```