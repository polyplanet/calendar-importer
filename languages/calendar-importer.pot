# Copyright (C) 2024 POLYPLANET
# This file is distributed under the GPL3.
msgid ""
msgstr ""
"Project-Id-Version: Calendar Importer 0.3.19\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/calendar-importer\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2024-09-18T15:31:49+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.11.0\n"
"X-Domain: calendar-importer\n"

#. Plugin Name of the plugin
#: index.php
msgid "Calendar Importer"
msgstr ""

#. Plugin URI of the plugin
#: index.php
msgid "http://wordpress.org/"
msgstr ""

#. Description of the plugin
#: index.php
msgid "Import calendar feeds and show them on your website."
msgstr ""

#. Author of the plugin
#: index.php
msgid "POLYPLANET"
msgstr ""

#: include/api/template-tags.php:91
msgid "All day"
msgstr ""

#. translators: %d more dates
#: include/api/template-tags.php:121
#: include/CalendarImporter/PostType/PostTypeEvent.php:236
#: include/templates/calendar-importer/calendar-month.php:136
msgid "%d more"
msgid_plural "%d more"
msgstr[0] ""
msgstr[1] ""

#. translators: 1 datetime from, 2 datetime to
#: include/api/template-tags.php:369
#: include/api/template-tags.php:401
#: include/templates/calendar-importer/date-long.php:19
msgctxt "datetime from, datetime to"
msgid "%1$s until %2$s"
msgstr ""

#. translators: 1 date, 2: time from 2 time to
#: include/api/template-tags.php:384
msgctxt "date, time from, time until"
msgid "%1$s, %2$s until %3$s"
msgstr ""

#: include/CalendarImporter/ACF/Block/Calendar.php:22
#: include/CalendarImporter/Compat/ACF.php:34
#: include/CalendarImporter/PostType/PostTypeEvent.php:72
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:64
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:86
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:120
#: src/php/json-strings.php:58
msgid "Calendar"
msgstr ""

#: include/CalendarImporter/ACF/Block/Calendar.php:37
msgid "A Calendar"
msgstr ""

#: include/CalendarImporter/ACF/Block/Calendar.php:52
msgid "calendar"
msgstr ""

#: include/CalendarImporter/ACF/Block/Calendar.php:52
msgid "date"
msgstr ""

#: include/CalendarImporter/ACF/Block/Calendar.php:52
msgid "event"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:30
msgid "Red"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:31
msgid "Orange"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:32
msgid "Ochre"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:33
msgid "Light Green"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:34
msgid "Green"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:35
msgid "Blue"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:36
msgid "Purple"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:37
msgid "Pink"
msgstr ""

#: include/CalendarImporter/Admin/Admin.php:79
msgid "Just now"
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:32
msgid "Sync"
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:33
msgid "Last Sync"
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:50
msgid "Synchronize now"
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:63
msgctxt "list time format"
msgid "g:i A"
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:94
msgid "New Subscription"
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:98
#: include/CalendarImporter/PostType/TaxonomyCalendar.php:120
msgid "Calendar Feed URL"
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:106
msgid "Paste the Calender Feed URL. The Calendar must be in the *.ics format."
msgstr ""

#: include/CalendarImporter/Admin/AdminCalendars.php:111
msgid "Subscribe to Calendar"
msgstr ""

#: include/CalendarImporter/Ajax/AjaxHandler.php:155
#: include/PosttypeTermArchive/Ajax/AjaxHandler.php:155
msgid "Nonce invalid"
msgstr ""

#: include/CalendarImporter/Ajax/AjaxHandler.php:163
#: include/PosttypeTermArchive/Ajax/AjaxHandler.php:163
msgid "Insufficient Permission"
msgstr ""

#: include/CalendarImporter/Compat/ACF.php:33
#: src/php/json-strings.php:21
msgid "Calendar Options"
msgstr ""

#. translators: stamen.com
#: include/CalendarImporter/Core/Core.php:44
msgid "Map tiles by <a href=\"%s\">Stamen Design</a>"
msgstr ""

#. translators: OpenStreetMap.org
#: include/CalendarImporter/Core/Core.php:47
msgid "Map Data &copy; <a href=\"%s\">OpenStreetMap</a>"
msgstr ""

#: include/CalendarImporter/Core/Core.php:48
#: include/templates/calendar-importer/part-js-templates.php:9
#: include/templates/calendar-importer/part-js-templates.php:22
msgid "Close"
msgstr ""

#: include/CalendarImporter/Core/Core.php:103
msgid "The plugin <strong>Advanced Custom Fields PRO</strong> is not active."
msgstr ""

#: include/CalendarImporter/Core/Core.php:105
msgid " The Calandar Plugin will not work without it. "
msgstr ""

#: include/CalendarImporter/Core/Renderer.php:182
msgid "Previous"
msgstr ""

#: include/CalendarImporter/Core/Renderer.php:186
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:78
msgid "Next"
msgstr ""

#. translators: number of seconds
#: include/CalendarImporter/Cron/Cron.php:178
msgid "Every %d seconds"
msgstr ""

#: include/CalendarImporter/iCal/Parser.php:71
msgid "Unkonwon error"
msgstr ""

#: include/CalendarImporter/iCal/Parser.php:99
msgid "No calendar data"
msgstr ""

#: include/CalendarImporter/iCal/zParser.php:36
msgid "Error while parsing Calendar Data"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:78
msgid "Dates"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:177
msgid "Next Date"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:295
msgid "Filter by %s"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:362
msgctxt "Post Type General Name"
msgid "Events"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:363
msgctxt "Post Type Singular Name"
msgid "Event"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:364
#: include/CalendarImporter/PostType/TaxonomyCalendar.php:653
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:66
#: src/php/json-strings.php:30
msgid "Calendars"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:365
msgid "Parent Event:"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:366
msgid "All Events"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:367
msgid "View Event"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:368
msgid "Add New Event"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:369
msgid "Add New"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:370
msgid "Edit Event"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:371
msgid "Update Event"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:372
msgid "Search Event"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:373
msgid "Not found"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:374
msgid "Not found in Trash"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:378
msgid "Events"
msgstr ""

#: include/CalendarImporter/PostType/PostTypeEvent.php:379
msgid "Event Description"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:179
msgid "No Calendar with ID %d"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:204
msgid "No Calendar URL specified"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:643
msgctxt "taxonomy general name"
msgid "Calendars"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:644
msgctxt "taxonomy singular name"
msgid "Calendar"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:645
msgid "Search Calendars"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:646
msgid "All Calendars"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:647
msgid "Parent Calendar"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:648
msgid "Parent Calendar:"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:649
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:63
msgid "Edit Calendar"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:650
msgid "Update Calendar"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:651
msgid "Add New Calendar"
msgstr ""

#: include/CalendarImporter/PostType/TaxonomyCalendar.php:652
msgid "New Calendar"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:62
msgid "Insert Calendar"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:68
msgid "Settings"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:69
#: src/php/json-strings.php:56
msgid "Display"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:71
#: src/php/json-strings.php:33
msgid "Period"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:72
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:114
#: src/php/json-strings.php:34
msgid "Day"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:73
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:98
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:115
#: src/php/json-strings.php:36
msgid "Week"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:74
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:116
msgid "Working Week"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:75
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:101
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:117
#: src/php/json-strings.php:37
msgid "Month"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:77
msgid "This"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:79
msgid "In"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:80
#: src/php/json-strings.php:39
msgid "From"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:81
#: src/php/json-strings.php:44
msgid "To"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:83
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:95
msgid "Date"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:85
#: src/php/json-strings.php:57
msgid "Layout"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:87
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:121
#: src/php/json-strings.php:59
msgid "List"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:88
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:122
#: src/php/json-strings.php:60
msgid "Tiles"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:90
#: src/php/json-strings.php:64
msgid "List Size"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:91
#: src/php/json-strings.php:61
msgid "Tile Size"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:92
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:129
#: src/php/json-strings.php:62
msgid "Small"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:93
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:130
#: src/php/json-strings.php:63
msgid "Large"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:96
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:136
msgid "In ... Days"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:99
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:142
msgid "In ... Weeks"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:102
#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:148
msgid "In ... Months"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:104
#: src/php/json-strings.php:67
msgid "Colors"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:106
#: src/php/json-strings.php:70
msgid "Show Single Event"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:108
#: src/php/json-strings.php:73
msgid "Navigation"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:125
#: src/php/json-strings.php:65
msgid "Normal"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:126
#: src/php/json-strings.php:66
msgid "Compact"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:134
#: src/php/json-strings.php:50
msgid "Today"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:135
#: src/php/json-strings.php:51
msgid "Tomorrow"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:137
msgid "Select Date"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:140
#: src/php/json-strings.php:46
msgid "This Week"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:141
#: src/php/json-strings.php:47
msgid "Next Week"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:143
msgid "Select Week"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:146
#: src/php/json-strings.php:40
msgid "This Month"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:147
#: src/php/json-strings.php:41
msgid "Next Month"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:149
msgid "Select Month"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:152
#: src/php/json-strings.php:69
msgid "Dark"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:153
#: src/php/json-strings.php:68
msgid "Light"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:156
#: src/php/json-strings.php:71
msgid "Permalink"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:157
msgid "Inline"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:158
#: src/php/json-strings.php:72
msgid "Lightbox"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:161
#: src/php/json-strings.php:74
msgid "Scroll"
msgstr ""

#: include/CalendarImporter/Shortcode/Mce/MceCalendar.php:162
msgid "Turn Pages"
msgstr ""

#: include/CalendarImporter/Widget/CalendarWidget.php:16
msgid "Shows Events from your Calendar"
msgstr ""

#: include/CalendarImporter/Widget/CalendarWidget.php:18
msgid "Show Events"
msgstr ""

#: include/templates/calendar-importer/date-short.php:54
msgctxt "until"
msgid "–"
msgstr ""

#: include/templates/calendar-importer/event-calendar-month.php:36
#: include/templates/calendar-importer/event-calendar-month.php:37
msgid "All Day Event"
msgstr ""

#: include/templates/calendar-importer/part-recurring.php:18
msgid "Upcoming Recurrences"
msgstr ""

#: include/templates/calendar-importer/part-sharing.php:35
msgid "Share via %s"
msgstr ""

#. translators: 1: time from, 2: time to
#: include/templates/calendar-importer/time-long.php:12
msgctxt "time from, time until"
msgid "%1$s until %2$s o’clock"
msgstr ""

#: src/php/json-strings.php:4
msgid "Calendar Details"
msgstr ""

#: src/php/json-strings.php:5
msgid "Color"
msgstr ""

#: src/php/json-strings.php:6
msgid "Refresh every"
msgstr ""

#: src/php/json-strings.php:7
msgid "Manually"
msgstr ""

#: src/php/json-strings.php:8
msgid "15 Minutes"
msgstr ""

#: src/php/json-strings.php:9
msgid "30 Minutes"
msgstr ""

#: src/php/json-strings.php:10
msgid "1 Hour"
msgstr ""

#: src/php/json-strings.php:11
msgid "6 Hours"
msgstr ""

#: src/php/json-strings.php:12
msgid "12 Hours"
msgstr ""

#: src/php/json-strings.php:13
msgid "1 Day"
msgstr ""

#: src/php/json-strings.php:14
msgid "1 Week"
msgstr ""

#: src/php/json-strings.php:15
msgid "Keep Past Events"
msgstr ""

#: src/php/json-strings.php:16
msgid "If checked Past Events will not be deleted"
msgstr ""

#: src/php/json-strings.php:17
msgid "Event Details"
msgstr ""

#: src/php/json-strings.php:18
msgid "URL"
msgstr ""

#: src/php/json-strings.php:19
msgid "Location"
msgstr ""

#: src/php/json-strings.php:20
msgid "Location Map"
msgstr ""

#: src/php/json-strings.php:22
msgid "Sharing"
msgstr ""

#: src/php/json-strings.php:23
msgid "Facebook"
msgstr ""

#: src/php/json-strings.php:24
msgid "Twitter"
msgstr ""

#: src/php/json-strings.php:25
msgid "LinkedIn"
msgstr ""

#: src/php/json-strings.php:26
msgid "Google Plus"
msgstr ""

#: src/php/json-strings.php:27
msgid "EMail"
msgstr ""

#: src/php/json-strings.php:28
msgid "Show Header Image on Single Event"
msgstr ""

#: src/php/json-strings.php:29
msgid "Calendar Widget"
msgstr ""

#: src/php/json-strings.php:31
msgid "Number of Events"
msgstr ""

#: src/php/json-strings.php:32
msgid "Entries"
msgstr ""

#: src/php/json-strings.php:35
msgid "Working week"
msgstr ""

#: src/php/json-strings.php:38
msgid "Months"
msgstr ""

#: src/php/json-strings.php:42
msgid "In … Months"
msgstr ""

#: src/php/json-strings.php:43
msgid "From Month"
msgstr ""

#: src/php/json-strings.php:45
msgid "To Month"
msgstr ""

#: src/php/json-strings.php:48
msgid "In … Weeks"
msgstr ""

#: src/php/json-strings.php:49
msgid "Days"
msgstr ""

#: src/php/json-strings.php:52
msgid "In … days"
msgstr ""

#: src/php/json-strings.php:53
msgid "From Day"
msgstr ""

#: src/php/json-strings.php:54
msgid "In … Days"
msgstr ""

#: src/php/json-strings.php:55
msgid "To Day"
msgstr ""

#: src/php/json-strings.php:75
msgid "Turn"
msgstr ""
