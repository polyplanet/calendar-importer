var autoprefixer	= require('gulp-autoprefixer');
var fs				= require('fs');
var gulp			= require('gulp');
var glob			= require('glob');
var concat			= require('gulp-concat');
var uglify			= require('gulp-uglify');
var sass			= require('gulp-sass')( require('sass') );
var sourcemaps		= require('gulp-sourcemaps');
// var rename			= require('gulp-rename');
// var importer		= require('gulp-fontello-import');
// var replace			= require('gulp-replace');
// var clean			= require('gulp-clean');



var path = {
	tmp		: './src/tmp/',
};

var is_dev = true;

// Fontello: settings object
var config = {
	fontello: {
		sourceDir			: './src/icons/',
		sourceConfigFile	: 'config.json',
		configFile			: 'config-generated.json',
	},
	fonts: {
		fontDest			: './fonts/',
		scssDest			: './src/scss/fonts/',
	}
};

var fontelloFn = {
	config	: function(cb) {


		console.log('run fontello config')
		// move config to tmp
		return gulp.src( config.fontello.sourceDir + config.fontello.sourceConfigFile )
			.pipe( rename( config.fontello.configFile ) )
			.pipe( gulp.dest( config.path.tmp ) );
	},
	import	: function( cb ) {
		console.log('run fontello import')
		// scan icon dir
		return importer.importSvg({
			config:	config.path.tmp + config.fontello.configFile,
			svgsrc:	config.fontello.sourceDir,
		},cb);
	},
	generate	: function(cb) {
		console.log('run fontello generate')
	    return importer.getFont({
			host:	'http://fontello.com',
			config:	config.path.tmp + config.fontello.configFile,
			font:	config.fonts.fontDest + 'fontello/',
			css:	config.path.tmp,
	    },cb);
	},
	scss : function(cb) {

		var fontConfig	= require( config.fontello.sourceDir + config.fontello.sourceConfigFile ),
			fontName	= fontConfig.name,
			prefix		= fontConfig.css_prefix_text; // icon-
		// write fontello vars
		fs.writeFileSync(
			config.fonts.scssDest + '_fontello-vars.scss',
			"" +
			"$fontello-cachebuster: '" + (new Date()).getTime().toString(16) + "';\n" +
			"$fontello-fontname: '"+fontName+"';\n"
		)
		// generate fontello codes
		var s = new RegExp( "\."+prefix+"([a-z0-9-_]+):before \{ content: '\\\\([a-f0-9]+)'; \}", 'g' );
		var r = "  $1: '\\$2',";

		return gulp.src( config.path.tmp + fontName +'-codes.css')
			.pipe( replace(s, r) )
			.pipe( gap.prependText('$icons: (') )
			.pipe( gap.appendText(');') )
			.pipe( rename('_fontello-codes.scss') )
			.pipe( gulp.dest( config.fonts.scssDest ) );
	},
	clean : function() {
		return gulp.src( config.path.tmp + '*.*', { read: false } )
			.pipe( clean() );
	}
}

gulp.task('fontello', gulp.series( fontelloFn.config, fontelloFn.import, fontelloFn.generate, fontelloFn.scss, fontelloFn.clean ) );



// 
// 
// gulp.task('acf-strings',function(cb){
// 	glob('acf-json/*.json',{},function(er,files){
// 		var strings = [], i, f, php;
// 
// 		for ( f in files ) {
// 			console.log(files[f])
// 			var data = require('./'+files[f]);
// 			strings = strings.concat( extract_strings( data, false ) );
// 
// 		}
// 		// unique array
// 		strings = strings.filter(function(value, index, self) {
// 			return self.indexOf(value) === index;
// 		});
// 
// 		php = '<?php\n';
// 		php += 'exit(0);\n';
// 		php += '\n';
// 
// 		for ( i=0; i < strings.length; i++ ) {
// 			php += "__( '"+strings[i]+"', 'calendar-importer');";
// 			php += '\n';
// 		}
// 		fs.writeFile('./src/php/acf-strings.php', php, function(){} );
// 
// 	});
// 	cb();
// });
// 
// function extract_strings( obj, all_keys ) {
// 	var strings = [];
// 	for ( var s in obj ) {
// 		switch (s) {
// 			case 'title':
// 			case 'description':
// 			case 'label':
// 			case 'instructions':
// 			case 'prepend':
// 			case 'append':
// 			case 'message':
// 				if ( obj[s] !== '' ) {
// 					strings.push( obj[s] );
// 				}
// 				break;
// 			default:
// 				if ( 'object' === typeof obj[s] ) {
// 					// numeric s or key is choices, fields, sub_fields
// 					if ( !!s.match(/[0-9]+/) || -1 !== ['choices','fields','sub_fields'].indexOf(s) ) {
// 						strings = strings.concat( extract_strings( obj[s], s === 'choices' ) );
// 					}
// 				} else { // scalar
// 					if ( all_keys ) {
// 						if ( obj[s] !== '' ) {
// 							strings.push( obj[s] );
// 						}
// 					}
// 				}
// 				break;
// 		}
// 	}
// 	return strings;
// }
// 
// 


function do_scss( src, dest ) {
	var g;
	g = gulp.src( src );
	if ( is_dev ){
		 g = g.pipe( sourcemaps.init() )
	};
	g = g.pipe( sass( { outputStyle: 'compressed' } ).on('error', sass.logError) )
	if ( is_dev ) {
		g = g.pipe( sourcemaps.write() );
	}
	g = g.pipe( gulp.dest( dest ) );
	return g;
}

function do_js( src, destname, dest ) {
	var g;
	g = gulp.src( src );
	if ( is_dev ) {
		g = g.pipe( sourcemaps.init() );
	}
	g = g.pipe( concat(destname) )
	g = g.pipe( uglify().on('error', console.log ) )		
	if ( is_dev ) {
		g = g.pipe( sourcemaps.write() );
	}
	g = g.pipe( gulp.dest( dest ) );
	return g;
}


gulp.task('styles', function() {
	return do_scss([
			'./src/scss/calendar-importer.scss'
		],
		'./css/'
	);
});


gulp.task( 'scripts', function() {
	return do_js( [
		'./src/js/calendar-importer.js',
		], 'calendar-importer.js', './js/' );

} );


gulp.task('styles:admin:main', function() {
	return do_scss([
			'./src/scss/admin/admin.scss'
		],
		'./css/admin/'
	);
} );

gulp.task('styles:admin:shortcode', function() {
	return do_scss([
			'./src/scss/admin/mce/pp_calendar_shortcode-editor.scss'
		],
		'./css/admin/mce/'
	);
} )

gulp.task('styles:admin:mcetoolbar', function() {
	return do_scss([
			'./src/scss/admin/mce/pp_calendar_shortcode-toolbar.scss'
		],
		'./css/admin/mce/'
	);
} );

gulp.task('styles:admin', gulp.parallel('styles:admin:main','styles:admin:shortcode','styles:admin:mcetoolbar'));


gulp.task('scripts:admin:main', function() {
	return do_js( [
			'./src/js/admin/acf-disabled.js',
			'./src/js/admin/taxonomy-pp_calendar.js',
			'./src/js/admin/pp-more.js',
		], 'admin.js', './js/admin/' );
});
gulp.task('scripts:admin:shortcode', function() {
	//*
	return do_js( [
			'./src/js/admin/mce/datepicker_control.js',
			'./src/js/admin/mce/pp_calendar_shortcode.js',
		], 'pp_calendar_shortcode.js', './js/admin/mce/' );
	/*/
	return gulp.src( [
			'./src/js/admin/mce/datepicker_control.js',
			'./src/js/admin/mce/pp_calendar_shortcode.js',
		] )
		.pipe( sourcemaps.init() )
		.pipe( concat('pp_calendar_shortcode.js') )
		.pipe( uglify().on('error', console.log ) )
		.pipe( sourcemaps.write() )
		.pipe( gulp.dest( './js/admin/mce/' ) );

	//*/
});

gulp.task('scripts:admin', gulp.parallel('scripts:admin:main','scripts:admin:shortcode') );

gulp.task('_set_prod',function(cb) {
	is_dev = false;
	cb();
} )

gulp.task( 'compile', gulp.parallel('styles','styles:admin','scripts','scripts:admin') );

gulp.task( 'build', gulp.series( '_set_prod', 'compile' ) );



gulp.task('watch', function() {
	// place code for your default task here
	gulp.watch('./src/**/*.scss',gulp.parallel('styles','styles:admin'));
	gulp.watch('./src/**/*.js',gulp.parallel('scripts','scripts:admin'));
	// gulp.watch('./acf-json/**/*.json',gulp.parallel('acf-strings'));
});
gulp.task( 'dev', gulp.series( 'compile', 'watch' ) );
gulp.task('default', gulp.series('compile','watch'));
