(function($){
	var l10n = calendar_importer_admin.l10n,
		colors = calendar_importer_admin.colors;

	/**
	 *	ACF disabled fields
	 */
	function filter_date_picker_args( args, $field ){
		if ($field.find('.acf-input > .disabled').length ) {
			args.disabled = true;
		}
		return args;
	}
	function color_picker_args( args, $field ) {
//		console.log($field.attr('data-key').indexOf('field_pp_'),colors);
		if ( $field.attr('data-key').indexOf('field_pp_') !== -1 ) {
			args.palettes = colors;
		}
		return args;
	}
	$(document).on('click focus','[type="checkbox"].disabled, .acf-google-map.disabled',function(e){
		e.preventDefault();
		e.target.blur();
		return false;
	});


	acf.add_action( 'ready_field/type=taxonomy', function(mao,marker,$field){
		$('.disabled .acf-taxonomy-field [type="checkbox"], .disabled .acf-taxonomy-field [type="radio"]').each(function(){

			$(this).prop('disabled',true);

		})
	});

	//
	acf.add_filter('date_picker_args', filter_date_picker_args );
	acf.add_filter('time_picker_args', filter_date_picker_args );
	acf.add_filter('date_time_picker_args', filter_date_picker_args );
	acf.add_filter('color_picker_args', color_picker_args );

	// set map field readonly
	acf.add_action( 'google_map_init', function(mao,marker,$field){
		$field.find('input.search').prop('readonly',true );
	});


})(jQuery);
