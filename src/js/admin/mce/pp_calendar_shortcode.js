(function($,opt){


	tinymce.PluginManager.add( 'pp_calendar_shortcode' , function( editor ) {

		var l10n = opt.l10n,
			options = opt.options,
			cal_id = 0;

		opt._init();
		// http://usefulangle.com/post/30/javascript-get-date-time-with-offset-hours-minutes
		function dateToISO8601( d ) {
			if ( 'string' === typeof d ) {
				d = new Date(d);
			}
			// timezone standard
			var timezone_offset_min = d.getTimezoneOffset(),
				offset_hrs = parseInt(Math.abs(timezone_offset_min/60)),
				offset_min = Math.abs(timezone_offset_min%60),
				timezone_standard;

			if(offset_hrs < 10)
				offset_hrs = '0' + offset_hrs;

			if(offset_min < 10)
				offset_min = '0' + offset_min;

			// Add an opposite sign to the offset
			// If offset is 0, it means timezone is UTC
			if( timezone_offset_min < 0 ) {
				timezone_standard = '+' + offset_hrs + ':' + offset_min;
			} else if( timezone_offset_min > 0 ) {
				timezone_standard = '-' + offset_hrs + ':' + offset_min;
			} else if( timezone_offset_min == 0 ) {
				timezone_standard = 'Z';
			}

			var current_date = d.getDate(),
				current_month = d.getMonth() + 1,
				current_year = d.getFullYear(),
				current_hrs = d.getHours(),
				current_mins = d.getMinutes(),
				current_secs = d.getSeconds(),
				current_datetime;

			// Add 0 before date, month, hrs, mins or secs if they are less than 0
			current_date = current_date < 10 ? '0' + current_date : current_date;
			current_month = current_month < 10 ? '0' + current_month : current_month;
			current_hrs = current_hrs < 10 ? '0' + current_hrs : current_hrs;
			current_mins = current_mins < 10 ? '0' + current_mins : current_mins;
			current_secs = current_secs < 10 ? '0' + current_secs : current_secs;

			// Current datetime
			// String such as 2016-07-16T19:20:30
			current_datetime = current_year + '-' + current_month + '-' + current_date + 'T' + current_hrs + ':' + current_mins + ':' + current_secs;
			return current_datetime + timezone_standard;
		}


		/*
		Convert {key: label, key2: label2, ...} > [ { label:label,value:key }, { label:label2,value:key2 }, ... ]
		*/
		function options_to_mce_options( opt ){
			var mce_opt = [];
			$.each(opt,function(key,label){
				mce_opt.push( { text: label, value: key } )
			})
			return mce_opt;
		}


		function shortcodeReplaceCB( shortcode ) {
			//*
			var id = 'pp-cal-'+ (cal_id++),
				$div = $('<div contenteditable="false" data-pp-colors="light" data-wp-entity="pp_calendar"></div>')
					.append('<div class="pp-loading"></div>')
					.attr( 'id', id )
					.attr( 'data-attrs', JSON.stringify( shortcode.attrs.named ) )
					.attr( 'data-tag', shortcode.tag );

			$.post({
				url: ajaxurl,
				data: $.extend( shortcode.attrs.named, { action: 'pp-calendar' } ),
				success:function(response) {
					$(editor.getDoc())
						.find('#'+id)
						.removeAttr('data-pp-colors')
						.html(response);
				}
			});
			return $('<div></div>').append( $div ).html();
		}

		function restoreShortcodes( content ) {
			//match any image tag with our class and replace it with the shortcode's content and attributes
			var $content = $( '<div>' + content + '</div>' );
			//*
			$content.find('[data-wp-entity="pp_calendar"]').each(function(){
				try {
					$(this).replaceWith( new wp.shortcode({
						tag: $(this).attr('data-tag'),
						attrs: JSON.parse( $(this).attr('data-attrs') ),
						type: 'single'
					}).string() );
				} catch(err){
					console.log(err)
				}
			});
			return $content.html();
		}




		function openDialog( callback, values ) {
			var sel		= editor.selection.getNode(),
				$sel	= $( sel ),
				d = {
					period			: 'week',
					template		: 'list',
					tile_size		: 'small', // small|large
					list_size		: 'normal', // normal|compact
					calendars		: '',

					from_days		: 'today',
					from_day		: new Date(),
					from_in_days	: 1,
					to_days			: 'today',
					to_day			: new Date(),
					to_in_days		: 1,

					from_weeks		: 'today',
					from_week		: new Date(),
					from_in_weeks	: 1,
					to_weeks		: 'today',
					to_week			: new Date(),
					to_in_weeks		: 1,

					from_months		: 'today',
					from_month		: new Date(),
					from_in_months	: 1,
					to_months		: 'today',
					to_month		: new Date(),
					to_in_months	: 1,

					colors			: 'light',
					show_event		: 'inline',
					paging			: 'scroll',
				},
				str, win, dp_fmt,
				win_ui_body = [],
				opt_ui_body = [],
				cal_ui_body = [];

			function win_set_state() {
				var tpl = win.find('#template'),
					pgn = win.find('#paging'),
					tile_size = win.find('#tile_size'),
					list_size = win.find('#list_size'),
					show_event = win.find('#show_event'),
					period = win.find('#period'),
					colors = win.find('#colors'),
					per = period.value(),
					show_evt_options = options_to_mce_options(options.show_event);
				// tile size select
				if ( tpl.value() == 'tiles' ) {
					tile_size.parent().show();
				} else {
					tile_size.parent().hide();
				}

				if ( tpl.value() == 'list' ) {
					list_size.parent().show();
				} else {
					list_size.parent().hide();
				}

				if ( tpl.value() == 'calendar' ) {
					// remove 'inline' options from
					show_evt_options = $.grep(show_evt_options,function(el){
						return el.value !== 'inline';
					});
					if ( show_event.value() === 'inline' ) {
						show_event.value('lightbox');
					}
					pgn.parent().show();
				} else {
					pgn.parent().hide();
				}
				show_event[0].settings.menu = show_event[0].state.data.menu = show_evt_options;
				//show_event[0].

				// period select
				$.each( ['day','week','month'], function(i,el){
					$.each(['from','to'],function(j,p){
						var sel = win.find('#'+p+'_'+el+'s'),
							pick = win.find('#'+p+'_'+el ),
							in_ = win.find('#'+p+'_in_'+el+'s');

						if ( per.indexOf(el) !== -1 ) {
							sel.parent().show();

							if ( sel.value() === el ) {
								pick.parent().show();
								in_.parent().hide();
							} else if ( sel.value() === 'plus_'+el+'s'  ) {
								pick.parent().hide();
								in_.parent().show();
							} else {
								pick.parent().hide();
								in_.parent().hide();
							}
						} else {
							sel.parent().hide();
							pick.parent().hide();
							in_.parent().hide();
						}
					});
				} );
			}


			if ( $sel.is('[data-wp-entity="pp_calendar"]') ) {
				d		= $.extend( d, JSON.parse( $sel.attr('data-attrs') ) );
			}
			if ( 'string' === typeof d.calendars ) {
				d.calendars = ! d.calendars ? [] : d.calendars.split(',');
			}

			cal_ui_body.push({
				type	: 'label',
				text	: l10n.calendars,
				style	: 'font-weight:bold;'
			})

			//for(var j=0;j<10;j++)
			$.each( options.calendars, function(i,el){
				cal_ui_body.push({
					type	: 'checkbox',
					name	: 'calendars|'+el.id,
					value	: el.id,
					text	: el.label,
					checked	: d.calendars.indexOf(el.id.toString()) !== -1,
					style	: 'padding-left:3px;border-left:20px solid ' + (!!el.color ? el.color : 'transparent'),
				});
			});

			opt_ui_body.push({
				type	: 'label',
				text	: l10n.settings,
				style	: 'font-weight:bold;'
			});
			opt_ui_body.push({
				type		: 'listbox',
				name		: 'period',
				label		: l10n.period,
				value		: d.period,
				minWidth	: 200,
				values		: options_to_mce_options( options.periods ),
				onselect	: win_set_state,
			});

			dp_cfg={
				day	: {
					dateFormat	: 'yy-mm-dd',
				},
				week	: {
					dateFormat	: 'yy-mm-dd',
					pick		: 'week',
				},
				month	: {
					dateFormat	: 'yy-mm',
					pick		: 'month',
				},
			};

			$.each(['day','week','month'], function(i,per){
				$.each(['from','to'],function(j,pt){
					opt_ui_body.push({
						type		: 'listbox',
						name		: pt+'_'+per+'s',
						label		: l10n[pt],
						value		: d[pt+'_'+per+'s'],
						minWidth	: 200,
						values		: options_to_mce_options( options[per+'s'] ),
						onselect	: win_set_state,
					});
					opt_ui_body.push({
						type		: 'datepicker',
						name		: pt+'_'+per, // from_day
						label		: l10n['pick_'+per ],
						value		: d[pt+'_'+per],
						datepicker	: dp_cfg[ per ],
						minWidth	: 200
					});
					opt_ui_body.push({
						type		: 'textbox',
						subtype		: 'number',
						name		: pt+'_in_'+per+'s',
						label		: l10n[ 'in_'+per ],
						value		: d[pt+'_in_'+per+'s'],
						minWidth	: 200
					});

				});
			});

			// -------------
			opt_ui_body.push({
				type	: 'label',
				text	: l10n.display,
				style	: 'font-weight:bold;'
			});
			opt_ui_body.push({
				type		: 'listbox',
				name		: 'template',
				label		: l10n.template,
				value		: d.template,
				minWidth	: 200,
				values		: options_to_mce_options( options.templates ),
				onselect	: win_set_state,
			});
			opt_ui_body.push({
				type		: 'listbox',
				name		: 'tile_size',
				label		: l10n.tile_size,
				value		: d.tile_size,
				minWidth	: 200,
				values		: options_to_mce_options( options.tile_sizes )
			});
			opt_ui_body.push({
				type		: 'listbox',
				name		: 'list_size',
				label		: l10n.list_size,
				value		: d.list_size,
				minWidth	: 200,
				values		: options_to_mce_options( options.list_sizes )
			});
			opt_ui_body.push({
				type		: 'listbox',
				name		: 'colors',
				label		: l10n.colorset,
				value		: d.colors,
				minWidth	: 200,
				values		: options_to_mce_options( options.colors )
			});
			opt_ui_body.push({
				type		: 'listbox',
				name		: 'show_event',
				label		: l10n.show_event,
				value		: d.show_event,
				minWidth	: 200,
				values		: options_to_mce_options( options.show_event )
			});

			opt_ui_body.push({
				type		: 'listbox',
				name		: 'paging',
				label		: l10n.paging,
				value		: d.paging,
				minWidth	: 200,
				values		: options_to_mce_options( options.paging )
			});



			win_ui_body.push({
				type		: 'form',
				items		: opt_ui_body,
				padding		: '0 10 0 0',
			});
			win_ui_body.push({
				type		: 'form',
				items		: cal_ui_body,
				padding		: '0 10 0 10',
				maxHeight	: 400,
				style		: 'overflow:auto;'
			});



			win = editor.windowManager.open({
				title		: l10n.insert,
				body		: [{
					type		: 'container',
					items		: win_ui_body,
					minWidth	: 550,
					layout		: 'flex',  // absolute, fit, flex, flow, grid, stack
				}],
				onsubmit	: callback,
			});

			win_set_state()

		}




		editor.addCommand( 'cmd_pp_calendar_shortcode', function() {
			openDialog( function(e){
				var data = { calendars : [] };
				$.each(e.data,function(s,val){
					var is_cal = s.indexOf('calendars') !== -1,
						is_from = s.indexOf('from_') !== -1,
						is_to = s.indexOf('to_') !== -1,
						is_date = s.match(/^(from|to)_(day|week|month)$/) !== null;

					if ( is_cal ) {
						if ( val ) {
							data.calendars.push( s.split('|').pop() );
						}
					} else if ( is_date ) {
						// normalize date strings to ISO8601
						data[s] = dateToISO8601( new Date(val) );
					} else {
						data[s] = val;
					}

				});

				editor.insertContent( new wp.shortcode({
					tag:'pp_calendar',
					attrs:data,
					type:'single'
				}).string() );
			} );
		});

		editor.addButton('pp_calendar_shortcode', {
			icon: 'pp_calendar-icon',
			tooltip: l10n.insert,
			cmd : 'cmd_pp_calendar_shortcode',
			onPostRender: function() {
				var btn = this;
				editor.on( 'nodechange', function( event ) {
					var is_cal =  $(editor.selection.getNode()).is('[data-wp-entity="pp_calendar"]');
					//btn.text( is_cal ? l10n.edit : l10n.insert );
					// btn.tooltip().text( is_cal ? l10n.edit : l10n.insert );
					btn.settings.tooltip = is_cal ? l10n.edit : l10n.insert;
					btn.$el.toggleClass('pp-edit',is_cal);
					//console.log(btn);
					//btn.active( is_cal );
				});
			}
		});
		editor.on('BeforeSetcontent', function(event) {
			if ( ! event.content ) {
//				return;
			}
			event.content = wp.shortcode.replace( 'pp_calendar', event.content, shortcodeReplaceCB );
//			event.content = replaceShortcodes( event.content );
		});

		editor.on('GetContent', function(event) {
			event.content = restoreShortcodes( event.content );
		});

	} );


})(jQuery,mce_pp_calendar_shortcode);
