(function($,opt){
	var tinymce = top.tinymce;

	(function(){
		var __picker 			= $.fn.datepicker,
			__selectDay 		= $.datepicker._selectDay,
			__selectDate 		= $.datepicker._selectDate,
			__updateDatepicker	= $.datepicker._updateDatepicker,
			__generateHTML 		= $.datepicker._generateHTML,
			datepicker_instActive;

		$.fn.datepicker = function(options) {

			if (options && options.pick) {
				if ( options.pick == 'week' ) {
					options.showWeek = true;
	//				options.beforeShowDay = function(){return [false,'']}
				}
		    } else {
				options.pick = false;
			}

			__picker.apply(this, arguments );

			var $self = this,
				inst = $.datepicker._getInst( this[0] );
			inst.dpDiv
				.on('mouseover','.ui-datepicker-calendar td a',function(e) {
					var inst = datepicker_instActive;

					if ( inst.settings.pick == 'week' ) {
						$(this).closest('tr').find('a').addClass('ui-state-hover');
					} else if ( inst.settings.pick == 'month' ) {
						$(this).closest('tbody').find('td[data-month][data-year] a').addClass('ui-state-hover');
					}

				});

			// if ( options.pick == 'week' ) {
			//
			// } else if ( options.pick == 'month' ) {
			// 	inst.dpDiv
			// 		.on('mouseover','.ui-datepicker-calendar td a',function(e) {
			//
			// 		});
			//
			// }
		}

		$.extend( $.datepicker, {
			_selectDay : function( id, month, year, td ) {
				var inst, current, wday, wdiff, day,
					target = $( id );
				inst = this._getInst( target[ 0 ] );

				if ( inst.settings.pick == 'week' ) {

					first_td = $(td).parent().children('[data-handler]').first();
					// find first day of week!
					day = $( "a", td ).html();
					current = new Date( year, month, day );
					wday = current.getDay();
					wdiff = current.getDate() - wday + ( wday == 0 ? -6 : 1 );

					current = new Date( current.setDate( wdiff ) );


					inst.selectedDay = inst.currentDay = current.getDate();
					inst.selectedMonth = inst.currentMonth = current.getMonth();
					inst.selectedYear = inst.currentYear = current.getFullYear();

					this._selectDate( id, this._formatDate( inst,
						inst.currentDay, inst.currentMonth, inst.currentYear ) );

				} else if ( inst.settings.pick == 'month' ) {

					inst.selectedDay = inst.currentDay = 1;
					inst.selectedMonth = inst.currentMonth = month;
					inst.selectedYear = inst.currentYear = year;

					this._selectDate( id, this._formatDate( inst,
						inst.currentDay, inst.currentMonth, inst.currentYear ) );
				} else {
					return __selectDay.apply( this, arguments );
				}
			},
			_updateDatepicker : function(inst) {
				var tMin, tMax;
				datepicker_instActive = inst;
				__updateDatepicker.apply(this,arguments);

				if ( inst.settings.pick == 'week' ) {
					tMin = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay ).getTime();
					tMax = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay + 7 ).getTime();

					inst.dpDiv.find('td[data-month][data-year]').each(function(){
						var t = new Date( $(this).attr('data-year'), $(this).attr('data-month'), $(this).text() ).getTime(),
							$a = $(this).find('a');
						$a.toggleClass( 'ui-state-active', t >= tMin && t < tMax );
					});
				} else if ( inst.settings.pick == 'month' ) {
					inst.dpDiv.find('td[data-month="'+inst.currentMonth+'"][data-year="'+inst.currentYear+'"]').each(function(){
						$(this).find('a').addClass( 'ui-state-active' );
					});
				}

			}
		});
	})();

	// https://stackoverflow.com/questions/38124482/tinymce-add-custom-control-for-windowmanager
	opt._init = function() {
		if ( !! tinymce.ui.DatePicker) {
			return;
		}
		tinymce.ui.DatePicker = tinymce.ui.Widget.extend({
			/**
			 * Constructs a instance with the specified settings.
			 *
			 * @constructor
			 * @param {Object} settings Name/value object with settings.
			 * @setting {String} format
			 */
			init: function(settings) {

				var self = this;

				self._super(settings);

				self.settings.datepicker = self.settings.datepicker || {}

				self.classes.add('datepicker');
			},
			/**
			 * Repaints the control after a layout operation.
			 *
			 * @method repaint
			 */
			repaint: function() {
				var self = this, style, rect, borderBox, borderW = 0, borderH = 0, lastRepaintRect;

				style = self.getEl().style;
				rect = self._layoutRect;
				lastRepaintRect = self._lastRepaintRect || {};

				// Detect old IE 7+8 add lineHeight to align caret vertically in the middle
				var doc = document;
				if (!self.settings.multiline && doc.all && (!doc.documentMode || doc.documentMode <= 8)) {
					style.lineHeight = (rect.h - borderH) + 'px';
				}

				borderBox = self.borderBox;
				borderW = borderBox.left + borderBox.right + 8;
				borderH = borderBox.top + borderBox.bottom + (self.settings.multiline ? 8 : 0);

				if (rect.x !== lastRepaintRect.x) {
					style.left = rect.x + 'px';
					lastRepaintRect.x = rect.x;
				}

				if (rect.y !== lastRepaintRect.y) {
					style.top = rect.y + 'px';
					lastRepaintRect.y = rect.y;
				}

				if (rect.w !== lastRepaintRect.w) {
					style.width = (rect.w - borderW) + 'px';
					lastRepaintRect.w = rect.w;
				}

				if (rect.h !== lastRepaintRect.h) {
					style.height = (rect.h - borderH) + 'px';
					lastRepaintRect.h = rect.h;
				}

				self._lastRepaintRect = lastRepaintRect;
				self.fire('repaint', {}, false);

				return self;
			},

			/**
			 * Renders the control as a HTML string.
			 *
			 * @method renderHtml
			 * @return {String} HTML representing the control.
			 */
			renderHtml: function() {
				var self = this, id = self._id, settings = self.settings, value = self.state.get('value'), extraAttrs = '';

				if (self.disabled()) {
					extraAttrs += ' disabled="disabled"';
				}

				return '<input id="' + id + '" class="' + self.classes + '" value="' + value + '" hidefocus="1"' + extraAttrs + ' />';
			},

			value: function(value) {
				if (arguments.length) {
					this.state.set('value', value);
					return this;
				}

				// Make sure the real state is in sync
				if (this.state.get('rendered')) {
					this.state.set('value', this.getEl().value);
				}

				return this.state.get('value');
			},

			/**
			 * Called after the control has been rendered.
			 *
			 * @method postRender
			 */
			postRender: function() {
				var self = this;

				self._super();

				$(self.getEl()).datepicker(self.settings.datepicker);

				// wrap the datepicker (only if it hasn't already been wrapped)
				if( $('body > #ui-datepicker-div').exists() ) {

					$('body > #ui-datepicker-div').wrap('<div class="acf-ui-datepicker" />');

				}

				self.$el.on('change', function(e) {
					self.state.set('value', e.target.value);
					self.fire('change', e);
				});
			},
			bindStates: function() {
				var self = this;

				self.state.on('change:value', function(e) {
					if (self.getEl().value != e.value) {
						self.getEl().value = e.value;
					}
				});

				self.state.on('change:disabled', function(e) {
					self.getEl().disabled = e.value;
				});

				return self._super();
			},

			remove: function() {
				this.$el.off();
				this._super();
			}
		});

		tinymce.ui.Factory.add( 'datepicker', tinymce.ui.DatePicker );

	}



})(jQuery, mce_pp_calendar_shortcode );
