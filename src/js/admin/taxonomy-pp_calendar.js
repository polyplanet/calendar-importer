(function($,opt){
	var l10n = calendar_importer_admin.l10n;
	$(document).on('click','.pp-calendar-sync-action',function(e){
		var url,
			self = this;
	
		e.preventDefault();
	
		if ( $(this).hasClass('spin') ) {
			return;
		}
	
		url = $(this).attr('href');
		$(this).addClass('spin');
		$.ajax({
			url: url,
			success:function(response){
				$(self).removeClass('spin');
				if ( response.success ) {
					$(self).closest('tr').find('.column-last_sync').text( l10n.now );
	
					if ( !!response.calendar && !!response.calendar.name ) {
						$(self).closest('tr').find('.row-title').text( response.calendar.name ); 
					}
				}
			},
			complete:function(){
				$(self).removeClass('spin');
			},
			data:'',
		})
	});

})(jQuery);
