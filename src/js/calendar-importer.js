(function($){
	var options = pp_calendar.options,
		l10n = pp_calendar.l10n,
		markerIcon,
		currentXhr = false;

	$(document)
		.on('acf-osm-map-create-layers', function(e) {
			// set layer provider for dark theme
			if ( $(e.target).closest('[data-pp-colors="dark"]').length ) {
				e.detail.mapData.mapLayers = ['CartoDB.DarkMatter']
			}
		})
		.on('acf-osm-map-marker-create', function(e) {
			// set custom marker
			e.detail.markerOptions.icon = L.divIcon( { className: 'pp-icon-location' } )
		});

	function show_event( event_id, $box, callback ) {
		var cb = 'function' === typeof callback ? callback : function(){};

		$box.addClass('pp-loading');
		currentXhr = $.get({
			url:options.ajax_url,
			data:{
				'action':'pp-date',
				'dates':event_id,
			},
			success:function(response){
				$box.removeClass('pp-loading');
				$box.html( response );
				currentXhr = false;
			}
		})

		return $box;
	}

	function show_event_lightbox( $event_el ) {
		var colors = $event_el.closest('[data-pp-colors]').attr('data-pp-colors'),
			html = $('#pp-tpl-event-lightbox').html(),
			$box = $(html)
				.appendTo('body')
				.attr('data-pp-colors',colors)
				.css('display','none').fadeIn()
				.find('.pp-box').first();

		show_event( $event_el.attr('data-event-id'), $box );
	}


	// show inline
	function get_event_inline_box( $event_el ) {
		var el_id = $event_el.attr('id'),
			$box = $( '#'+el_id+ ' ~ .pp-event-inline:visible').first();

		if ( $box.length ) {
			return $box.first();
		}
		return false;
	}

	function show_event_inline($event_el) {
		var html = $('#pp-tpl-event-inline').html(),
			$container = get_event_inline_box( $event_el )
				.attr('data-event-open','true'),
			$box = $(html);
			$box.appendTo( $container )
				.css('display','none').slideDown()
				.find('.pp-box').first();

		show_event(
			$event_el.attr('data-event-id'),
			$box,
			function(){
				$box.slideDown( 500, function(){});
				$("html, body").stop().animate({
					scrollTop: $box.offset().top - $event_el.height() - 50,
					duration:500,
				});
			});
		$event_el.attr( 'data-event-open', 'true' );


		//$box.first().html(html);

	}
	function hide_events_inline( callback ) {
		var cb = 'function' === typeof callback ? callback : function(){},
			len = $('[data-event-id][data-event-open="true"]').each(function(i,el){
				var $box = get_event_inline_box( $(el) ),
					$self = $(this);
				// close
				$box.slideUp( 333, function(){
					$box.html('');
					$self.attr('data-event-open','false');
					$box.removeAttr('style').attr('data-event-open','false');
					cb();
				});

//				$("html, body").animate({scrollTop:$box.offset().top},'swing')
			}).length;
		if ( ! len ) {
			cb();
		}
	}

	$(document)
		.on('click','[data-event-show="lightbox"]',function(e){
			var self = this;
			e.preventDefault();
			show_event_lightbox( $(self).closest('.pp-event') );
		})
		.on('click','[data-event-show="inline"]',function(e){
			var self = this, cb = null;

			if ( $(this).closest('[data-event-open="false"]').length ) {
				cb = function() {
					show_event_inline( $(self).closest('.pp-event') );
				};
			}
			e.preventDefault();
			hide_events_inline( cb );
		})
		.on('click','.pp-more',function(e) {
			e.preventDefault();
			$('[data-more="true"]').attr('data-more','false');
			$(this).closest('[data-more]').attr('data-more','true');

		})
		.on('click','.pp-event-lightbox',function(e){
			e.preventDefault();
			if ( $(e.target).is('.pp-event-lightbox,.pp-close') ) {
				var $el = $(e.target).closest('.pp-event-lightbox');
				!! currentXhr && currentXhr.abort();
				$el.fadeOut(function(){
					$el.remove();
				});
			}
		})
		.on('click','.pp-event-inline .pp-close',function(e){
			e.preventDefault();
			hide_events_inline();
			!! currentXhr && currentXhr.abort();
		})
		.on('click','.pp-pages-controls [data-action]',function(e){
			var cb = $(e.target).data('action'),
				$pages = $(e.target).closest('.pp-pages'),
				$current = $pages.find('.pp-active'),
				$next = $current[cb]('.pp-events');
			if ( ! $next.length ) {
				return;
			}
			$current.removeClass('pp-active');
			$next.addClass('pp-active');
		});

	// maps
	$(document).ready( function() {
		$('.pp-pages').each(function(){
			$(this).children().first().addClass('pp-active');
		});
	})
})(jQuery)
