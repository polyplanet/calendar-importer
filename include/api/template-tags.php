<?php




function pp_calendar_info( $info ) {
	return pp_schedule()->get( $info );
}

function pp_date_is_multiple( $date ) {
	$dtend2 = clone( $date->dtend_dt );
	$dtend2->sub( new \DateInterval('PT1S') );
	return $date->dtstart_dt->format('Y-m-d') !== $dtend2->format('Y-m-d');
}


function pp_do_event_js() {
	global $pp_did_event_body;
	$the_date = pp_get_the_date();
	if ( ! isset($pp_did_event_body) ) {
		$pp_did_event_body = array();
	}

	if ( isset($pp_did_event_body[$the_date->id]) ) {
		return;
	}

	?>
	<script type="text/html" data-event-html-for="<?php echo intval( $the_date->id ); ?>">
		<?php get_template_part('calendar-importer/event','body') ?>
	</script>
	<?php
	$pp_did_event_body[$the_date->id] = true;
}


/**
 *	@param WP_Post $event
 *	@return int
 */
function pp_get_unique_event_id( $event ) {
	static $pp_event_counter;
	if ( ! isset($pp_event_counter)) {
		$pp_event_counter = 1;
	}
	$event_id = sprintf( 'event-%d-%d', $event->ID, $pp_event_counter++ );
	return $event_id;
}

/**
 *	@param boolean $all_events Check all day events too.
 */
function pp_get_schedule_layout( $all_events = false ) {
	$layout = (object) array(
		'min'		=> 24,
		'max'		=> 0,
		'all_day'	=> 0,
	);

	while ( pp_have_dates( $all_events ) ) {
		pp_the_date( $all_events );
		$date = pp_get_the_date();
		$layout->all_day += $date->all_day;

		if ( ! $date->all_day ) {
			$begin_hour = intval( $date->dtstart_dt->format('G') );
			$end_hour = intval( $date->dtend_dt->format('G') );
			$layout->min = min( $begin_hour - 1, $layout->min );
			$layout->max = max( $end_hour + 1, $layout->max );
		}

	}
	pp_reset_dates( $all_events );

	$layout->min = max( 0, $layout->min );
	$layout->max = min( 23, $layout->max );
	return $layout;
}

/**
 *	@param boolean $all_events Print all day events too.
 */
function pp_print_schedule( $all_events = false ) {

	$schedule_layout = pp_get_schedule_layout( $all_events );

	if ( $schedule_layout->all_day ) {
		?>
		<div class="pp-schedule-hour pp-schedule-hour-all-day">
			<div class="pp-hour pp-icon-clock-invers pp-all-day">
				<?php esc_attr_e('All day','calendar-importer') ?>
			</div>
			<div class="pp-schedule-content pp-more-box" data-more="false">
				<?php
					$i = 0;
					$more_i = 0;
					$more_max = apply_filters( 'pp_calendar_max_all_day_events', 4 );
					while ( pp_have_dates() ) {
						pp_the_date();
						$date = pp_get_the_date();

						if ( ! $date->all_day ) {
							continue;
						}
						$i++;
						if ( $more_max ) {
							if ( $i == $more_max ) {
								printf('<div class="pp-more-cut"></div>');
							}
							if (  $i >= $more_max ) {
								$more_i++;
							}
						}

						get_template_part( 'calendar-importer/event-calendar', pp_calendar_info( 'period' ) );
				//		pp_do_event_js();
					}
					if ( $more_i ) {
						esc_html_e( sprintf( '<a class="pp-more pp-icon-plus" href="#">%s</a>',
							/* translators: %d more dates */
							sprintf( _n( '%d more', '%d more', $more_i, 'calendar-importer'), $more_i )
						));
					}
					pp_reset_dates();

				?>
			</div>
		</div>
		<?php
	}
	$prev_hour = $schedule_layout->min;
	$current_hour = $schedule_layout->max;
	$must_close = false;

	$open_hour_tpl = '
		<div class="pp-schedule-hour">
			<div class="pp-hour pp-icon-clock">%s</div>
			<div class="pp-schedule-content">';
	$close_hour_tpl = '
			</div>
		</div>';
	$zero_minutes = '<span class="pp-minutes">:00</span>';

	while ( pp_have_dates() ) {
		pp_the_date();
		$date = pp_get_the_date();
		if ( $date->all_day ) {
			continue;
		}
		$current_hour = $date->dtstart_dt->format('H:00');
		if ( $current_hour != $prev_hour ) {
			// close it
			if ( $must_close ) {
				echo $close_hour_tpl;
				$must_close = false;
			}

			// print empty hours after an event
			if ( intval( $prev_hour ) ) {
//					$diff = intval($current_hour) - intval($prev_hour) - 1;
				for ( $i = intval($prev_hour); $i < intval($current_hour)-1; $i++ ) {
					printf( $open_hour_tpl . $close_hour_tpl, str_pad( $i+1, '0',2 ) . $zero_minutes );
				}
			}

			// re-open
			printf( $open_hour_tpl, $current_hour );

			$prev_hour = $current_hour;
			$must_close = true;
		}

		get_template_part( 'calendar-importer/event-calendar', pp_calendar_info( 'period' ) );
		// pp_do_event_js();

	}
	if ( $must_close ) {
		echo $close_hour_tpl;

	}
	for ( $i = intval( $current_hour ); $i < $schedule_layout->max; $i++ ) {
		printf( $open_hour_tpl . $close_hour_tpl, str_pad( $i+1, '0',2 ) . $zero_minutes );
	}

	pp_reset_dates();

}

function _get_color_meta_event( $event_id ) {
	global $wpdb;
	return $wpdb->get_var(
		$wpdb->prepare(
			"SELECT meta_value FROM $wpdb->postmeta WHERE post_id = %d AND meta_key = %s LIMIT 1",
			$event_id,
			'pp_event_color'
		)
	);
}
function _get_color_meta_calendar( $term_id ) {
	global $wpdb;
	return $wpdb->get_var(
		$wpdb->prepare(
			"SELECT meta_value FROM $wpdb->termmeta WHERE term_id = %d AND meta_key = %s LIMIT 1",
			$term_id,
			'pp_cal_color'
		)
	);
}

function pp_event_color( $date ) {
	if ( $color = _get_color_meta_event( $date->event->ID ) ) {
		return $color;
	}
	if ( $color = _get_color_meta_calendar( $date->term_id ) ) {
		return $color;
	}

	return 'default';//get_field('default_color','pp_calendar_options');
}
function pp_event_color_key( $date ) {
	$color = pp_event_color( $date );
	return strtolower(substr( $color, 1 ) );
}

/**
 *	@param WP_Post $event
 *	@return string
 */
function pp_event_location( $event ) {
	$output = '';
	$output .= pp_event_location_address( $event );
	$output .= pp_event_location_map( $event );
	return $output;
}
/**
 *	@param WP_Post $event
 *	@return string
 */
function pp_event_location_address( $event ) {
	$output = '';
	if ( $location = get_field( 'field_pp_event_location', $event->ID ) ) {
		$output .= sprintf( '<address>%s</address>', $location );
	}
	return $output;
}

/**
 *	@param WP_Post $event
 *	@return string
 */
function pp_event_location_map( $event ) {
	return pp_event_location_map_leaflet( $event );
}

/**
 *	@param WP_Post $event
 *	@return string
 */
function pp_event_location_map_leaflet( $event ) {
	$field = get_field( 'field_pp_event_location_map', $event->ID, true );
	if ( ! is_string( $field ) ) {
		// acf openstreetmap field plugin might not be active...
		return '';
	}
	return $field;
}

/**
 *	@param WP_Post $event
 *	@return string
 */
// function pp_event_location_map_iframe( $event ) {
// 	$output = '';
// 	if ( $location_map = get_field( 'field_pp_event_location_map', $event->ID, false ) ) {
// 		/*
// 		$output .= sprintf( '<div>Karte: <div class="pp-map" data-address="%s" data-zoom="%d" data-lat="%f" data-lng="%f"></div></div>',
// 			esc_attr( $location['address'] ),
// 			esc_attr( $location['zoom'] ),
// 			esc_attr( $location['lat'] ),
// 			esc_attr( $location['lng'] )
// 		);
// 		/*/
// 
// 		$embed = sprintf('//www.openstreetmap.org/export/embed.html?layer=mapnik&bbox=%f,%f,%f,%f&marker=%f,%f',
// 			$location_map['lng'] - 0.005,
// 			$location_map['lat'] -0.005,
// 			$location_map['lng'] + 0.005,
// 			$location_map['lat'] +0.005,
// 			$location_map['lat'],
// 			$location_map['lng']
// 		);
// 
// 		$link = sprintf('//www.openstreetmap.org/?mlat=%f&mlon=%f#map=%d/%f/%f',
// 			$location_map['lat'],
// 			$location_map['lng'],
// 			$location_map['zoom'],
// 			$location_map['lat'],
// 			$location_map['lng']
// 		);
// 		$output .= sprintf('<iframe class="pp-map" scrolling="no" src="%s"></iframe><small><a target="_blank" href="%s">%s</a></small>',
// 			$embed,
// 			$link,
// 			__('View bigger map','calendar-importer')
// 		);
// 
// 		//*/
// 	}
// 	return $output;
// }


/**
 *	@param WP_Post $event
 *	@return string
 */
function pp_event_link( $event ) {
	if ( $url = get_field( 'pp_event_url', $event->ID ) ) {
		// Event link
		return sprintf( '<a href="%s" rel="noopener noreferrer" target="blank">%s</a>', 
			esc_url( $url ), 
			esc_html( preg_replace( '|https?://|','', $url ) )
		);
	}
	return '';
}


/**
 *	@param WP_Post $event
 *	@return string
 */
function pp_event_permalink( $event ) {
	return get_permalink( $event->ID );
}

/**
 *	UNUSED!!!
 */
function pp_format_datetime( $date = false, $date_format = null, $time_format = null ) {
	if ( ! $date ) {
		$date = pp_get_the_day();
	}
	if ( is_null( $date_format ) ) {
		$date_format = get_option( 'date_format' );
	}
	if ( is_null( $time_format ) ) {
		$time_format = get_option( 'time_format' );
	}

	$dtstart = clone( $date->dtstart_dt );
	$dtend = clone( $date->dtend_dt );
	$endclone = clone( $date->dtend_dt );
	$endclone->sub( new DateInterval('PT1S') );
	$is_one_day = $dtstart->format('Y-m-d') == $endclone->format('Y-m-d');

	if ( $date->all_day ) {
		$dtend->sub( new \DateInterval('P1D') );

		if ( $is_one_day ) {
			// 12.Okt.2017
			return sprintf('<time datetime="%s">%s</time>',
				esc_attr( $dtstart->format( 'c' ) ),
				esc_html( $dtstart->format( $date_format ) )
			);

		} else {
			// 12.Okt.2017 - 14.Okt.2017
			/* translators: 1 datetime from, 2 datetime to */
			return sprintf( _x('%1$s until %2$s','datetime from, datetime to','calendar-importer'),
				sprintf('<time datetime="%s">%s</time>',
					esc_attr( $dtstart->format( 'c' ) ),
					esc_html( $dtstart->format( $date_format ) )
				),
				sprintf('<time datetime="%s">%s</time>',
					esc_attr( $dtend->format( 'c' ) ),
					esc_html( $dtend->format( $date_format ) )
				)
			);
		}
	} else {
		if ( $is_one_day ) {
			// 12.Okt.2017 9:00 - 10:00
			/* translators: 1 date, 2: time from 2 time to */
			return sprintf( _x('%1$s, %2$s until %3$s','date, time from, time until','calendar-importer'),

				esc_html($dtstart->format( $date_format )),

				sprintf('<time datetime="%s">%s</time>',
					esc_attr( $dtstart->format( 'c' ) ),
					esc_html( $dtstart->format( $time_format ) )
				),
				sprintf('<time datetime="%s">%s</time>',
					esc_attr( $dtend->format( 'c' ) ),
					esc_html( $dtend->format( $time_format ) )
				)
			);

		} else {
			// 12.Okt.2017 9:00 - 14.Okt.2017 10:00
			/* translators: 1 datetime from, 2 datetime to */
			return sprintf( _x('%1$s until %2$s','datetime from, datetime to','calendar-importer'),

				sprintf('<time datetime="%s">%s</time>',
					esc_attr( $dtstart->format( 'c' ) ),
					esc_html( $dtstart->format( $date_format . ' ' . $time_format ) )
				),
				sprintf('<time datetime="%s">%s</time>',
					esc_attr( $dtend->format( 'c' ) ),
					esc_html( $dtend->format( $date_format . ' ' . $time_format ) )
				)
			);
		}
	}
}
