<?php


function pp_schedule() {
	return CalendarImporter\Model\ModelSchedules::instance();
}


/**
 *	Wrapper for WP date_i18n().
 *	Add Timezone seconds to $dt and pass it to date_i18n().
 *
 *	@param DateTime $date_format
 *	@param DateTime $dt
 *
 *	@return String
 */
function pp_date_i18n( $date_format, $dt ) {
	return date_i18n( $date_format, $dt->getTimestamp() + $dt->format('Z'), false );
}

/**
 *	@return String
 */
function pp_get_timezone_option() {

	$tz = get_option('timezone_string');

	if ( ! empty( $tz ) ) {
		return $tz;
	}

	$offs = get_option('gmt_offset') * 60 * 60; // seconds

	if ( ! empty( $offs ) ) {
		return ( $offs > 0 ? '+' : '-' ) . date( 'Hi', abs( $offs ) );
	}

	return '+0000';
}

/**
 *	@return Boolean
 */
function pp_have_days() {
	return pp_schedule()->have_days();
}

function pp_reset_days() {
	return pp_schedule()->reset_days();
}

function pp_the_day() {
	// setup day
	return pp_schedule()->the_day();
}

function pp_get_the_day() {
	// return current day
	return pp_schedule()->get_the_day();
}




function pp_have_dates( $all = false ) {
	return pp_schedule()->have_dates( $all );
}

function pp_reset_dates( $all = false ) {
	return pp_schedule()->reset_dates( $all );
}

function pp_the_date( $all = false ) {
	// setup day
	return pp_schedule()->the_date( $all );
}

function pp_get_the_date() {
	// return current day
	return apply_filters( 'pp_the_date', pp_schedule()->get_the_date() );
}



function pp_count_recurring_dates() {
	return pp_schedule()->count_recurring_dates();
}
function pp_have_recurring_dates() {
	return pp_schedule()->have_recurring_dates();
}
function pp_reset_recurring_dates() {
	return pp_schedule()->reset_recurring_dates();
}

function pp_the_recurring_date() {
	return pp_schedule()->the_recurring_date();

}
function pp_get_the_recurring_date() {
	return pp_schedule()->get_the_recurring_date();
}


/**
 *	Looping Dates
 */
function the_pp_date() {
	return apply_filters( 'the_pp_date', pp_schedule()->the_date() );
}
function next_pp_date() {
	return pp_schedule()->next_date();
}
