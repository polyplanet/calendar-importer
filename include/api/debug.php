<?php

if ( defined('CALENDAR_IMPORTER_DEBUG') && CALENDAR_IMPORTER_DEBUG ) {
	function pp_cal_debug( $what, $error_type = E_USER_NOTICE ) {
		if ( defined( 'WP_CLI' ) ) {
			\WP_CLI::line( $what );
		} else {
			trigger_error( $what, $error_type );
		}
	}
} else {
	function pp_cal_debug( $what, $error_type = E_USER_NOTICE ) {
		do_action( 'pp_cal_debug', $what, $error_type );
	}
}
