<?php

namespace CalendarImporter\Cron;

use CalendarImporter\Core;

class Cron extends Core\PluginComponent {

	/**
	 *	@var array [
	 *		'<hook>' => [
	 *			'<hash>' => [
	 *				0 => '<class_name>'
	 *				1 => 'method'
	 *			]
	 *		]
	 *	]
	 */
	private $jobs = [];

	/**
	 *	@param	string		$hook
	 *	@param	callable	$callback
	 *	@param	array		$args
	 *	@param	absint		$interval
	 *	@return array
	 */
	public static function findJobs( $hook, $callback, $args = null, $interval = null ) {
		$jobs = array();
		$cron = get_option( 'cron' );
		$inst = self::instance();

		// early return
		if ( ! isset( $inst->jobs[ $hook ] ) ) {
			return $jobs;
		}

		unset( $cron['version'] );
		// compares jobs with wp-jobs and returns it
		foreach ( $cron as $time => $wp_jobs ) {

			foreach ( $wp_jobs as $job_hook => $job ) {
				if ( $job_hook != $hook ) {
					continue;
				}
				foreach ( $job as $args_key => $wp_job ) {
					//

					if (
							// callback matches
							/*
							in_array( $callback, $inst->jobs[ $hook ] ) &&
							/*/
							$inst->has_hook( $hook, $callback ) &&
							//*/
							// intarval matches
							( is_null( $interval ) || $interval == $wp_job['interval'] ) &&

							// args match
							( is_null( $args ) || $args == $wp_job['args'] )
					) {
						$jobs[]	= self::createJob( $hook, $callback, $wp_job['args'], $wp_job['interval'] );
					}
				}
			}
		}

		return $jobs;
	}

	/**
	 *	@param	string		$hook
	 *	@param	callable	$callback
	 *	@param	array		$args
	 *	@param	absint		$interval
	 *	@return Cron\Job
	 */
	public static function getJob( $hook, $callback, $args = array(), $interval = 3600 ) {

		$found = self::findJobs( $hook, $callback, $args );

		if ( ! empty( $found ) ) {
			return $found[0];
		}

		return self::createJob( $hook, $callback, $args, $interval );
	}

	/**
	 *	@param	string		$hook
	 *	@param	callable	$callback
	 *	@param	array		$args
	 *	@param	absint		$interval
	 *	@return Cron\Job
	 */
	protected static function createJob( $hook, $callback, $args = array(), $interval = 3600 ) {

		if ( $schedule = self::instance()->get_schedule( $interval ) ) {
			return new Job( $hook, $callback, $args, $schedule );
		}

	}


	/**
	 *	Constructor
	 */
	protected function __construct() {

		add_action( 'init', array( $this, 'init' ) );

		add_option( 'calendar_importer_cronjobs', array() );
		add_option( 'calendar_importer_cronschedules', array() );

		add_filter( 'cron_schedules', array( $this, 'cron_schedules' ) );

//		$this->init();
	}


	/**
	 *	@return void
	 */
	public function init() {
		$this->jobs = $this->get_jobs();

		foreach ( $this->jobs as $hook => $callbacks ) {

			foreach ( $callbacks as $key => $callback ) {
				add_action( $hook, $callback );
			}
			$this->jobs[ $hook ] = (array) $callbacks;
		}
		$this->update();
	}

	/**
	 *	@param string $hook
	 *	@param null|callable $cb
	 *	@return bool
	 */
	protected function has_hook( $hook, $cb = null ) {
		if ( ! is_array( $this->jobs[$hook] ) ) {
			return false;
		}
		if ( is_null( $cb ) ) {
			return true;
		}
		foreach ( $this->jobs[ $hook ] as $key => $callback ) {
			if ( is_array( $callback ) && is_array( $cb ) ) {
				if ( $callback[0] === $cb[0] && $callback[1] === $cb[1] ) {
					return true;
				}
			} else if ( ! is_array( $callback ) && ! is_array( $cb ) ) {
				if ( $callback === $cb ) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 *	@param int $interval
	 *	@return bool|string
	 */
	public function get_schedule( $interval ) {
		if ( empty( $interval ) ) {
			return false;
		}
		if ( ! $schedule = $this->find_schedule( $interval ) ) {
			$schedule = sprintf( 'every_%d_seconds', $interval );
			$schedules = get_option('calendar_importer_cronschedules');
			$schedules[ $schedule ] = array(
				'interval'	=> $interval,
				/* translators: number of seconds */
				'display'	=> sprintf( __( 'Every %d seconds', 'calendar-importer' ), $interval ),
			);
			update_option( 'calendar_importer_cronschedules', $schedules );
		}
		return $schedule;
	}

	/**
	 *	@param int $interval
	 *	@return bool|string
	 */
	public function find_schedule( $interval ) {
		foreach ( wp_get_schedules() as $slug => $schedule ) {
			if ( $schedule['interval'] == $interval ) {
				return $slug;
			}
		}
		return false;
	}

	/**
	 *	@filter cron_schedules
	 */
	public function cron_schedules( $schedules ) {
		if ( ( $ci_schedules = get_option('calendar_importer_cronschedules') ) && is_array( $ci_schedules ) ) {
			$schedules += $ci_schedules;
		}
		return $schedules;
	}

	/**
	 *	Register a job.
	 *	@param Cron\Job $job
	 */
	public function register_job( $job ) {
		$hook = $job->get_hook();
		$key = $job->get_key();

		if ( ! isset( $this->jobs[ $hook ] ) ) {
			$this->jobs[ $hook ] = array();
		}
		$this->jobs[ $hook ][ $key ] = $job->get_callback();

		$this->log( sprintf('Register Job: hook <%s> key <%s>', $hook, $key ) );

		$this->update();
	}

	/**
	 *	@param string $text
	 */
	public function log( $text ) {
		if ( ! is_string( $text ) ) {
			return;
		}
		pp_cal_debug( sprintf( '%s: %s', get_class( $this ), $text ) );
	}

	/**
	 *	Unregister a job.
	 *	@param Cron\Job $job
	 */
	public function unregister_job( $job ) {
		$hook = $job->get_hook();
		$key = $job->get_key();
		if ( isset( $this->jobs[ $hook ], $this->jobs[ $hook ][ $key ] ) ) {
			unset( $this->jobs[ $hook ][ $key ] );
		}
		$this->log( sprintf('Unregister Job: hook <%s> key <%s>', $hook, $key ) );
		$this->update();
	}

	/**
	 *	@return array 
	 */
	protected function get_jobs() {
		$jobs = get_option( 'calendar_importer_cronjobs' );

		foreach ( $jobs as $hook => $callbacks ) {
			foreach ( $callbacks as $key => $callback ) {
				if ( is_array( $callback ) 
					&& 2 === count( $callback ) 
					&& is_string($callback[0]) 
					&& is_callable( $callback[0], 'instance' ) 
				) {
					$jobs[$hook][$key][0] = $callback[0]::instance();
				}
			}
		}

		return $jobs;
	}


	/**
	 *	Update jobs option
	 */
	private function update() {
		//*
		$jobs = [];
		foreach ( $this->jobs as $hook => $callbacks ) {
			$jobs[$hook] = [];
			foreach ( $callbacks as $key => $callback ) {
				if ( is_array( $callback ) && 2 === count( $callback ) && ($callback[0] instanceof Core\Singleton) ) {
					$callback[0] = get_class($callback[0]);
				}
				$jobs[$hook][$key] = $callback;
				
			}
		}
		update_option( 'calendar_importer_cronjobs', $jobs );
		/*/
		update_option( 'calendar_importer_cronjobs', $this->jobs );
		//*/
	}


	/**
	 *	@inheritdoc
	 */
	public function activate() {
		return array(
			'success'	=> true,
			'messages'	=> array(),
		);
	}

	/**
	 *	@inheritdoc
	 */
	public function deactivate() {
		// stop all jobs
		$jobs = $this->get_jobs();
		foreach ( $jobs as $hook => $callbacks ) {
			/*
			wp_clear_scheduled_hook( $hook );
			/*/
			foreach ( array_unique( (array) $callbacks ) as $callback ) {
				foreach ( self::findJobs( $hook, $callback ) as $job ) {
					$job->stop();
				}
			}
			//*/
		}
		// update_option( 'calendar_importer_cronjobs', array() );
		return array(
			'success'	=> true,
			'messages'	=> array(),
		);
	}

	/**
	 *	@inheritdoc
	 */
	public static function uninstall() {

		delete_option( 'calendar_importer_cronjobs' );
		delete_option( 'calendar_importer_cronschedules' );

		return array(
			'success'	=> true,
			'messages'	=> array(),
		);
	}

	/**
	 *	@inheritdoc
	 */
	public function upgrade( $new_version, $old_version ) {
		if ( version_compare( $old_version, '0.3.0', '<' ) ) {
			$this->jobs = $this->get_jobs();
			$this->update();
			pp_cal_debug( sprintf( '[CALENDAR_IMPORTER] %s: Migrate %s => %s', get_class( $this ), $old_version, $new_version ) );
		}
	}


}
