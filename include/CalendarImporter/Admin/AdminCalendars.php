<?php

namespace CalendarImporter\Admin;
use CalendarImporter\Ajax;
use CalendarImporter\Core;
use CalendarImporter\iCal;
use CalendarImporter\PostType;


class AdminCalendars extends Core\Singleton {


	protected function __construct() {
		$this->sync_ajax_handler = new Ajax\AjaxHandler( 'pp_calendar_sync', array(
			'public'		=> false,
			'use_nonce'		=> true,
			'capability'	=> 'edit_posts',
			'callback'		=> array( $this, 'ajax_calendar_sync' ),
			'method'		=> 'get',
		));

		add_action( 'pp_calendar_pre_add_form', array( $this, 'print_create_form' ) );

		add_filter( 'manage_edit-pp_calendar_columns', array( $this, 'calendar_columns' ) );
		add_filter( 'manage_pp_calendar_custom_column', array( $this, 'render_column' ), 10, 3 );
	}

	/**
	 *	@filter manage_edit-pp_calendar_columns
	 */
	public function calendar_columns( $columns ) {
		$columns['sync'] = __( 'Sync', 'calendar-importer' );
		$columns['last_sync'] = __( 'Last Sync', 'calendar-importer' );
		unset( $columns['posts'] );
		unset( $columns['slug'] );
		unset( $columns['description'] );

		return $columns;
	}

	/**
	 *	@filter manage_pp_calendar_custom_column
	 */
	public function render_column( $output, $column, $term_id ) {
		if ( 'sync' === $column ) {
			$req = $this->sync_ajax_handler->request;
			$req += [ 'term_id' => $term_id ];
			$url = add_query_arg( $req, admin_url( 'admin-ajax.php' ) );
			printf('<a class="pp-calendar-sync-action acf-icon -sync blue acf-js-tooltip" title="%s" href="%s"></a>',
				esc_html__('Synchronize now','calendar-importer'),
				$url
			);
			return '';
		} else if ( 'last_sync' === $column ) {
			if ( $last_sync = get_term_meta( $term_id, '_pp_cal_last_sync', true ) ) {
				$time_diff = time() - $last_sync;

				if ( $time_diff > 0 && $time_diff < DAY_IN_SECONDS ) {
					printf( esc_html__( '%s ago' ), human_time_diff( $last_sync ) );
				} else {
					echo esc_html( date_i18n( __( 'Y/m/d' ), $last_sync ) );
					echo ' ';
					echo esc_html( date_i18n( _x('g:i A','list time format','calendar-importer'), $last_sync ) );
				}
			}
		}
	}


	/**
	 *	Ajax callback
	 */
	public function ajax_calendar_sync( $args ) {
		$term_id = $args[ 'term_id' ];

		$taxo = PostType\TaxonomyCalendar::instance();
		$sync_result = $taxo->sync( $term_id );

		return $sync_result;
	}

	/**
	 *	@action pp_calendar_pre_add_form
	 */
	public function print_create_form() {
		?>
		<div class="pp-calendar-create card">
			<form method="post" action="edit-tags.php">
				<input type="hidden" name="action" value="add-tag" />
				<input type="hidden" name="taxonomy" value="pp_calendar" />
				<?php wp_nonce_field('add-tag', '_wpnonce_add-tag'); ?>
				<input name="tag-name" type="hidden" value="new_calendar" />

				<h2><?php esc_html_e( 'New Subscription', 'calendar-importer' ) ?></h2>
				<div class="acf-field">
					<div class="acf-label">
						<label for="pp_cal_url">
							<?php esc_html_e('Calendar Feed URL','calendar-importer') ?>
						</label>
					</div>
					<div class="acf-input-wrap acf-url">
						<i class="acf-icon -globe -small"></i>
						<input type="url" id="pp_cal_url" class="" name="_pp_cal_url" value="" placeholder="" />
					</div>
					<p class="description"><?php
						esc_html_e( 'Paste the Calender Feed URL. The Calendar must be in the *.ics format.', 'calendar-importer' );
					?></p>
				</div>
				<p>
					<button type="submit" class="button button-primary">
						<?php esc_html_e('Subscribe to Calendar','calendar-importer') ?>
					</button>
				</p>
			</form>
		</div>
		<?php
	}
}
