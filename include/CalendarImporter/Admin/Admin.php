<?php

namespace CalendarImporter\Admin;
use CalendarImporter\Core;


class Admin extends Core\Singleton {

	private $core;

	private $calendars;

	/**
	 *	Private constructor
	 */
	protected function __construct() {

		$this->core = Core\Core::instance();
		$this->calendars = AdminCalendars::instance();

		add_action( 'admin_init', array( $this , 'admin_init' ) );
		add_action( 'admin_enqueue_scripts', array( $this , 'enqueue_assets' ) );
	}

	/**
	 *	@return array
	 */
	public function get_default_colors() {
		return apply_filters( 'pp_calendar_colors', array(
			__('Red','calendar-importer')			=> '#d88585',
			__('Orange','calendar-importer')		=> '#e79c70',
			__('Ochre','calendar-importer')			=> '#cbbb89',
			__('Light Green','calendar-importer')	=> '#b7c786',
			__('Green','calendar-importer')			=> '#86c794',
			__('Blue','calendar-importer')			=> '#72c0d6',
			__('Purple','calendar-importer')		=> '#8893ca',
			__('Pink','calendar-importer')			=> '#cb89c9',

//			Hellgrün   #b7c786
//			Grün	#86c794
//			Orange	#e79c70
//			Rot		#d88585
//			Ocker      #cbbb89
		));

	}

	/**
	 *	@return string color
	 */
	public function next_color() {
		$n = get_option( 'pp_calendar_color_counter' );
		$colors = array_values( $this->get_default_colors() );
		$color = $colors[ $n % count( $colors ) ];
		update_option('pp_calendar_color_counter', $n + 1 );
		return $color;
	}

	/**
	 *	@action admin_init
	 */
	function admin_init() {
	}

	/**
	 *	Enqueue options Assets
	 *
	 *	@action admin_enqueue_scripts
	 */
	function enqueue_assets() {

		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style( 'acf-datepicker', acf_get_url('assets/inc/datepicker/jquery-ui.min.css') );
		wp_enqueue_style( 'calendar-importer-admin', $this->core->get_asset_url( 'css/admin/admin.css' ), array(), $this->core->version() );

		wp_enqueue_script( 'calendar_importer-admin' , $this->core->get_asset_url( 'js/admin/admin.js' ), array('acf-input'), $this->core->version() );
		wp_localize_script('calendar_importer-admin' , 'calendar_importer_admin' , array(
			'l10n'	=> array(
				'now'	=> __( 'Just now', 'calendar-importer' ),
			),
			'colors'	=> array_values( $this->get_default_colors() ),
		) );
	}

}
