<?php


namespace CalendarImporter\Shortcode;

use CalendarImporter\Ajax;
use CalendarImporter\Core;
use CalendarImporter\Model;

class ShortcodeCalendar extends Shortcode {
	
	private $doing_calendar = false;
	
	protected $shortcode = 'pp_calendar';

	private $mce = null;

	private $output = '';

	/** @var Ajax\AjaxHandler */
	private $ajax_date;

	/** @var Ajax\AjaxHandler */
	private $ajax_calendar;

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {
		parent::__construct();

		$this->mce = Mce\MceCalendar::instance();


		add_action( 'get_template_part_calendar-importer/calendar', array( $this, 'get_template_part' ), 10, 2 );
		add_action( 'get_template_part_calendar-importer/event', array( $this, 'get_template_part' ), 10, 2 );
		add_action( 'get_template_part_calendar-importer/event-calendar', array( $this, 'get_template_part' ), 10, 2 );
		add_action( 'get_template_part_calendar-importer/events', array( $this, 'get_template_part' ), 10, 2 );
		add_action( 'get_template_part_calendar-importer/date', array( $this, 'get_template_part' ), 10, 2 );
		add_action( 'get_template_part_calendar-importer/time', array( $this, 'get_template_part' ), 10, 2 );
		add_action( 'get_template_part_calendar-importer/datetime', array( $this, 'get_template_part' ), 10, 2 );
		add_action( 'get_template_part_calendar-importer/part', array( $this, 'get_template_part' ), 10, 2 );


		$this->ajax_date = new Ajax\AjaxHandler('pp-date',array(
			'public'		=> true,
			'use_nonce'		=> false,
			'capability'	=> false,
			'callback'		=> array( $this, 'ajax_date' ),
			'method'		=> 'GET', // GET, POST, REQUEST
			'content_type'	=> 'text/html',
		));

		$this->ajax_calendar = new Ajax\AjaxHandler('pp-calendar',array(
			'public'		=> true,
			'use_nonce'		=> false,
			'capability'	=> false,
			'callback'		=> array( $this, 'ajax_calendar' ),
			'method'		=> 'REQUEST', // GET, POST, REQUEST
			'content_type'	=> 'text/html',
		));

	}


	/**
	 *	@action wp_ajax_pp-calendar
	 */
	public function ajax_calendar( $atts ) {

		header('Content-Type: text/html');

		echo $this->do_calendar( $atts );
		exit();

	}

	/**
	 *	@action wp_ajax_pp-date
	 */
	public function ajax_date( $atts ) {

		header('Content-Type: text/html');

		echo $this->do_date( $atts );

		exit();

	}

	/**
	 *	Allow Themes to override calendar templates
	 *
	 *	@action get_template_part_calendar-importer/*
	 */
	public function get_template_part( $name, $slug ) {
		$filenames = array( "{$name}-{$slug}.php", "{$name}.php" );

		if ( '' === locate_template( $filenames ) ) {
			foreach ( $filenames as $filename ) {
				$path = CALENDAR_IMPORTER_DIRECTORY . 'include/templates/' . $filename;
				if ( file_exists( $path ) ) {
					load_template( $path, false );
					break;
				}
			}
		}
	}


	/**
	 *	@inheritdoc
	 */
	public function do_shortcode( $atts, $content ) {
		
		return $this->do_calendar( $atts );

	}


	/**
	 *	Print Event
	 *
	 *	@action wp_ajax_do_date
	 *	@action wp_ajax_nopriv_do_date
	 */
	public function do_date( $atts ) {
	

		$cache_key = md5( serialize( $atts ) );
		$model = Model\ModelSchedules::instance();
		$cache = Model\ModelCache::instance();
		$renderer = Core\Renderer::instance();

		$use_cache = apply_filters( 'pp_debug_use_output_cache', true );

		if ( ! isset( $atts['dates'] ) ) {
			return '';
		}
		if ( ! $use_cache || ! $output = $cache->get( $cache_key ) ) {
			$tags = array();
			$dates = $model->sanitize_numeric_array( $atts['dates'] );
			if ( ! count($dates) ) {
				return '';
			}
//			foreach ( $dates as $date_id ) {
				// get event_id from date id!
//				$tags[] = 'event-'.$date_id;
//			}

			ob_start();
			$renderer->print_date( $atts );
			$output = ob_get_clean();
			$cache->set( $cache_key, $output, 0, $tags );
		}
		header('Content-Type: text/html');
		echo $output;
		exit();
	}

	/**
	 *	Print Calendar
	 *
	 *	@action wp_ajax_do_calendar
	 *	@action wp_ajax_nopriv_do_calendar
	 */
	public function do_calendar( $atts ) {

		if ( $this->doing_calendar === true ) {
			return '';
		}

		$this->doing_calendar = true;

		$cache_key = md5( serialize( $atts ) );
		$cache = Model\ModelCache::instance();


		$use_cache = apply_filters( 'pp_debug_use_output_cache', true );

		if ( ! isset( $atts['calendars'] ) || empty( $atts['calendars'] ) ) {
			return '';
		}

		$renderer = Core\Renderer::instance();

		if ( ! $use_cache || ! ( $output = $cache->get( $cache_key ) ) ) {

			$model = Model\ModelSchedules::instance();

			$tags = array();
			$cals = $model->sanitize_numeric_array( $atts['calendars'] );
			foreach ( $cals as $cal ) {
				$tags[] = 'cal-'.$cal;
			}

			$time_to_midnight = DAY_IN_SECONDS - strtotime( date('HisT') ) % DAY_IN_SECONDS;

			ob_start();

			$renderer->print_calendar( $atts );
			$renderer->print_color_styles();
			$output = ob_get_clean();

			$cache->set( $cache_key, $output, $time_to_midnight, $tags );
		}

		$this->doing_calendar = false;

		return $output;

	}

	private function ob_flushed( $buffer, $phase ) {
		$this->output = $buffer;
		return '';
	}



}
