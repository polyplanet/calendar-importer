<?php


namespace CalendarImporter\Shortcode\Mce;
use CalendarImporter\Core;
use CalendarImporter\PostType;
use CalendarImporter\Admin\Mce;

class MceCalendar extends Mce\Mce {

	/**
	 *	@inheritdoc
	 */
	protected $module_name = 'pp_calendar_shortcode';

	/**
	 *	@inheritdoc
	 */
	protected $editor_buttons = array(
		'mce_buttons_2' => array(
			'pp_calendar_shortcode'	=> -1,
		),
	);

	/**
	 *	@inheritdoc
	 */
	protected $toolbar_css	= true;

	/**
	 *	@inheritdoc
	 */
	protected $editor_css	= true;

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {
		// don't insert celandars in the event editor
		if ( PostType\PostTypeEvent::instance()->is_edit() ) {
			return;
		}

		// create a stub, so the scripts get enqueued
		$this->plugin_params = array(
			'l10n' => array(),
			'options'	=> array(),
		);

		add_action( 'init', array( $this, 'init' ), 11 );

		parent::__construct();
	}

	/**
	 *	@action init
	 */
	public function init() {

		$this->plugin_params = array(
			'l10n' => array(
				'insert'		=> __( 'Insert Calendar', 'calendar-importer' ),
				'edit'			=> __( 'Edit Calendar', 'calendar-importer' ),
				'name'			=> __( 'Calendar', 'calendar-importer' ),

				'calendars'		=> __( 'Calendars', 'calendar-importer'),

				'settings'		=> __( 'Settings', 'calendar-importer'),
				'display'		=> __( 'Display', 'calendar-importer'),

				'period'		=> __( 'Period', 'calendar-importer'),
				'day'			=> __( 'Day', 'calendar-importer'),
				'week'			=> __( 'Week', 'calendar-importer'),
				'workweek'		=> __( 'Working Week', 'calendar-importer'),
				'month'			=> __( 'Month', 'calendar-importer'),

				'this'			=> __( 'This', 'calendar-importer'),
				'next'			=> __( 'Next', 'calendar-importer'),
				'in'			=> __( 'In', 'calendar-importer'),
				'from'			=> __( 'From', 'calendar-importer'),
				'to'			=> __( 'To', 'calendar-importer'),

				'date'			=> __( 'Date', 'calendar-importer'),

				'template'		=> __( 'Layout', 'calendar-importer'),
				'calendar'		=> __( 'Calendar', 'calendar-importer'),
				'list'			=> __( 'List', 'calendar-importer'),
				'tiles'			=> __( 'Tiles', 'calendar-importer'),

				'list_size'		=> __( 'List Size', 'calendar-importer'),
				'tile_size'		=> __( 'Tile Size', 'calendar-importer'),
				'small'			=> __( 'Small', 'calendar-importer'),
				'large'			=> __( 'Large', 'calendar-importer'),

				'pick_day'		=> __( 'Date', 'calendar-importer' ),
				'in_day'		=> __( 'In ... Days', 'calendar-importer' ),

				'pick_week'		=> __( 'Week', 'calendar-importer' ),
				'in_week'		=> __( 'In ... Weeks', 'calendar-importer' ),

				'pick_month'	=> __( 'Month', 'calendar-importer' ),
				'in_month'		=> __( 'In ... Months', 'calendar-importer' ),

				'colorset'		=> __( 'Colors', 'calendar-importer' ),

				'show_event'	=> __( 'Show Single Event', 'calendar-importer' ),

				'paging'		=> __( 'Navigation', 'calendar-importer' ),

			),
			'options'	=> array(
				'calendars'		=> array(),
				'periods'		=> array(
					'day'			=> __( 'Day', 'calendar-importer'),
					'week'			=> __( 'Week', 'calendar-importer'),
					'workweek'		=> __( 'Working Week', 'calendar-importer'),
					'month'			=> __( 'Month', 'calendar-importer'),
				),
				'templates'		=> array(
					'calendar'		=> __( 'Calendar', 'calendar-importer'),
					'list'			=> __( 'List', 'calendar-importer'),
					'tiles'			=> __( 'Tiles', 'calendar-importer'),
				),
				'list_sizes'	=> array(
					'normal'		=> __( 'Normal', 'calendar-importer'),
					'compact'		=> __( 'Compact', 'calendar-importer'),
				),
				'tile_sizes'	=> array(
					'small'			=> __( 'Small', 'calendar-importer'),
					'large'			=> __( 'Large', 'calendar-importer'),
				),

				'days'			=> array(
					'today'			=> __( 'Today', 'calendar-importer' ),
					'tomorrow'		=> __( 'Tomorrow', 'calendar-importer' ),
					'plus_days'		=> __( 'In ... Days', 'calendar-importer' ),
					'day'			=> __( 'Select Date', 'calendar-importer' ),
				),
				'weeks'			=> array(
					'this_week'		=> __( 'This Week', 'calendar-importer' ),
					'next_week'		=> __( 'Next Week', 'calendar-importer' ),
					'plus_weeks'	=> __( 'In ... Weeks', 'calendar-importer' ),
					'week'			=> __( 'Select Week', 'calendar-importer' ),
				),
				'months'		=> array(
					'this_month'	=> __( 'This Month', 'calendar-importer' ),
					'next_month'	=> __( 'Next Month', 'calendar-importer' ),
					'plus_months'	=> __( 'In ... Months', 'calendar-importer' ),
					'month'			=> __( 'Select Month', 'calendar-importer' ),
				),
				'colors'		=> array(
					'dark'			=> __( 'Dark', 'calendar-importer' ),
					'light'			=> __( 'Light', 'calendar-importer' ),
				),
				'show_event'	=> array(
					'permalink'		=> __( 'Permalink', 'calendar-importer' ),
					'inline'		=> __( 'Inline', 'calendar-importer' ),
					'lightbox'		=> __( 'Lightbox', 'calendar-importer' ),
				),
				'paging'		=> array(
					'scroll'		=> __( 'Scroll', 'calendar-importer' ),
					'turn'			=> __( 'Turn Pages', 'calendar-importer' ),
				),
			)
		);

		$taxonomy	= 'pp_calendar';
		$calendars	= array();

		$calendar_terms = get_terms(array(
			'taxonomy'		=> $taxonomy,
			'hide_empty'	=> false,
		));

		foreach ( $calendar_terms as $calendar_term ) {
			$calendars[ ] = array(
				'id'	=> $calendar_term->term_id,
				'label'	=> $calendar_term->name,
				'color'	=> get_field( 'field_pp_cal_color', $taxonomy . '_' . $calendar_term->term_id )
			);
		}

		$this->plugin_params['options']['calendars'] = $calendars;

	}
}
