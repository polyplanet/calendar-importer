<?php

namespace CalendarImporter\Widget;
use CalendarImporter\Core;

class CalendarWidget extends \WP_Widget {



	/**
	 * Widget Constructor
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'		=> 'pp-calendar',
			'description'	=> __( 'Shows Events from your Calendar', 'calendar-importer' ),
		);
		parent::__construct( 'pp-calendar', __( 'Show Events', 'calendar-importer' ), $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$renderer = Core\Renderer::instance();

		$period_field = get_field('period','widget_'.$this->id);
		$layout_field = get_field('display','widget_'.$this->id);

		if ( ! is_array( $period_field ) || ! is_array( $layout_field ) ) {
			return;
		}

		// wp doesn't escape output either!
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		$period = $period_field["period"];
		if ( 'workweek' === $period || 'workingweek' === $period ) { // workingweek: legacy bug
			$period_data = $period_field['week'];
		} else if ( isset( $period_field[$period] ) ) {
			$period_data = $period_field[$period];
		} else {
			// fallback
			$period = 'day';
			$period_data = array(
				'from_days' => 'today',
				'from_in_days'	=> '',
				'to_days' => 'tomorrow',
				'to_in_days'	=> '',
			);
		}

		$attr = array(
			'calendars'	=> get_field('calendars','widget_'.$this->id),
			'period'	=> $period,
			'limit'		=> get_field('limit','widget_'.$this->id),
		) + $period_data + $layout_field;

		$renderer->print_calendar($attr);

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php

		// #### widget options here ####

	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$new_instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		// #### widget option validation here ####

		return $new_instance;
	}


}
