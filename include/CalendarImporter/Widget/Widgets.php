<?php

namespace CalendarImporter\Widget;
use CalendarImporter\Core;

class Widgets extends Core\Singleton {

	/**
	 * Private constructor
	 */
	protected function __construct() {
		parent::__construct();
		add_action('widgets_init', array( $this, 'widgets_init' ) );
	}

	public function widgets_init(){
		register_widget("CalendarImporter\Widget\CalendarWidget");
	}

}
