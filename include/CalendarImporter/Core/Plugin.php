<?php

namespace CalendarImporter\Core;

use CalendarImporter\PostType;
use CalendarImporter\Compat;

class Plugin extends Singleton {

	private $info = null;


	private $_version = null;


	private static $components = array(
//		'CalendarImporter\Compat\ACF',
		'CalendarImporter\Model\ModelSchedules',
		'CalendarImporter\PostType\PostTypeEvent',
		'CalendarImporter\PostType\TaxonomyCalendar',
		'CalendarImporter\Cron\Cron',
	);

	/**
	 *	Private constructor
	 */
	protected function __construct() {

		register_activation_hook( CALENDAR_IMPORTER_FILE, array( __CLASS__ , 'activate' ) );
		register_deactivation_hook( CALENDAR_IMPORTER_FILE, array( __CLASS__ , 'deactivate' ) );
		register_uninstall_hook( CALENDAR_IMPORTER_FILE, array( __CLASS__ , 'uninstall' ) );

		parent::__construct();
		add_action( 'cli_init', array( __CLASS__, 'maybe_upgrade_components' ) );
		add_action( 'admin_init', array( __CLASS__, 'maybe_upgrade_components' ) );
		add_action( 'wpmu_upgrade_site', array( __CLASS__, 'maybe_upgrade_components' ) );
	}

	
	/**
	 *	@return string plugin version
	 */
	public function version() {
		if ( is_null( $this->_version ) ) {
			$this->_version = include_once CALENDAR_IMPORTER_DIRECTORY . 'include/version.php';
		}
		return $this->_version;
	}


	/**
	 *	Run component upgrade if version changed
	 */
	public static function maybe_upgrade_components() {
		// trigger upgrade

		$new_version = self::instance()->version();
		$old_version = get_option( 'calendar_importer_version' );

		if ( $new_version != $old_version ) {
			self::upgrade( $new_version, $old_version );
		}
		update_option( 'calendar_importer_version', $new_version );
	}

	/**
	 *	Fired on plugin activation
	 */
	public static function activate() {

//		$core = Core::instance();

		if ( ! class_exists( 'acf' ) ) {
			// try to activate acf pro
			$result = activate_plugins( 'advanced-custom-fields-pro/acf.php' );

			if ( is_wp_error( $result ) ) {
				// deactivate self
				deactivate_plugins( basename( CALENDAR_IMPORTER_FILE ) );
				//add_action('admin_notices', array( $core, 'acf_unavailable_notice' ) );
			} else {
				// activate qef
				activate_plugins( 'acf-quick-edit-fields/index.php' );
			}
		}
		self::maybe_upgrade_components();

	}


	/**
	 *	@param string $nev_version
	 *	@param string $old_version
	 *	@return array(
	 *		'success' => bool,
	 *		'messages' => array,
	 * )
	 */
	public static function upgrade( $new_version, $old_version ) {

		$result = array(
			'success'	=> true,
			'messages'	=> array(),
		);

		foreach ( self::$components as $component ) {
			$comp = $component::instance();
			$comp->upgrade( $new_version, $old_version );
		}

		return $result;
	}

	/**
	 *	Fired on plugin deactivation
	 */
	public static function deactivate() {
		foreach ( self::$components as $component ) {
			$comp = $component::instance();
			$comp->deactivate();
		}
	}

	/**
	 *	Fired on plugin deinstallation
	 */
	public static function uninstall() {
		foreach ( self::$components as $component ) {
			/*
			$comp = $component::instance();
			$comp->unistall();
			/*/
			$component::uninstall();
			//*/
		}
	}

}
