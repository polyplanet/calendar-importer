<?php

namespace CalendarImporter\Core;

use CalendarImporter\Admin;
use CalendarImporter\Compat;
use CalendarImporter\Cron;
use CalendarImporter\Model;
use CalendarImporter\PostType;
use CalendarImporter\Shortcode;
use CalendarImporter\Widget;

class Core extends Plugin {

	private $_version = null;

	/**
	 *	Private constructor
	 */
	protected function __construct() {

		add_action( 'plugins_loaded' , array( $this , 'init_compat' ), 0 );
		add_action( 'plugins_loaded' , array( $this , 'load_textdomain' ) );
		parent::__construct();
	}
	
	/**
	 *	Register frontend styles and scripts
	 */
	public function register_scripts() {
		$script_deps = array('jquery' );
		$style_deps = array();
		if ( is_plugin_active('acf-openstreetmap-field/index.php')) {
			$script_deps[] = 'acf-osm-frontend';
			$style_deps[] = 'leaflet';
		}

		wp_register_style( 'pp-calendar', $this->get_asset_url('css/calendar-importer.css'), $style_deps, $this->version() );
		wp_register_script( 'pp-calendar', $this->get_asset_url('js/calendar-importer.js'), $script_deps, $this->version(), true );
		wp_localize_script( 'pp-calendar', 'pp_calendar', array(
			// whatever
			'l10n'		=> array(
				/* translators: stamen.com */
				'map_attribution'	=> sprintf( __('Map tiles by <a href="%s">Stamen Design</a>', 'calendar-importer' ),'http://stamen.com') .
					'<a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; ' .
					/* translators: OpenStreetMap.org */
					sprintf( __( 'Map Data &copy; <a href="%s">OpenStreetMap</a>', 'calendar-importer' ), 'http://www.openstreetmap.org/copyright' ),
				'close'	=> __('Close','calendar-importer'),
			),
			'options'	=> array(
				'ajax_url'	=> admin_url('admin-ajax.php'),
				'map'	=> array(
					'tilesets'	=> array(
						'light'	=> 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png',
						'dark'	=> 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png',
					)

				),
			)
		) );
	}
	/**
	 *	Load frontend styles and scripts
	 *
	 *	@action wp_enqueue_scripts
	 */
	public function enqueue_scripts() {

		$this->register_scripts();

		wp_enqueue_style('pp-calendar');
		wp_enqueue_script('pp-calendar');

	}




	/**
	 *	Load Compatibility classes
	 *
	 *  @action plugins_loaded
	 */
	public function init_compat() {
		\PosttypeTermArchive\Core\Core::instance();

		if ( class_exists( 'acf' ) ) {
			Compat\ACF::instance();
			$this->init();
		} else {
			if ( is_admin() && current_user_can( 'activate_plugins' ) ) {
				add_action('admin_notices', array( $this, 'acf_unavailable_notice' ) );
				//deactivate_plugins( CALENDAR_IMPORTER_FILE );
			}
		}
	}


	public function acf_unavailable_notice() {
		if ( current_user_can( 'activate_plugins' ) ) {
			?><div class="notice notice-error">

				<p><?php esc_html_e( 'The plugin <strong>Advanced Custom Fields PRO</strong> is not active.' , 'calendar-importer' ); ?></p>

				<p><?php esc_html_e(' The Calandar Plugin will not work without it. ', 'calendar-importer' ); ?></p>

			</div><?php
		}
	}


	/**
	 *	Load text domain
	 *
	 *  @action plugins_loaded
	 */
	public function load_textdomain() {
		$path = pathinfo( dirname( CALENDAR_IMPORTER_FILE ), PATHINFO_FILENAME );
		load_plugin_textdomain( 'calendar-importer' , false, $path . '/languages' );
	}

	/**
	 *	Init hook.
	 *
	 *  @action plugins_loaded
	 */
	public function init() {

		$renderer = Renderer::instance();

		
		add_action( 'admin_enqueue_scripts', array( $this , 'register_scripts' ), 1 );
		add_action( 'wp_enqueue_scripts' , array( $this , 'enqueue_scripts' ), 99 );

		add_action( 'wp_head', array( $renderer, 'print_color_styles' ) );


		Model\ModelSchedules::instance();

		PostType\PostTypes::instance();

		Widget\Widgets::instance();

		Cron\Cron::instance();

		Shortcode\ShortcodeCalendar::instance();


		if ( is_admin() || defined( 'DOING_AJAX' ) ) {



			Admin\Admin::instance();
			/*
			Admin\Tools::instance();
			Admin\Settings::instance();
			*/
		}
	}

	/**
	 *	Get asset url for this plugin
	 *
	 *	@param	string	$asset	URL part relative to plugin class
	 *	@return wp_enqueue_editor
	 */
	public function get_asset_url( $asset ) {
		return plugins_url( $asset, CALENDAR_IMPORTER_FILE );
	}


}
