<?php

namespace CalendarImporter\Core;

abstract class PluginComponent extends Singleton {

	private static $upgraded_messages = array();

	abstract function activate();
	abstract function upgrade( $new_version, $old_version );
	abstract function deactivate();
	static function uninstall(){}



	/**
	 *	@param string $message
	 *	@param bool $success
	 */
	protected function add_upgraded_message( $message, $success = true ) {
		self::$upgraded_messages[] = (object) array(
			'message'	=> $message,
			'success'	=> $success,
		);
		pp_cal_debug( '[CALENDAR_IMPORTER] ' . $message );
	}
	protected function get_upgraded_messages( $success_state = null ) {
		$messages = array();
		foreach ( self::$upgraded_messages as $message ) {
			if ( is_null( $success_state ) || $message->success === $success_state ) {
				$messages[] = $message->message;
			}
		}
		return $messages;
	}

	protected function get_upgraded_success_state( ) {

		foreach ( self::$upgraded_messages as $message ) {
			if ( ! $message->success ) {
				return false;
			}
		}
		return true;
	}

}
