<?php

namespace CalendarImporter\Core;

use CalendarImporter\Compat;
use CalendarImporter\Cron;
use CalendarImporter\PostType;

class Renderer extends Singleton {

	/**
	 * @inheritdoc
	 */
	protected function __construct() {

		parent::__construct();

		if ( ! has_action( 'wp_footer', array( $this, 'print_js_templates' ) ) ) {
			add_action( 'wp_footer', array( $this, 'print_js_templates' ) );
		}

	}

	/**
	 *	@action wp_head
	 */
	public function print_color_styles() {
		global $wpdb;

		// get all colors
		$colors = array(
			//get_field('calendar_options_default_color'),
		);
		$cal_colors = $wpdb->get_col( "SELECT DISTINCT meta_value FROM $wpdb->termmeta WHERE meta_key = 'pp_cal_color'" );
		$colors = array_merge( $cal_colors, $colors );

		$event_colors = $wpdb->get_col( "SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = 'pp_event_color'" );
		$colors = array_merge( $event_colors, $colors );
		$colors = array_unique( $colors );

		$colors = array_filter( $colors, array( $this, 'valid_color' ) );

		if ( empty( $colors ) ) {
			return;
		}

		// generate css
		$css = '';
		foreach ( $colors as $color ) {
			$key = strtolower(substr( $color, 1 ));
			$css .= sprintf( 
				'.pp-color-%1$s, .pp-color-day-%1$s .pp-color-day { color:#%s; }' . "\n", 
				sanitize_html_class( $key ),
				sanitize_key( $color )
			);
			$css .= sprintf( 
				'.pp-link-color-%s a { color:#%s; }' . "\n", 
				sanitize_html_class( $key ),
				sanitize_key( $color )
			);
			$css .= sprintf( 
				'.pp-border-%s { border-color:#%s; }' . "\n", 
				sanitize_html_class( $key ),
				sanitize_key( $color )
			);
			$css .= sprintf( 
				'.pp-background-%s { background-color:#%s; }' . "\n", 
				sanitize_html_class( $key ),
				sanitize_key( $color )
			);
		}

		if ( wp_doing_ajax() ) {
			?>
<style type="text/css">
<?php echo $css; ?>
</style>
			<?php
		} else {
			$handle = 'pp-calendar-color-styles';
			wp_register_style( $handle, false, [], false );
			
			wp_add_inline_style( $handle, $css );
			wp_enqueue_style( $handle );
		}
	}

	/**
	 *  Whether a color is a valid hex color
	 *
	 *	@return bool
	 */
	public function valid_color( $color ) {
		return is_string( $color ) && preg_match('/#([0-9a-f]{6})/i', $color ) === 1;
	}




	/**
	 *  Print a date
	 *
	 *	@param array $atts	see ModelSchedule::query_events
	 */
	public function print_date( $atts ) {

		global $wp_embed;

		$model = pp_schedule();

		$atts = wp_parse_args( $atts, array(
			'colors' => 'light',
		));

		if ( $model->query_events( $atts ) ) {
			$autoembed_filter = false;

			if ( $autoembed_filter = has_filter('the_content',array( $wp_embed, 'autoembed' ) ) ) {
				remove_filter( 'the_content', array( $wp_embed, 'autoembed' ), $autoembed_filter );
			}

			while ( pp_have_dates() ) {
				pp_the_date();
				get_template_part( 'calendar-importer/event', 'body' );
			}

			if ( $autoembed_filter ) {
				add_filter('the_content',array( $wp_embed, 'autoembed' ), $autoembed_filter );
			}
		}
		$model->reset(); // ~200k
	}



	/**
	 *  Print a calendar
	 *
	 *	@param array $atts	+ see ModelSchedule::query_events
	 */
	public function print_calendar( $atts ) {


		$atts = wp_parse_args( $atts, array(
			'template'			=> 'list',
			'tile_size'			=> 'small',
			'colors'			=> 'light',
			'open'				=> 'permalink',

			// calendar only
			'paging'			=> 'scroll',
		));

		/*
		$model = Model\ModelSchedules::instance();
		/*/
		$model = pp_schedule();
		//*/

		$atts['distinct-events'] = $atts['template'] === 'tiles';

		if ( $atts['template'] === 'calendar' && $atts['paging'] !== 'scroll' ) {

			$pages_args = $model->get_pages_args( $atts );

			if ( count( $pages_args ) > 1 ) {
				echo '<div class="pp-pages">';
			}

			foreach ( $pages_args as $args ) {
				if ( $model->query_events( $args ) ) {

					get_template_part( 'calendar-importer/events', 'calendar' );

				}
			}

			if ( count( $pages_args ) > 1 ) {
				echo '	<div class="pp-pages-controls">';
				printf( 
					'<button class="pp-icon-button pp-icon-arrow-left" data-action="prev"><span class="screen-reader-text">%s</span></button>',
					esc_html__( 'Previous', 'calendar-importer' )
				);
				printf( 
					'<button class="pp-icon-button pp-icon-arrow-right" data-action="next"><span class="screen-reader-text">%s</span></button>',
					esc_html__( 'Next', 'calendar-importer' )
				);
				echo '	</div>';
				echo '</div>';
			}

		} else {
			if ( $model->query_events( $atts ) ) {

				get_template_part( 'calendar-importer/events', $atts['template'] );

			}
		}
		$model->reset(); // ~200k

		unset($model);
		unset($atts);
		unset($autoembed_filter);


	}
	
	/**
	 *	@action wp_footer
	 */
	public function print_js_templates() {

		get_template_part( 'calendar-importer/part','js-templates' );
	}



}
