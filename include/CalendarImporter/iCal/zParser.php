<?php

namespace CalendarImporter\iCal;

use CalendarImporter\Core;


class zParser extends Parser {

	private $cals = array();
	private $events = array();


	/**
	 *	@inheritdoc
	 */
	public function fetch( $url, $cached = true, $expire = 900 ) { // 15 min expire

		$url = esc_url( $url, array( 'http', 'https', 'webcal' ) );

		if ( ! isset( $this->cals[$url] ) ) {

			$response = $this->fetchUrl( $url, $cached, $expire );

			if ( is_wp_error( $response ) ) {
				pp_cal_debug( '[CALENDAR_IMPORTER] ' . implode( ', ', $response->get_error_messages() ) );
				return $response;
			}

			try {
				$this->cals[ $url ] = new zCalendar( $response );
				$this->cals[ $url ]->setUrl( $url );
			} catch( \Exception $e ) {
				pp_cal_debug( '[CALENDAR_IMPORTER] ' . $e->getMessage() );
				pp_cal_debug( '[CALENDAR_IMPORTER] ' . $e->getTraceAsString() );
				return new \WP_Error( 'pp-calendar', __( 'Error while parsing Calendar Data', 'calendar-importer' ) );
			}
		}

		pp_cal_debug( '[CALENDAR_IMPORTER] calendar data parsed' );
		return $this->cals[ $url ];

	}



}
