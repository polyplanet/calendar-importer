<?php

namespace CalendarImporter\iCal;

use CalendarImporter\Core;

// trigger autoload
new \ZCiCal();

class zCalendar extends \ZCiCal {

	private $url;
	private $timezone;

	public function setUrl($url){
		$this->url = $url;
	}
	public function getUrl(){
		return $this->url;
	}
	public function calendarName() {
		if ( isset( $this->tree->data['X-WR-CALNAME'] ) ) {
			return $this->tree->data['X-WR-CALNAME']->getValues();
		}
		return parse_url( $this->url, PHP_URL_HOST );
	}

	public function calendarTimezone() {
		if ( isset( $this->timezone ) ) {
			return $this->timezone;
		}

		foreach ($this->tree->child as $node) {
			// get tz node
			if ( $node->getName() === 'VTIMEZONE' && isset( $node->data['TZID'] ) ) {
				$tz = $node->data['TZID']->getValues();
				break;
			}
		}

		if ( isset( $tz ) ) {
			$test_tz = [ $tz ];
			try {
				$wtz = \IntlTimeZone::getIDForWindowsID( $tz );
				$test_tz[] = $wtz;
			} catch ( \IntlException $ex ) {}
			foreach ( $test_tz as $tz_candidate ) {
				try {
					new \DateTimeZone( $tz_candidate ); // wp timezone?
					$this->timezone = $tz_candidate;
					break;
				} catch ( \Exception $err ) { }
			}
		}

		if ( ! isset( $this->timezone ) ) {
			$this->timezone = false;
		}
		return $this->timezone;
	}

	public function calendarDescription() {
		if ( isset( $this->tree->data['X-WR-CALDESC'] ) ) {
			return $this->tree->data['X-WR-CALDESC']->getValues();
		}
	}

	public function calendarColor() {
		if ( isset( $this->tree->data['X-APPLE-CALENDAR-COLOR'] ) ) {
			$color = $this->tree->data['X-APPLE-CALENDAR-COLOR']->getValues();
			return preg_replace( '/^(#[0-9A-F]{6})(.*)$/i', '\1', $color );
		}
	}

	public function events( $skip_recurrences = true ) {
		$events = array();
		if ( isset( $this->tree->child ) ) {
			foreach( $this->tree->child as $node) {
				if ( $node->getName() == 'VEVENT' ) {
					$event = new zEvent( $node, $this );
					if ( ! $skip_recurrences || ! $event->isRecurrence() ) {
						$events[] = $event;
					}
				}
			}
		}
		return $events;
	}

}
