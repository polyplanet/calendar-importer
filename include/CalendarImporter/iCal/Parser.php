<?php

namespace CalendarImporter\iCal;

use CalendarImporter\Core;
use ICal as VendorICal;


abstract class Parser extends Core\Singleton {

	/**
	 *	@param	string	$url	iCal URL
	 *	@param	bool	$cached	Get result from cache
	 *	@param	int		$expire	cache expiration time
	 *	@return	WP_Error|zCalendar
	 */
	abstract function fetch( $url, $cached = true, $expire = 900 );

	/**
	 *	@return WP_Error|string
	 */
	protected final function fetchUrl( $url, $cached = true, $expire = 900 ) { // expire 15 min.

		$key		= md5( $url );
		$now		= time();
		$data_key	= '_pp_cal_fetch_data_' . $key;
		$status_key	= '_pp_cal_fetch_status_' . $key;
		$time_key	= '_pp_cal_fetch_time_' . $key;

		$last_fetch		= get_site_transient( $time_key );
		$last_data		= get_site_transient( $data_key );
		$last_status	= get_site_transient( $status_key );
		$is_expired		= $last_fetch < ( $now - $expire );

		
		pp_cal_debug('[CALENDAR_IMPORTER] Calendar fetch');
		// fetch
		if ( ! $cached || $is_expired ) {
			/** @var str */
			$data	= $this->fetchICalData( $url );
			if ( is_wp_error( $data ) ) {
				set_site_transient( $status_key, $data );
				// return cached data if available
				pp_cal_debug('[CALENDAR_IMPORTER] Calendar fetch ERROR: '.$data->get_error_message() );
				if ( $last_data ) {
					$data = $last_data;
				}
			} else {
				set_site_transient( $data_key, $data );
				set_site_transient( $status_key, true );
			}
			set_site_transient( $time_key, $now );
			return $data;
		}

		// serve from cache
		if ( ! $is_expired && $last_data ) {
			pp_cal_debug('[CALENDAR_IMPORTER] serve from cache');
			return $last_data;
		}

		// Not expired, last request was an error
		if ( is_wp_error( $last_status ) ) {
			pp_cal_debug('[CALENDAR_IMPORTER] serve error from cache');
			return $last_status;
		}

		pp_cal_debug('[CALENDAR_IMPORTER] unknown err');

		// Not expired, last request was good, no last data cached. Very unlikely error case.
		return new \WP_Error('pp-cal-err', __( 'Unkonwon error', 'calendar-importer' ) );
	}


	/**
	 *	@param	string	$url
	 *	@return	WP_Error|string
	 */
	private function fetchICalData( $url ) {
		pp_cal_debug( '[CALENDAR_IMPORTER] Calendar fetchICalData' );
		pp_cal_debug( '[CALENDAR_IMPORTER] Fetch ' . $url );

		$url		= set_url_scheme( $url, 'https' );
		$response	= wp_remote_get( $url, array(
			// some settings ...
			'timeout' => 30,
		) );
		// request failed
		if ( is_wp_error( $response ) ) {
			pp_cal_debug( '[CALENDAR_IMPORTER] ' . $response->get_error_message() );
			return $response;
		}

		$data = $response['body'];

		// request failed
		if ( false === strpos( $data, 'BEGIN:VCALENDAR' ) ) {
			pp_cal_debug('[CALENDAR_IMPORTER] no ical response');
			return new \WP_Error( 'pp-cal-no-ical-data', __('No calendar data','calendar-importer') );
		}
		pp_cal_debug('[CALENDAR_IMPORTER] Fetched');
		return $data;
	}


}
