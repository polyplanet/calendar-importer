<?php

namespace CalendarImporter\iCal;

use RRule\RRule;
use RRule\RfcParser;

class zEvent {

	/**
	 *	@var	ZCiCalDataNode
	 */
	private $node;

	/**
	 *	@var	CalendarImporter\iCal\zCalendar
	 */
	private $calendar;

	/**
	 *	@var	array
	 */
	private $dates = null;

	/**
	 *	@param	ZCiCalNode	$node
	 */
	public function __construct( $node, $calendar ) {
		$this->node = $node;
		$this->calendar = $calendar;
	}

	
	/**
	 *	@param	string	$what
	 *	@return DateTime
	 */
	private function getDateTime( $what ) {
		if ( isset( $this->node->data[$what] ) ) {
			if ( $what === 'DTSTART' || $what === 'DTEND' ) {
				// return local time!
				$dt = $this->node->data[$what]->getValues();

				if ( $this->isAllDay() ) {
					// ignore Timezone
					$dtime = new \DateTime( $dt );
				} else {
					
					$tzone = $this->getTimeZone( $what );
					// convert to UTC ...
					$dtime = new \DateTime( $dt, $tzone );
					$dtime->setTimezone( new \DateTimeZone('UTC') );					
				}
				return $dtime;
			}
		}
	}

	/**
	 *	@param Boolean|String $what Whether to lookup a timezone in DTSTART or DTEND too
	 *	@return DateTimeZone of the event or calendar
	 */
	public function getTimeZone( $what = false ) {

		$tzid = false;

		if ( isset( $this->node->data[$what] ) ) {
			$tzid = @$this->node->data[$what]->getParameter('tzid');
		}

		$test_timezones = [
			// $tzid, // date timezone
		];
		if ( $tzid !== false && ! is_null( $tzid ) ) {
			$test_timezones[] = $tzid;
			try {
				if ( $tzid !== false ) {
					$wtz = \IntlTimeZone::getIDForWindowsID( $tzid );
					$test_timezones[] = $wtz;
				}
			} catch ( \IntlException $ex ) {}
		}

		$test_timezones[] = $this->calendar->calendarTimezone(); // calandar timezone
		$test_timezones[] = pp_get_timezone_option(); // WP settings timezone


		$test_timezones = array_unique( array_filter( $test_timezones ) );

		foreach ( $test_timezones as $tz_candidate ) {
			try {
				$tzone = new \DateTimeZone( $tz_candidate ); // wp timezone?
				return $tzone;
				break;
			} catch ( \Exception $err ) {}
		}
		pp_cal_debug( '[CALENDAR_IMPORTER] Timezone invalid: '.$what.' candidates '.var_export( $test_timezones, true ) );
		return new \DateTimeZone( date_default_timezone_get() );
	}


	/**
	 *	@param	string	$what
	 *	@return	mixed
	 */
	public function __get( $what ) {
		$what = strtoupper( $what );
		if ( isset( $this->node->data[$what] ) ) {

			// return date/time values as UTC
			if ( $what === 'DTSTART' || $what === 'DTEND' ) {

				$dtime = $this->getDateTime( $what );
				return $dtime->format('Ymd\THis');
			}
			return $this->node->data[$what]->getValues();
		}
	}

	/**
	 *	@return boolean
	 */
	public function isPast() {
		foreach ( $this->getDates() as $date ) {
			if ( ! $date['is_past'] ) {
				return false;
			}
		}
		return true;
	}


	/**
	 *	@return boolean
	 */
	public function isBefore( \DateTime $beforeDate ) {
		$before = $beforeDate->getTimestamp();
		foreach ( $this->getDates() as $date ) {
			if ( $date['dtend_timestamp'] >= $before ) {
				return false;
			}
		}
		return true;
	}

	/**
	 *	@return boolean
	 */
	public function isRecurring() {
		return ! is_null( $this->rrule );
	}

	/**
	 *	@return boolean
	 */
	public function isRecurrence() {
		return isset( $this->node->data['RECURRENCE-ID'] );
	}

	/**
	 *	@return boolean
	 */
	public function isAllDay() {
		return @$this->node->data["DTSTART"]->getParameter('value') === 'DATE';
	}


	/**
	 *	Dump Event props
	 */
	public function dump() {
		foreach ( $this->node->data as $key => $value ) {
			if ( is_array( $value ) ) {
				for ($i = 0; $i < count($value); $i++) {
					$p = $value[$i]->getParameters();
					echo "  $key $i: " . $value[$i]->getValues() . "\n";
				}
			} else {
				echo "  $key: " . $value->getValues() . "\n";
			}
		}
	}

 	/**
 	 *	@param	string	$dt_format
 	 *	@param	string	$d_foramt
 	 *	@return	array
 	 */
	public function getDates( $dt_format = 'Y-m-d H:i:s', $d_format = 'Ymd' ) {
		if ( is_null( $this->dates ) ) {
			$this->dates	= array();
			$day			= 86400;
			$now			= time();
			$now_day		= intval( date('Ymd',$now ) );
			$is_all_day		= $this->isAllDay();

			if ( ! is_null( $this->rrule ) ) {
				$recurrances = $this->getRecurrances();
			} else {
				$recurrances = array(
					array(
						'dtstart'	=> strtotime($this->dtstart),
						'dtend'		=> strtotime($this->dtend),
					)
				);
			}

			foreach ( $recurrances as $recurrance ) {
				$dtend_day = intval( date('Ymd', $recurrance['dtend'] ) );
				$this->dates[] = array(
					'is_all_day'		=> $is_all_day,//(0 == ( $recurrance['dtstart'] % DAY_IN_SECONDS ),
					'dtstart_date'		=> date( $d_format, $recurrance['dtstart'] ),
					'dtend_date'		=> date( $d_format, $recurrance['dtend'] ),
					'dtstart_datetime'	=> date( $dt_format, $recurrance['dtstart'] ),
					'dtend_datetime'	=> date( $dt_format, $recurrance['dtend'] ),
					'dtstart_timestamp'	=> $recurrance['dtstart'],
					'dtend_timestamp'	=> $recurrance['dtend'],
					'is_past'			=> date( 'Ymd', $now ) > $dtend_day,
				);
			}
		}
		return $this->dates;
	}

	/**
	 *	@return	array containing unix timestamps of dtstart and dtend
	 */
	private function getRecurrances() {
		$recurrances	= [];
		$dtstart_ts		= strtotime( $this->dtstart );
		$dtend_ts		= strtotime( $this->dtend );
		$duration		= $dtend_ts - $dtstart_ts;

		/*
		$end_date		= strtotime( '+1 year' );

		$rd = new \ZCRecurringDate( $this->rrule, strtotime( $this->dtstart ) );

		foreach ( $rd->getDates( $end_date ) as $i => $dtstart ) {
			if ( ! isset( $recurrances[$i] ) ) {
				$recurrances[$i] = [];
			}
			$recurrances[$i]['dtstart']	= $dtstart;
			$recurrances[$i]['dtend']	= $dtstart + $duration;
		}
		/*/
		try {
			$args = RfcParser::parseRRule($this->rrule);			
		} catch ( \Exception $err ) {
			error_log( $err->getMessage() );
			error_log( "RRULE: $this->rrule" );
		}
		$args['DTSTART'] = $this->getDateTime( 'DTSTART' );
		$args['DTSTART']->setTimeZone( $this->getTimeZone('DTSTART') );
		if ( ! isset( $args['UNTIL'] ) && ! isset( $args['COUNT'] ) ) {
			$args['UNTIL'] = new \DateTime('+1 year');
			$args['UNTIL']->setTimeZone( $this->getTimeZone('DTSTART') );
		}
		try {
			$rrule = new RRule( $args );			
		} catch( \Exception $err ) {
			throw new Exception( sprintf( 
				'Failed to create rrule “%s” from calendar URL “%s”. Reason: %s',
				$this->rrule,
				$this->calendar->getUrl(),
				$err->getMessage()
			) );
		}

		foreach ( $rrule as $i => $occurance ) {
			$dtstart = $occurance->format('U');
			$recurrances[$i]['dtstart']	= $dtstart;
			$recurrances[$i]['dtend']	= $dtstart + $duration;
		}
		//*/

		return $recurrances;
	}


}
