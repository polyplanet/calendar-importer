<?php

namespace CalendarImporter\ACF\Block;

use CalendarImporter\Core;

abstract class Block extends Core\Singleton {

	/**
	 *	@var string block name
	 */
	protected $slug;

	/**
	 *	@var string block title
	 */
	protected $title;

	/**
	 *	@var string block category
	 */
	protected $category;

	/**
	 *	@var string block icon
	 */
	protected $icon;

	/**
	 *	@var string block description
	 */
	protected $description = '';

	/**
	 *	@var string block description
	 */
	protected $align = 'full';

	/**
	 *	@var string block description
	 */
	protected $mode = 'preview';

	/**
	 *	@var string block description
	 */
	protected $keywords = [];

	/**
	 *	@inheritdoc
	 */
	protected function __construct( ) {
		if ( doing_action('acf/init') ) {
			$this->register_block();
		} else {
			add_action( 'acf/init', [ $this, 'register_block' ] );
		}
	}

	/**
	 *	@action acf/init
	 */
	public function register_block() {

		// register a calendar block
		acf_register_block_type(array(
			'name'				=> $this->slug,
			'title'				=> $this->title,
			'description'		=> $this->description,
			'render_callback'	=> [ $this, 'render' ],
			'category'			=> $this->category,
			'icon'				=> $this->icon,
			'mode'				=> $this->mode, // auto|preview|edit
			'align'				=> $this->align,
			'keywords'			=> $this->keywords,
		));

	}

	/**
	 *	@param array $block
	 *	@param string $content
	 *	@param array $is_preview
	 *	@param int $pot_id
	 */
	abstract function render( $block, $content, $is_preview, $post_id );

}
