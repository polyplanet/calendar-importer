<?php

namespace CalendarImporter\ACF\Block;

use CalendarImporter\Core;

class Calendar extends Block {

	/**
	 *	@inheritdoc
	 */
	protected $slug = 'pp-calendar';

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		/**
		 *	@var string block title
		 */
		$this->title = __( 'Calendar', 'calendar-importer' );

		/**
		 *	@var string block category
		 */
		$this->category = 'common';

		/**
		 *	@var string block icon
		 */
		$this->icon = 'calendar';

		/**
		 *	@var string block description
		 */
		$this->description = __( 'A Calendar', 'calendar-importer' );

		/**
		 *	@var string block align
		 */
		$this->align = 'none';

		/**
		 *	@var string block mode
		 */
		$this->mode = 'edit';

		/**
		 *	@var string block description
		 */
		$this->keywords = [ __('calendar','calendar-importer'), __('date','calendar-importer'), __('event','calendar-importer') ];

		add_action( 'enqueue_block_editor_assets', array( $this, 'enqueue_assets' ) );
		
		parent::__construct();
	}


	/**
	 *	@action enqueue_block_editor_assets
	 */
	public function enqueue_assets() {
		Core\Core::instance()->register_scripts();
		add_action( 'admin_head', array( Core\Renderer::instance(), 'print_color_styles' ) );		
		wp_enqueue_style('pp-calendar');
	}

	/**
	 *	@inheritdoc
	 */
	public function render( $block, $content, $is_preview, $post_id ) {
		printf(
			'<div class="pp-block-calendar align%s">', 
			esc_attr( $block['align'] )
		);

		$renderer = Core\Renderer::instance();


		$period_field = get_field('period');
		$layout_field = get_field('display');

		if ( ! is_array( $period_field ) || ! is_array( $layout_field ) ) {
			return;
		}

		$period = $period_field["period"];
		if ( 'workweek' === $period || 'workingweek' === $period ) { // workingweek: legacy bug
			$period_data = $period_field['week'];
		} else if ( isset( $period_field[$period] ) ) {
			$period_data = $period_field[$period];
		} else {
			// fallback
			$period_data = array(
				'from_days'    => 'today',
				'from_in_days' => 0,
				'to_days'      => 'tomorrow',
				'to_in_days'   => 1,
			);
		}

		$attr = array(
			'calendars'	=> get_field('calendars'),
			'period'	=> $period,
			'limit'		=> get_field('limit'),
		) + $period_data + $layout_field;

		$renderer->print_calendar($attr);

		echo '</div>';
	}

}
