<?php

namespace CalendarImporter\ACF;


use CalendarImporter\Core;

if ( ! defined('ABSPATH') )
	die();


class I18N extends Core\Singleton {

	private $text_fields = [
		'title',
		'description',
		'label',
		'instructions',
		'prepend',
		'append',
		'message',
	];
	private $sub_fields = [
		'fields',
		'sub_fields',
		'layouts',
	];
	private $choices_fields = [
		'choices',
	];
	private $ignore_fields = [
		'conditional_logic',
		'wrapper',
		'location',	
	];



	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		// localization
//		add_filter( 'acf/load_field', [ $this, 'translate_acf_object' ], 10, 2 );
		add_filter( 'acf/load_fields', [ $this, 'translate_fields' ], 10, 2 );
		add_filter( 'acf/load_field_group', [ $this, 'translate_field_group' ] );

	}


	/**
	 *	Translate ACF Field Group
	 *
	 *	@param Array $field_group
	 *	@return Array
	 *
	 *	@filter acf/load_field_group
	 */
	public function translate_field_group( $field_group ) {

		if ( $this->should_translate( $field_group ) ) {
			$field_group = $this->translate_acf_object( $field_group );
		}
		return $field_group;
	}

	/**
	 *	Translate ACF Fields
	 *
	 *	@param Array $field_group
	 *	@return Array
	 *
	 *	@filter acf/load_fields
	 */
	public function translate_fields( $fields, $parent ) {
		// front- & backend
		if ( $this->should_translate( $parent ) ) {

			$fields = $this->translate_acf_object( $fields );

		}
		return $fields;
	}

	/**
	 *	@param array $field_group
	 *	@return boolean
	 */
	private function should_translate( $field_group ) {
		// other field group
		if ( strpos( $field_group['key'],'group_ppcalendar_') === false ) {
			return false;
		}
		// is sync
		if ( isset( $_REQUEST['post_status'], $_REQUEST['post_type'] ) && $_REQUEST['post_type'] === 'acf-field-group' ) {
			return false;
		}
		if ( isset( $_REQUEST['post_status'], $_REQUEST['post_type'] ) && $_REQUEST['post_status'] === 'sync' && $_REQUEST['post_type'] === 'acf-field-group' ) {
			return false;
		}
		global $pagenow;
		if (  $pagenow === 'post.php' && 'acf-field-group' === get_post_type() ) {
			return false;
		}
		return true;
	}

	/**
	 *	Recursively Translate ACF Group or Field
	 *
	 *	@param Array $field_group
	 *	@return Array
	 *
	 *	@filter acf/load_fields
	 */
	public function translate_acf_object( $object ) {
		foreach ( $object as $key => $value ) {

			if ( in_array( $key, $this->text_fields, true ) ) {

				if ( ! empty( $value ) && is_scalar( $value ) ) {
					$object[$key] = __( $value, 'calendar-importer' );
				}
	
			} else if ( in_array( $key, $this->sub_fields, true ) ) {

				$object[$key] = $this->translate_acf_object($value);

			} else if ( in_array( $key, $this->choices_fields, true ) ) {

				foreach ( $value as $c => $choice ) {
					if ( ! empty( $choice ) && is_scalar($choice) ) {
						$object[$key][$c] = __( $choice, 'calendar-importer' );
					}
				}

			} else if ( in_array( $key, $this->ignore_fields, true ) ) {
				continue;

			} else if( is_array( $value ) ) {
				$object[$key] = $this->translate_acf_object($value);				
			}

		}
		return $object;
	}


}
