<?php

namespace CalendarImporter\ACF;


use CalendarImporter\Core;
//use McGuffin\PostType;

if ( ! defined('ABSPATH') )
	die();


class JSON extends Core\Singleton {

	private $current_json_save_path = null;
	private $prefix = 'ppcalendar';
	private $json_path = 'acf-json'; // acf-json elsewhere ...

	/**
	 *	@inheritdoc
	 */
	public function __construct() {

		// local json paths
		add_filter( 'acf/settings/load_json', [ $this, 'load_json' ] );
		add_filter( 'acf/settings/save_json', [ $this, 'save_json' ] );

		// handle json files
		add_action( 'acf/delete_field_group', [ $this, 'mutate_field_group' ], 9 );
		add_action( 'acf/trash_field_group', [ $this, 'mutate_field_group' ], 9 );
		add_action( 'acf/untrash_field_group', [ $this, 'mutate_field_group' ], 9 );
		add_action( 'acf/update_field_group', [ $this, 'mutate_field_group' ], 9 );

	}


	/**
	 *	@filter 'acf/settings/load_json'
	 */
	public function load_json( $paths ) {

		$paths[] = $this->add_json_path( CALENDAR_IMPORTER_DIRECTORY );

		return $paths;

	}

	/**
	 *	array_filter callback
	 *	Whether a is located within theme root
	 *	@param string $path
	 *	@return boolean
	 */
	private function not_theme_root( $path ) {
		return strpos( $path, get_theme_root() ) === false;
	}

	/**
	 *	array_map callback
	 *	append replative json path
	 *	@param string $path
	 *	@return string
	 */
	private function add_json_path( $path ) {
		return trailingslashit( $path ) . $this->json_path;
	}

	/**
	 *	@filter 'acf/settings/save_json'
	 */
	public function save_json( $path ) {
		if ( ! is_null( $this->current_json_save_path ) ) {
			return $this->current_json_save_path;
		}
		return $path;
	}

	/**
	 *	Figure out where to save ACF JSON
	 *
	 *	@action 'acf/update_field_group'
	 */
	public function mutate_field_group( $field_group ) {
		// default

		if ( strpos( $field_group['key'], "group_{$this->prefix}_" ) === false ) {
			$this->current_json_save_path = null;
			return;
		}

		foreach ( $this->load_json([]) as $path ) {

			$this->current_json_save_path = $path;

			$file = trailingslashit($path) . trailingslashit( $this->json_path ) . $field_group['key'] .'.json';
			if ( file_exists( $file ) ) {
				break;
			}
		}
	}

}
