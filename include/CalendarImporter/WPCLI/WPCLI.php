<?php
/**
 *	@package CalendarImporter\WPCLI
 *	@version 1.0.0
 *	2018-09-22
 */

namespace CalendarImporter\WPCLI;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use CalendarImporter\Core;

class WPCLI extends Core\Singleton {

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		\WP_CLI::add_command( 'calendar-importer clear-cache', array( new CalendarCache(), 'clear' ), array(
//			'before_invoke'	=> 'a_callable',
//			'after_invoke'	=> 'another_callable',
			'shortdesc'		=> 'calendar commands',
//			'when'			=> 'before_wp_load',
			'is_deferred'	=> false,
		) );
		\WP_CLI::add_command( 'calendar-importer sync', array( new CalendarSync(), 'sync' ), array(
//			'before_invoke'	=> 'a_callable',
//			'after_invoke'	=> 'another_callable',
			'shortdesc'		=> 'calendar commands',
//			'when'			=> 'before_wp_load',
			'is_deferred'	=> false,
		) );
	}
}
