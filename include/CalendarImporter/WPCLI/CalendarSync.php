<?php
/**
 *	@package CalendarImporter\WPCLI
 *	@version 1.0.0
 *	2018-09-22
 */

namespace CalendarImporter\WPCLI;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use CalendarImporter\PostType;

class CalendarSync extends \WP_CLI_Command {

	/**
	 * Clear cache
	 *
	 * ## OPTIONS
	 *
	 * [<term_id>]
	 * : Term ID
	 *
	 * [--verbose]
	 * : Print debugging information
	 *
	 * ## EXAMPLES
	 *
	 *     wp calendar-importer sync 123
	 */
	public function sync( $args, $assoc_args ) {
		$assoc_args = wp_parse_args( $assoc_args, [
			'verbose' => false,
		] );

		if ( $assoc_args['verbose'] ) {
			add_action('pp_cal_debug', function($what){ \WP_CLI::line($what); } );
		}
		$total = 0;
		if ( $args[0] ) {
			$term = get_term($args[0]);
			if ( ! $term || is_wp_error( $term ) || $term->taxonomy !== 'pp_calendar' ) {
				\WP_CLI::error( 'Not a calender' ); 
				return;
			}
			$terms = [ $term ];
		} else {
			$terms = get_terms( 'pp_calendar' );
		}
		foreach ( $terms as $term ) {
			\WP_CLI::line( "Sync {$term->slug}" ); 
			$taxonomy = PostType\TaxonomyCalendar::instance();
			$taxonomy->sync( $term->term_id, false );
		}

	}

}
