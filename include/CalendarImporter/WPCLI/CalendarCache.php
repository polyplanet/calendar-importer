<?php
/**
 *	@package CalendarImporter\WPCLI
 *	@version 1.0.0
 *	2018-09-22
 */

namespace CalendarImporter\WPCLI;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use CalendarImporter\Model;
class CalendarCache extends \WP_CLI_Command {

	/**
	 * Clear cache
	 *
	 * ## OPTIONS
	 * [<term_id>]
	 * : Term ID
	 *
	 * [--verbose]
	 * : Print debugging information
	 *
	 * ## EXAMPLES
	 *
	 *     wp calendar-importer clear-cache
	 *
	 *	@alias comment-check
	 */
	public function clear( $args, $assoc_args ) {

		$assoc_args = wp_parse_args( $assoc_args, [
			'verbose' => false,
		] );
		if ( $assoc_args['verbose'] ) {
			add_filter( 'calendar_importer_debug', '__return_true' );
		}

		$total = 0;
		$model = Model\ModelCache::instance();
		if ( isset( $args[0] ) ) {
			$model->clear( 'cal-' . $args[0] );
		} else {
			$deleted = $model->purge();
		}

	}

}
