<?php


/*  Copyright 2015  Jörn Lund

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace CalendarImporter\PostType;

use CalendarImporter\Admin;
use CalendarImporter\Core;
use CalendarImporter\Cron;
use CalendarImporter\iCal;
use CalendarImporter\Model;

class TaxonomyCalendar extends Taxonomy {

	protected $taxonomy_slug = 'pp_calendar';

	private $current_cal = null;

	private $syncing_terms = [];

	protected function __construct() {

		add_filter( 'pre_insert_term',array( $this, 'pre_insert_term' ), 10, 2 );

		add_action( 'created_' . $this->taxonomy_slug, array( $this, 'create_term' ), 11, 2 );

		add_action( 'edited_' . $this->taxonomy_slug, array( $this, 'edited_term' ), 11, 2 );

		add_action( 'delete_' . $this->taxonomy_slug, array( $this, 'delete_term' ), 10, 4 );

		add_action( 'update_term_meta', array( $this, 'update_term_meta' ), 10, 4 );
		/*
		add_action( 'edit_tag_form_fields', array( $this, 'edit_tag_form_fields' ), 1 );
		/*/
		add_action( "{$this->taxonomy_slug}_edit_form_fields", array( $this, 'edit_tag_form_fields' ), 1 );
		//*/

		add_filter( 'term_updated_messages', array( $this, 'create_error_message' ) );
		add_action( 'load-edit-tags.php', array( $this, 'load_edit_tags' ) );

//		add_action( 'pp_sync_calendar', array($this,'sync') );

		parent::__construct();
	}

	public function load_edit_tags() {
		global $taxnow;
		if ( $taxnow === $this->taxonomy_slug ) {
			$calendars = get_terms(array(
				'hide_empty'	=> false,
				'taxonomy'		=> $this->taxonomy_slug,
				'fields'		=> 'ids',
			));

			foreach ( $calendars as $term_id ) {
				$this->setup_cron( $term_id );
			}
		}
	}

	/**
	 *	Whether we are on the post edit screen for this post type.
	 *	Can be called early (eg. in init-hook)
	 *
	 *	@return bool
	 */
	public function is_edit() {
		global $pagenow;
		if ( ! is_admin() || 'term.php' !== $pagenow ) {
			return false;
		}
		if ( ! isset( $_REQUEST['taxonomy'] ) ) {
			return false;
		}
		return 'pp_calendar' === wp_unslash( $_REQUEST['taxonomy'] );

	}

	/**
	 *	Whether we are on the post list table for this post type.
	 *	Can be called early (eg. in init-hook)
	 *
	 *	@return bool
	 */
	public function is_list() {
		if ( ! is_admin() || 'edit-tags.php' !== pathinfo( $_SERVER['SCRIPT_NAME'], PATHINFO_BASENAME ) ) {
			return false;
		}
		return isset($_REQUEST['taxonomy']) && 'pp_calendar' === $_REQUEST['taxonomy'];
	}


	/**
	 *	Show calendar Feed URL
	 *
	 *	@param object $tag
	 *
	 *	@action edit_tag_form_fields
	 */
	public function edit_tag_form_fields( $tag ) {
		if ( $tag->taxonomy === $this->taxonomy_slug ) {
			?>
			<tr class="form-field calendar-feed-url-wrap">
				<th scope="row">
					<?php esc_html_e( 'Calendar Feed URL', 'calendar-importer' ); ?>
				</th>
				<td><code>
					<?php echo esc_url( get_term_meta( $tag->term_id, '_pp_cal_url', true ) ); ?>
				</code></td>
			</tr>
			<?php
			
		}
	}
	
	/**
	 *	@param String $event_uid
	 *	@return WP_Post|Boolean
	 */
	private function get_event_post( $event_uid ) {
		$event_posts = get_posts(array(
			'post_type'			=> 'pp_event',
			'posts_per_page'	=> 1,
			'meta_key'			=> '_pp_calendar_event_uid',
			'meta_value'		=> $event_uid,
		));
		if ( count( $event_posts ) ){
			return $event_posts[0];
		}
		return false;
	}

	/**
	 *	@param	int		$term_id
	 *	@param	bool	$cached
	 *	@return array
	 */
	public function sync( $term_id, $cached = true ) {

		if ( defined( 'CALENDAR_IMPORTER_SYNC_TIMEOUT' ) ) {
			ini_set( 'max_execution_time', defined( 'CALENDAR_IMPORTER_SYNC_TIMEOUT' ) );
		} else {
			ini_set( 'max_execution_time', 60 );
		}

		pp_cal_debug("[CALENDAR_IMPORTER] Sync [$term_id]");

		if ( in_array( $term_id, $this->syncing_terms ) ) {
			pp_cal_debug("[CALENDAR_IMPORTER] Sync [$term_id] already running.");
			return;
		}

		$this->syncing_terms[] = $term_id;

		$this->cleanup_cron();

		$acf_id = 'pp_calendar_' . $term_id;
		$term = get_term( $term_id, $this->taxonomy_slug );

		// no term!
		if ( ! $term = get_term( $term_id, $this->taxonomy_slug ) ) {
			return [
				'success' => false,
				'error'   => new \WP_Error( 'pp-cal-err', sprintf( __('No Calendar with ID %d', 'calendar-importer' ), $term_id ) ),
			];
		}

		$response = array(
			'success'	=> false,
			'events'	=> array(
				'created'		=> 0,
				'updated'		=> 0,
				'deleted'		=> 0,
				'doublettes'	=> 0,
				'unassigned'	=> PostTypeEvent::instance()->delete_unassigned(),
			),
			'calendar'		=> array(
				'name'			=> $term->name,
				'description'	=> $term->description,
			),
			'last_sync'	=> null,
			'error'		=> null,
		);



		// no url!
		if ( ! $calendar_url = get_term_meta( $term_id, '_pp_cal_url', true ) ) {
			$response['error']	= new \WP_Error( 'pp-cal-err', __('No Calendar URL specified', 'calendar-importer' ) );
			return $response;
		}

		$last_sync	= $result['last_sync'] = get_term_meta( $term_id, '_pp_cal_last_sync', true );

		$ical				= iCal\zParser::instance();
		$this->current_cal	= $ical->fetch( $calendar_url, ! CALENDAR_IMPORTER_DEBUG );


		// fetch failed
		if ( is_wp_error( $this->current_cal ) ) {
			$response['error'] = $this->current_cal;
			pp_cal_debug( '[CALENDAR_IMPORTER] sync error ' . $this->current_cal->get_error_message() );
			return $response;
		}

		$term_data = $response['calendar'] = array(
			'name'			=> $this->current_cal->calendarName(),
			'description'	=> $this->current_cal->calendarDescription(),
		);

		// update name and description
		wp_update_term( $term_id, $this->taxonomy_slug, $term_data );
		
		// set color from calendar
		$events = $this->current_cal->events();
		pp_cal_debug( sprintf( '[CALENDAR_IMPORTER] found %d events' , count( $events ) ) );
		$schedules = Model\ModelSchedules::instance();
		$cache = Model\ModelCache::instance();

		$posts = get_posts( array(
			'post_type'		=> 'pp_event',
			'numberposts'	=> -1,
			'post_status'	=> 'any',
			'tax_query'		=> array(
				array(
					'taxonomy'			=> $this->taxonomy_slug,
					'field'				=> 'id',
					'terms'				=> $term_id,
				),
			),
		));
		// events to delete later
		$all_posts = [];
		$doublettes = [];
		
		foreach ( $posts as $post ) {
			$uid = get_post_meta( $post->ID, '_pp_calendar_event_uid', true );
			if ( ! isset( $all_posts[ $uid ] ) ) {
				$all_posts[ $uid ] = $post;
			} else {
				// Error case: multiple posts having the same UID.
				$doublettes[] = $post;
			}
		}

		// reset schedules for term
		$schedules->delete( array( 'term_id' => $term_id ), array('%d') );

		$one_year_ago = new \DateTime();
		$one_year_ago->sub( new \DateInterval( 'P1Y' ) );
		$one_year_ago_ts = $one_year_ago->getTimestamp();

		foreach ( array_reverse( $events ) as $event ) {
			// get dates not older than 1Y
			$event_dates = $event->getDates( 'Y-m-d H:i:s', 'Y-m-d 00:00:00' );
			$event_dates = array_filter( $event_dates, function($date) use ( $one_year_ago_ts ) {
				return $date['dtend_timestamp'] >= $one_year_ago_ts;
			} );

			// skip events with no dates or older than 1Y
			if ( ! count( $event_dates ) ) {
				pp_cal_debug( sprintf( '[CALENDAR_IMPORTER] Skipping "%s" (%s) for it is older than one year', $event->summary, $event->uid ) );
				continue;
			}

			$event_id = null;
			$event_uid = $event->uid;

			if ( ! is_null( $event->lastmodified ) ) {
				$event_modified_ts = strtotime( $event->lastmodified );
			} else {
				$event_modified_ts = new \DateTime();
			}

			$insert_args = array(
				'post_date'     => date( 
					'Y-m-d H:i:s', 
					! is_null($event->created)
						? strtotime($event->created)
						: null
				), // created
				'post_content'  => '', //
				'post_title'    => $event->summary, // cal event summary
				'post_type'     => 'pp_event',
			);

			// Check if update or create
			if ( isset( $all_posts[ $event_uid ] ) ) {
				// event exists

				// update
				$insert_args['ID'] = $all_posts[ $event_uid ]->ID;
				$insert_args['post_status'] = $all_posts[ $event_uid ]->post_status;
				$response['events']['updated']++;
				unset( $all_posts[ $event_uid ] ); // dont delete

			} else {
				// is new event
				$insert_args['post_status'] = 'publish';
				$response['events']['created']++;
			}


			// Set description
			if ( ! is_null($event->description) ) {
				$insert_args['post_content'] = $event->description;
			}

			// Set modified
			if ( ! is_null($event->lastmodified) ) {
				$insert_args['post_modified'] = date( 'Y-m-d H:i:s', $event_modified_ts ); // modified
			}


			// create event
			pp_cal_debug( sprintf('[CALENDAR_IMPORTER] Create post for "%s" (%s)', $event->summary, $event->uid ));
			$event_id = wp_insert_post( $insert_args, true );
			if ( is_wp_error( $event_id ) ) {
				trigger_error(sprintf( "Calendar Importer: Insert post: %s, Blog-ID: %d ", $event_id->get_error_message(), get_current_blog_id() ), E_USER_WARNING );
				continue;
			}
			update_post_meta( $event_id, '_pp_calendar_event_uid', $event_uid );
			pp_cal_debug( '[CALENDAR_IMPORTER] update postmeta' );

			$schedules->delete( array( 'post_id' => $event_id ), array('%d') );
			pp_cal_debug( '[CALENDAR_IMPORTER] delete schedules '.$event->summary );


			// assign to calendar
			wp_set_object_terms( $event_id, absint( $term_id ), $this->taxonomy_slug );

			// Set recurring dates
			foreach ( $event_dates as $date ) {
				// dont insert past dates
				/*
				if ( $date['is_past'] ) {
					continue;
				}
				/*/
				//*/
				$schedules->insert( array(
					'post_id'	=> $event_id,
					'term_id'	=> $term_id,
					'dtstart'	=> $date['is_all_day'] ? $date['dtstart_date'] : $date['dtstart_datetime'],
					'dtend'		=> $date['is_all_day'] ? $date['dtend_date'] : $date['dtend_datetime'],
					'all_day'	=> intval($date['is_all_day']),
				), array('%d','%d','%s','%s','%d') );
				pp_cal_debug( '[CALENDAR_IMPORTER] ... insert date '.$date['dtstart_date'] );

			}

			pp_cal_debug( '[CALENDAR_IMPORTER] insert ' . count( $event_dates ). ' schedules '.$event->summary );

			// location
			if ( ! is_null( $event->location ) ) {

				update_field( 'field_pp_event_location', $event->location,  $event_id );
				pp_cal_debug( '[CALENDAR_IMPORTER] ... set location' );
				// apple maybe knows geodata!
				if ( ! is_null( $event->{'X-APPLE-STRUCTURED-LOCATION'} ) ) {

					$geo = substr( $event->{'X-APPLE-STRUCTURED-LOCATION'}, 4 );
					$geo = explode( ',', $geo );
					$geo = array_map( 'floatval', $geo );
					if ( count($geo) == 2 ) {
						$location_address = preg_replace( '/[\n\r\t]/imsU', ', ', $event->location );
						$location_map = array(
							'address'	=> $location_address, // acf gmaps compat
							'zoom'		=> 13,
							'lat'		=> $geo[0],
							'lng'		=> $geo[1],
							'markers'	=> array( array(  // acf osm compat
								'label'			=> $location_address,
								'default_label'	=> '',
								'lat'		=> $geo[0],
								'lng'		=> $geo[1],
							) ),
						);
						update_field( 'field_pp_event_location_map', $location_map,  $event_id );
						pp_cal_debug( '[CALENDAR_IMPORTER] ... set location map' );
					}
				}
			} else {
				update_field( 'field_pp_event_location', null, $event_id );
				update_field( 'field_pp_event_location_map', null,  $event_id );
			}

			update_post_meta( $event_id, 'pp_event_dtstart', $event->dtstart );
			update_post_meta( $event_id, 'pp_event_dtend', $event->dtend );
			pp_cal_debug( '[CALENDAR_IMPORTER] ... update postmeta' );
			// URL
			if ( ! is_null( $event->url ) ) {
				update_field( 'field_pp_event_url', esc_url_raw( $event->url ),  $event_id );
			} else {
				update_field( 'field_pp_event_url', $event->url,  $event_id );
			}
			pp_cal_debug( '[CALENDAR_IMPORTER] ... update url' );
		}

		$response['events']['doublettes'] = count( $doublettes );
		// delete doublettes
		foreach ( $doublettes as $post ) {
			wp_delete_post( $post->ID, true );
			$response['events']['deleted']++;
		}
		// process delete list
		foreach ( $all_posts as $slug => $post ) {
			wp_delete_post( $post->ID, true );
			
			$response['events']['deleted']++;
		}
		if ( array_sum( $response['events'] ) > 0 ) {
			$cache->clear( 'cal-' . $term_id );
		}
		$response['success'] = true;

		$last_sync = $response['last_sync'] = time();

		update_term_meta( $term_id, '_pp_cal_last_sync', $last_sync );
		pp_cal_debug( '[CALENDAR_IMPORTER] Sync complete' );
		pp_cal_debug( sprintf(
			'[CALENDAR_IMPORTER] Created: %d, Updated: %d, Deleted: %d (%d Doublettes)', 
			$response['events']['created'],
			$response['events']['updated'],
			$response['events']['deleted'],
			$response['events']['doublettes']
		));

		return $response;
	}

	/**
	 *
	 */
	public function delete_term( $term_id, $tt_id, $deleted_term, $object_ids ) {

		// delete events
		foreach( $object_ids as $event_id ) {
			wp_delete_post( $event_id, true );
		}

		// delete schedule
		$schedules = Model\ModelSchedules::instance();
		$schedules->delete( array( 'term_id' => $term_id ) );

		// stop cron
		$this->stop_cron( $term_id );

		$cache = Model\ModelCache::instance();
		$cache->clear( 'cal-'.$term_id );
	}



	/**
	 *	@action pre_insert_term
	 */
	public function pre_insert_term( $term, $taxonomy ) {
		if ( ! check_ajax_referer( 'add-tag', '_wpnonce_add-tag', false ) ) {
			return $term;
		}

		if ( $taxonomy === $this->taxonomy_slug ) {
			// webcal://p51-calendars.icloud.com/published/2/hFclNDj2vdM5JoodT2o7DyLSotL7i_o8q3CqelNy6vvBHqhaE7m1YaEOLLicvf6n8_uMYSus3h8uJAtpdx2JzHAJmqLA2W5tWnHL2p-6RP0
			// https://calendar.google.com/calendar/ical/tjbrt9lhh47p8tbridjj9be964%40group.calendar.google.com/public/basic.ics
			// https://www.schulferien.eu/downloads/ical4.php?land=HH&type=0&year=2018

			$term = false;

			if ( isset( $_POST['_pp_cal_url'] ) ) {

				$cal_url = esc_url_raw( $_POST['_pp_cal_url'], [ 'http', 'https', 'webcal' ]);

				$expire_field = get_field_object('field_pp_cal_expire');
				$expire = $expire_field['default_value'][0];
				$cached = ! WP_DEBUG;
				$this->current_cal = iCal\zParser::instance()->fetch( $cal_url, $cached, $expire );

				if ( is_wp_error( $this->current_cal ) ) {
					set_transient('last_cal_err', $this->current_cal );
					return $term;
				}

				// avoid duplicate names
				$term_name = $term_name_base = $this->current_cal->calendarName();
				$i = 0;
				while ( get_term_by( 'name', $term_name, $taxonomy ) ) {
					$term_name = sprintf('%s (%d)', $term_name_base, ++$i );
				}
				$term = $term_name;

			}

		}

		return $term;
	}

	/**
	 *	@filter term_updated_messages
	 */
	public function create_error_message( $messages ) {
		if ( $err = get_transient('last_cal_err' ) ) {
			delete_transient('last_cal_err' );
			if ( is_wp_error( $err ) ) {
				$messages['_item'][4] = sprintf( '<strong>%s</strong><br />%s <code>%s</code>',
					$messages['_item'][4],
					__('Error Message:'),
					$err->get_error_message()
				);
			}
		}
		return $messages;
	}

	/**
	 *	Set default color if none is set
	 *
	 *	@action created_pp_calendar
	 */
	public function edited_term( $term_id ) {

		$cache = Model\ModelCache::instance();
		$cache->clear( 'cal-'.$term_id );


		if ( ! ( $color = get_field( 'field_pp_cal_color', $this->taxonomy_slug . '_' . $term_id ) ) ) {
			$admin = Admin\Admin::instance();
			$color = $admin->next_color();
			update_field( 'field_pp_cal_color', $color, $this->taxonomy_slug . '_' . $term_id );
		}

		$this->setup_cron( $term_id );

	}

	/**
	 *	Remove unused cron jobs
	 */
	private function cleanup_cron() {
		
		$jobs = Cron\Cron::findJobs( 'pp_sync_calendar', array( $this, 'sync' ) );

		foreach ( $jobs as $job ) {
			$args = $job->get_args();
			if ( ! isset( $args['term_id'] ) || ! term_exists( $args['term_id'], $this->taxonomy_slug ) ) {
				$job->stop();
			}
		}
	}

	/**
	 *	Reset cron jobs
	 */
	private function setup_cron( $term_id ) {

		// make sure sync interval isn't too small ... min 15 minutes!
		$interval = get_field( 'pp_cal_expire', $this->taxonomy_slug . '_' . $term_id );
		$interval = absint( $interval );

		if ( $interval ) {
			// Start another one
			$interval = max( $interval, apply_filters( 'calendar_importer_min_sync', 900 ) );
			$jobs = Cron\Cron::findJobs( 'pp_sync_calendar', array( $this, 'sync' ), array( 'term_id' => intval( $term_id ) ), $interval );
			if ( ! count( $jobs ) ) {
				// jobs with false interval
				$this->stop_cron( $term_id );
				$job = Cron\Cron::getJob( 'pp_sync_calendar', array( $this, 'sync' ), array( 'term_id' => intval( $term_id ) ), $interval )
					->start();
				pp_cal_debug("started cronjob");
			}
		} else {
			$this->stop_cron( $term_id );
		}

	}

	/**
	 *	Stop all cron jobs
	 */
	private function stop_cron( $term_id ) {
		$jobs = Cron\Cron::findJobs( 'pp_sync_calendar', array( $this, 'sync' ), array( 'term_id' => intval( $term_id ) ) );
		foreach ( $jobs as $job ) {
			$job->stop();
		}
	}

	/**
	 *	Update URL
	 *	@action created_pp_calendar
	 */
	public function create_term( $term_id, $tt_id ) {
		if ( is_wp_error( $this->current_cal ) ) {
			pp_cal_debug( '[CALENDAR_IMPORTER] Error creating term: '.$this->current_cal->get_error_message() );
			return;
		}

		// save feed url
		update_term_meta( $term_id, '_pp_cal_url', $this->current_cal->getUrl() );

		// sync events
		$sync_result = $this->sync( $term_id );

		if ( is_wp_error( $sync_result['error'] ) ) {
			$message = $sync_result['error']->get_error_message();
		}
	}

	/**
	 *	@filter update_term_meta
	 */
	public function update_term_meta( $meta_id, $term_id, $meta_key, $meta_value ) {

		if ( $meta_key === 'pp_cal_expire' ) {

			$this->setup_cron( $term_id );
		}
	}

	/**
	 *	Register taxonomy
	 *
	 */
	public function register_taxonomy( ) {

		// labels
		$labels = array(
			'name'              => _x( 'Calendars', 'taxonomy general name' , 'calendar-importer' ),
			'singular_name'     => _x( 'Calendar', 'taxonomy singular name' , 'calendar-importer' ),
			'search_items'      => __( 'Search Calendars' , 'calendar-importer' ),
			'all_items'         => __( 'All Calendars' , 'calendar-importer' ),
			'parent_item'       => __( 'Parent Calendar' , 'calendar-importer' ),
			'parent_item_colon' => __( 'Parent Calendar:' , 'calendar-importer' ),
			'edit_item'         => __( 'Edit Calendar' , 'calendar-importer' ),
			'update_item'       => __( 'Update Calendar' , 'calendar-importer' ),
			'add_new_item'      => __( 'Add New Calendar' , 'calendar-importer' ),
			'new_item_name'     => __( 'New Calendar' , 'calendar-importer' ),
			'menu_name'         => __( 'Calendars' , 'calendar-importer' ),
			'popular_items'		=> null,
		);

		$args = array(
			'hierarchical'		=> false,
			'labels'			=> $labels,
			'show_ui'			=> true,
			'public'			=> true,
			'show_admin_column' => true,
			'meta_box_cb'		=> false,
			'capabilities'		=> array(
				'create_terms'		=> 'do_not_allow',
				'manage_terms'		=> 'edit_posts',
				'edit_terms'		=> 'edit_posts',
				'delete_terms'		=> 'delete_posts',
				'assign_terms'		=> 'do_not_allow',
			),
		);

		register_taxonomy( $this->taxonomy_slug, array( 'pp_event' ), $args );

	}



	/**
	 *	@inheritdoc
	 */
	public function upgrade( $new_version, $old_version ) {

		$cache = Model\ModelCache::instance()->purge();

	}


}
