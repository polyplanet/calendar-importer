<?php

namespace CalendarImporter\PostType;
use CalendarImporter\Core;

abstract class PostType extends Core\PluginComponent {

	protected $post_type_slug;
	private $meta_boxes = array();

	/**
	 * Private constructor
	 */
	protected function __construct() {

		if ( current_action() === 'init' ) {
			$this->register_post_types();
		} else {
			add_action( 'init' , array( $this, 'register_post_types' ), 0 );
		}

		add_action( 'save_post_' . $this->post_type_slug, array( $this, 'save_post' ), 10, 3 );
		add_action( "add_meta_boxes_{$this->post_type_slug}", array( $this, 'add_meta_boxes' ) );

		parent::__construct();
	}

	/**
	 *	@param	string	$slug
	 *	@param	array 	$args
	 *	@return bool
	 */
	protected function add_meta_box( $slug, $args ) {
		$args = wp_parse_args( $args, array(
			'title'			=> '',
			'callback'		=> null,
			'context'		=> 'advanced', // normal|advanced|side
			'priority'		=> 'default', // low|high|default
			'callback_args'	=> array(),
		));
		if ( is_callable( $args[ 'callback' ] ) ) {
			$this->meta_boxes[ $slug ] = $args;
			return true;
		}
		return false;
	}
	/**
	 *	@action  add_meta_box_{$this->post_type_slug}
	 */
	public function add_meta_boxes() {
		foreach ( $this->meta_boxes as $slug => $args ) {
			extract( $args );
			add_meta_box( "{$this->post_type_slug}_{$slug}", $title, $callback, $this->post_type_slug, $context, $priority, $callback_args );
		}

	}


	/**
	 * Init hook.
	 *
	 * @action init
	 */
	abstract function register_post_types();

	public function save_post( $post_id, $post, $update ) {
	}


	public function get_slug() {
		return $this->post_type_slug;
	}


	public function add_taxonomy( Taxonomy $taxonomy ) {
		register_taxonomy_for_object_type( $taxonomy->get_slug(), $this->get_slug() );
		return $this;
	}

	/**
	 *	@inheritdoc
	 */
	public function activate() {
		// register post types, taxonomies
		$this->register_post_types();

		// flush rewrite rules
		flush_rewrite_rules();
	}

	/**
	 *	@inheritdoc
	 */
	public function deactivate() {
		// flush rewrite rules
		global $wp_rewrite;
		if ( $wp_rewrite ) {
			$wp_rewrite->flush_rules( true );
		}
	}

	/**
	 *	@inheritdoc
	 */
	public static function uninstall() {
		$posts = get_posts(array(
			'post_type' 		=> $this->post_type_slug,
			'post_status'		=> 'any',
			'posts_per_page'	=> -1,
		));
		foreach ( $posts as $post ) {
			wp_delete_post( $post->ID, true );
		}
	}

	/**
	 *	@inheritdoc
	 */
	public function upgrade( $new_version, $old_version ) {
	}
}
