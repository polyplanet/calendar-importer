<?php

namespace CalendarImporter\PostType;

use CalendarImporter\Core;

abstract class Taxonomy extends Core\PluginComponent {


	protected $taxonomy_slug;

	/**
	 * Private constructor
	 */
	protected function __construct() {
		if ( current_action() === 'init' ) {
			$this->register_taxonomy();
		} else {
			add_action( 'init' , array( $this , 'register_taxonomy' ) , 0 );
		}
		parent::__construct();
	}

	public function get_slug() {
		return $this->taxonomy_slug;
	}

	/**
	 * Init hook.
	 *
	 * @action init
	 */
	abstract function register_taxonomy();

	public function add_post_type( PostType $post_type ) {
		register_taxonomy_for_object_type( $this->get_slug(), $post_type->get_slug() );
		return $this;
	}


	/**
	 *	@inheritdoc
	 */
	public function activate() {
		// register post types, taxonomies
		$this->register_taxonomy();

		// flush rewrite rules
		flush_rewrite_rules();
	}

	/**
	 *	@inheritdoc
	 */
	public function deactivate() {
		// flush rewrite rules
		global $wp_rewrite;
		if ( $wp_rewrite ) {
			$wp_rewrite->flush_rules( true );
		}
	}

	/**
	 *	@inheritdoc
	 */
	public static function uninstall() {
		$terms = get_terms( array(
			'taxonomy' 		=> $this->taxonomy_slug,
			'hide_empty'	=> false,
		));

		foreach ( $terms as $term ) {
			wp_delete_term( $term->term_id, $this->taxonomy_slug );
		}

	}

	/**
	 *	@inheritdoc
	 */
	public function upgrade( $new_version, $old_version ) {
	}

}
