<?php

namespace CalendarImporter\PostType;
use CalendarImporter\Core;
use CalendarImporter\Model;

class PostTypeEvent extends PostType {

/*
	private static $_instance = null;
*/

	protected $post_type_slug = 'pp_event';

	/**
	 *	@var Model\ModelSchedules
	 */
	private $model;

	/**
	 * Private constructor
	 */
	protected function __construct() {

		parent::__construct();

		if ( ! defined('WP_CLI') && ! wp_doing_ajax() ) {
			add_filter( 'the_content', array( $this, 'the_content' ) );
		}

		add_action( 'restrict_manage_posts', array( $this, 'restrict_manage_posts' ), 10, 2 );

		add_action( 'admin_init', array( $this, 'register_meta_boxes' ), 10, 2 );

		$this->model = Model\ModelSchedules::instance();

		add_filter( "manage_{$this->post_type_slug}_posts_columns", array($this,'manage_columns') );
		add_action( "manage_{$this->post_type_slug}_posts_custom_column", array($this,'print_column'), 10, 2 );

	}

	/**
	 *	@inheritdoc
	 */
	public function save_post( $post_id, $post, $update ) {

		// terms are not set during sync.
		if ( ! $update ) {
			return;
		}

		$cache = Model\ModelCache::instance();
		$terms = get_the_terms( $post_id, 'pp_calendar' );
		
		if ( false === $terms ) {
			trigger_error( 'Calendar Importer: No calendar terms for post '.$post_id .'', E_USER_WARNING );
			return;
		} else if ( is_wp_error( $terms ) ) {
			trigger_error( 'Calendar Importer: '.$terms->get_error_message(), E_USER_WARNING );
			return;
		}
		foreach ( $terms as $term ) {
			$cache->clear( 'cal-'.$term->term_id );
		}
	}

	/**
	 *	@action admin_init
	 */
	public function register_meta_boxes() {
		$this->add_meta_box( 'calendar', array(
			'title'		=> __('Calendar','calendar-importer'),
			'callback'	=> array( $this, 'meta_box_calendar'),
			'context'	=> 'side',
		) );

		$this->add_meta_box( 'recurring', array(
			'title'		=> __('Dates','calendar-importer'),
			'callback'	=> array( $this, 'meta_box_dates'),
			'context'	=> 'side',
		) );
	}

	/**
	 *	Whether we are on the post edit screen for this post type.
	 *	Can be called early (eg. in init-hook)
	 *
	 *	@return bool
	 */
	public function is_edit() {
		global $pagenow;

		if ( ! is_admin() || ! isset( $pagenow ) || 'post.php' !== $pagenow ) {
			return false;
		}
		if ( ! isset( $_REQUEST['post'] ) ) {
			return false;
		}
		
		return ( $post = get_post( wp_unslash( $_REQUEST['post'] ) ) ) && $this->post_type_slug === $post->post_type;

	}

	/**
	 *	Whether we are on the post list table for this post type.
	 *	Can be called early (eg. in init-hook)
	 *
	 *	@return bool
	 */
	public function is_list() {

		global $pagenow;

		if ( ! is_admin() || ! isset( $pagenow ) || 'edit.php' !== $pagenow ) {
			return false;
		}
		if ( ! isset( $_REQUEST['post_type'] ) ) {
			return false;
		}

		return $this->post_type_slug === $_REQUEST['post_type'];
	}


	/**
	 *	Print date column
	 *
	 *	@action manage_{$this->post_type_slug}_posts_custom_column
	 */
	public function print_column( $column, $post_id ) {
		if ( $column === 'dtstart' ) {
			$this->model->query_events(array(
				'events'	=> $post_id,
				'period'	=> 'day',
				'from_days'	=> 'today',
				'limit'		=> 1,
			));
			if ( $date = $this->model->get_the_date() ) {
				// if found in pp_schedules table
				echo pp_format_datetime($date);
			} else if ( ( $dtstart = get_post_meta($post_id, 'pp_event_dtstart', true ) ) && ( $dtend = get_post_meta($post_id,'pp_event_dtend',true) ) ) {
				// if postmeta is present
				

				$timezone = new \DateTimeZone( pp_get_timezone_option() );
				$utc = new \DateTimeZone( 'UTC' );

				$dtstart_dt = new \DateTime( $dtstart, $utc );
				$dtstart_dt->setTimezone( $timezone );

				$dtend_dt =	new \DateTime( $dtend, $utc );
				$dtend_dt->setTimezone( $timezone );
				// $dtstart_dt->setTimezone( new \DateTimeZone( pp_get_timezone_option() ) );
				// $dtend_dt->setTimezone( new \DateTimeZone( pp_get_timezone_option() ) );

				$date = (object) array(
					'dtstart_dt'	=> $dtstart_dt,
					'dtend_dt'		=> $dtend_dt,
					'all_day'		=> 0 === ( $dtstart_dt->getTimestamp() % DAY_IN_SECONDS ),
				);
				echo pp_format_datetime( $date );
//				$event = $this->model->get_the_event();
			}
		}
	}
	/**
	 *	Add date column
	 *
	 *	@filter manage_{$this->post_type_slug}_posts_columns
	 */
	public function manage_columns( $columns ) {
		$new_columns = array();
		foreach ( $columns as $key => $col ) {
			$new_columns[$key] = $col;

			if ( $key === 'title' ) {
				$new_columns['dtstart'] = __('Next Date','calendar-importer');
			}
		}
		return $new_columns;
	}

	/**
	 *	Print calendar metabox callback
	 */
	public function meta_box_calendar($post, $metabox) {
		$terms = wp_get_object_terms( $post->ID, 'pp_calendar');
		foreach ( $terms as $term ) {
			$arg = array(
				'post_type'		=> $this->post_type_slug,
				'pp_calendar'	=> $term->slug
			);
			?>
				<p><strong>
				<?php
				printf( 
					'<a href="%s">%s</a>', 
					esc_url( add_query_arg( $arg, admin_url('edit.php') ) ),
					esc_html( $term->name )
				);
				?></strong><?php
				if ( current_user_can( 'edit_term', $term->term_id ) ) {
					?><br /><?php
					edit_term_link( '', '', '', $term );
				}
				?>
				</p>
			<?php

		}
	}

	/**
	 *	Dates metabox callback
	 */
	public function meta_box_dates( $post, $metabox ) {
		$model = Model\ModelSchedules::instance();
		$model->query_events( array(
			'events'	=> $post->ID,
		));
		if ( pp_have_recurring_dates() ) {
			$count_dates = pp_count_recurring_dates();
			$i = 0;
			?>
			<ul class="pp-recurring-events pp-more-box" data-more="false">
				<?php
			while ( pp_have_recurring_dates() ) {
				pp_the_recurring_date();
				$date = pp_get_the_recurring_date();
				if ( $i === 5 ) {
					?>
					<li class="pp-datetime pp-more">
						<button class="pp-more-button button-link">
							<?php esc_html_e(
								sprintf( 
									_n( '%d more', '%d more', $count_dates, 'calendar-importer'),
									$count_dates
								)
							); ?>
						</button>
					</li>
					<?php
				}
				?>
				<li class="pp-datetime">
					<?php echo pp_format_datetime($date); ?>
				</li>
				<?php
				$count_dates--;
				$i++;
			}
				?>
			</ul>

			<?php
		} else {
			?>
			<p>
				<?php $this->print_column( 'dtstart', $post->ID ); ?>
			</p>
			<?php

		}
	}

	/**
	 *	@action restrict_manage_posts
	 */
	public function restrict_manage_posts( $post_type, $which ) {
		if ( $post_type !== $this->post_type_slug ) {
			return;
		}

		if ( ! 'top' === $which ) {
			return;
		}

		$taxonomy_slug = 'pp_calendar';
		$taxonomy = get_taxonomy( $taxonomy_slug );

		$dropdown_options = array(
			'show_option_all'	=> $taxonomy->labels->all_items,
			'hide_empty'		=> 0,
			'hierarchical'		=> 0,
			'show_count'		=> 0,
			'orderby'			=> 'name',
			'selected'			=> isset( $_GET[ $taxonomy_slug ] ) ? wp_unslash( $_GET[ $taxonomy_slug ] ) : '',
			'name'				=> $taxonomy_slug,
			'taxonomy'			=> $taxonomy_slug,
			'value_field'		=> 'slug',
		);

		/* translators: %s post type singular name */
		echo '<label class="screen-reader-text" for="cat">' . 
			sprintf( esc_html__( 'Filter by %s', 'calendar-importer' ), esc_html( $taxonomy->labels->singular_name ) ) . 
		'</label>';

		wp_dropdown_categories( $dropdown_options );

	}

	/**
	 *	Delete all events not bound to any calendar
	 *
	 *	@return Integer Number of deleted posts
	 */
	public function delete_unassigned() {
		global $wpdb;
		$unassigned_post_ids = $wpdb->get_col(
			"SELECT 
				p.ID 
			FROM $wpdb->posts AS p
			LEFT JOIN $wpdb->term_relationships AS tr 
				ON p.ID = tr.object_id
			WHERE p.post_type = 'pp_event' 
				AND tr.object_id IS NULL;"
		);
		$count = 0;
		foreach ( $unassigned_post_ids as $post_id ) {
			$count++;
			wp_delete_post( $post_id, true );
		}
		return $count;
	}

	/**
	 *	Append and Prepend Calendar HTML to post content
	 *
	 *	@filter the_content
	 */
	public function the_content( $content ) {
		$post = get_post();
		if ( ! $post ) {
			return $content;
		}
		if ( ( $post->post_type !== $this->post_type_slug ) ) {
			return $content;
		}

		$model = Model\ModelSchedules::instance();
		$model->query_events( array(
			'events'	=> $post->ID,
		) );

		ob_start();
		get_template_part('calendar-importer/event','before-content');
		$before = ob_get_clean();

		ob_start();
		get_template_part('calendar-importer/event','after-content');
		$after = ob_get_clean();

		return $before . $content . $after;
	}

	/**
	 *	Register Post Types
	 */
	public function register_post_types( ) {
		// register post type Event
		$labels = array(
			'name'                => _x( 'Events', 'Post Type General Name', 'calendar-importer' ),
			'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'calendar-importer' ),
			'menu_name'           => __( 'Calendars', 'calendar-importer' ),
			'parent_item_colon'   => __( 'Parent Event:', 'calendar-importer' ),
			'all_items'           => __( 'All Events', 'calendar-importer' ),
			'view_item'           => __( 'View Event', 'calendar-importer' ),
			'add_new_item'        => __( 'Add New Event', 'calendar-importer' ),
			'add_new'             => __( 'Add New', 'calendar-importer' ),
			'edit_item'           => __( 'Edit Event', 'calendar-importer' ),
			'update_item'         => __( 'Update Event', 'calendar-importer' ),
			'search_items'        => __( 'Search Event', 'calendar-importer' ),
			'not_found'           => __( 'Not found', 'calendar-importer' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'calendar-importer' ),
		);

		$args = array(
			'label'					=> __( 'Events', 'calendar-importer' ),
			'description'			=> __( 'Event Description', 'calendar-importer' ),
			'labels'				=> $labels,
			'supports'				=> array( 'title' , 'editor', 'thumbnail' ),
			'taxonomies'			=> array( 'pp_calendar' ),
			'menu_icon'				=> 'dashicons-calendar',
			'hierarchical'			=> false,
			'public'				=> true,
			'show_ui'				=> true,
			'show_in_menu'			=> true,
			'show_in_nav_menus'		=> false,
			'show_in_admin_bar'		=> true,
			'menu_position'			=> 25,
			'can_export'			=> false,
			'has_archive'			=> false,
			'exclude_from_search'	=> false,
			'publicly_queryable'	=> true,
			'map_meta_cap'			=> true,
			'exclude_from_search'	=> true,
			'capabilities'			=> array(
				'create_posts'			=> 'do_not_allow',
				'edit_post'				=> 'edit_post',
				'read_post'				=> 'read_post',
//				'delete_post'			=> false,

				// Primitive capabilities used outside of map_meta_cap():
				'edit_posts'			=> 'edit_posts',
				'edit_others_posts'		=> 'edit_others_posts',
				'publish_posts'			=> 'publish_posts',
				'read_private_posts'	=> 'read_private_posts',

				'read'                   => 'read',
				'delete_posts'           => 'do_not_allow',
				'delete_private_posts'   => 'do_not_allow',
				'delete_published_posts' => 'do_not_allow',
				'delete_others_posts'    => 'do_not_allow',
				'edit_private_posts'     => 'edit_private_posts',
				'edit_published_posts'   => 'edit_published_posts',
			),

		);
		register_post_type( $this->post_type_slug, $args );
	}

}
