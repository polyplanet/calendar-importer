<?php

namespace CalendarImporter\PostType;

use CalendarImporter\Core;

class PostTypes extends Core\Singleton {


	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		add_action( 'init' , array( $this , 'register_post_types' ), 0 );

		add_action( 'save_post', array( $this, 'save_post' ), 20, 3 );

	}


	public function save_post( $post_id, $post, $update ) {
		do_action( 'save_post_type_' . $post->post_type,  $post_id, $post, $update );
	}



	public function register_post_types() {

		$event		= PostTypeEvent::instance();
		$calendar	= TaxonomyCalendar::instance()
			->add_post_type( $event );


		do_action( 'register_post_type_taxonomy', $event->get_slug(), $calendar->get_slug(), false );
		add_filter( 'posts_orderby', array( $this, 'posts_orderby' ), 10, 2 );

	}

	/**
	 *	Order Restaurants alphabetically by default
	 *
	 *	@filter posts_orderby
	 */
	public function posts_orderby( $orderby, $query ) {
		if ( 'restaurant' === $query->get( 'post_type' ) && '' === $query->get( 'orderby' ) ) {
			global $wpdb;
			return "{$wpdb->posts}.post_title ASC";
		}
		return $orderby;
	}


	private function hide_post_type( $post_type , $slug ) {
		global $wp_post_types;
		if ( isset( $wp_post_types[ $post_type ] ) ) {
			$wp_post_types[ $post_type ]->public = false;
			$wp_post_types[ $post_type ]->show_ui = false;
			$wp_post_types[ $post_type ]->show_in_menu = false;
			$wp_post_types[ $post_type ]->show_in_nav_menus = false;
			$wp_post_types[ $post_type ]->show_in_admin_bar = false;
			$wp_post_types[ $post_type ]->can_export = false;
			$wp_post_types[ $post_type ]->has_archive = false;
			$wp_post_types[ $post_type ]->rewrite = false;
			$wp_post_types[ $post_type ]->query_var = false;
			$wp_post_types[ $post_type ]->map_meta_cap = false;
			if ( ! defined( 'DOING_AJAX' ) ) {
				$slug = ( !$slug ) ? 'edit.php?post_type=' . $post_type : $slug;
				remove_menu_page( $slug );
			}
		}
	}


	private function unregister_post_type( $post_type , $slug ) {
		global $wp_post_types;
		if ( isset( $wp_post_types[ $post_type ] ) ) {
			unset( $wp_post_types[ $post_type ] );
			if ( ! defined( 'DOING_AJAX' ) ) {
				$slug = ( !$slug ) ? 'edit.php?post_type=' . $post_type : $slug;
				remove_menu_page( $slug );
			}
		}
	}
}
