<?php


namespace CalendarImporter\Model;

use CalendarImporter\Core;

/**
 *	Persistent Caching
 *
 */
class ModelCache extends Core\Singleton {

	private $prefix = 'pp_cal_';

	protected function __construct() {
		add_option( 'pp_calendar_tags_transient',array());
		add_action( 'deleted_transient', array( $this, 'deleted_transient' ) );
	}

	/**
	 *	@param $key
	 */
	public function set( $key, &$content, $lifetime = 0, $tags = array() ) {
		// wrap around set_transent / get_transient / delete_transient
		// how to implement tagging?

		$transient_key = $this->prefix . $key;

		if ( $result = set_transient( $transient_key, $content, $lifetime ) ) {
			$transient_tags = get_option('pp_calendar_tags_transient');
			foreach ( $tags as $tag ) {
				if ( ! isset( $transient_tags[$tag] ) ) {
					$transient_tags[$tag] = array();
				}
				$transient_tags[$tag][] = $transient_key;
			}
			update_option( 'pp_calendar_tags_transient', $transient_tags );
		}

	}

	/**
	 *	@action deleted_transient
	 */
	public function deleted_transient( $deleted_transient ) {
		$transient_tags = get_option('pp_calendar_tags_transient');
		$clone = $transient_tags + array();
		foreach ( $clone as $tag => $transients ) {
			foreach ( $transients as $i => $transient ) {
				if ( $deleted_transient === $transient ) {
					unset( $transient_tags[$tag][$i] );
					break 2;
				}
			}
		}
		update_option( 'pp_calendar_tags_transient', $transient_tags );
	}

	public function get( $key ) {
		if ( CALENDAR_IMPORTER_DEBUG ) {
			return false;
		}
		$transient_key = $this->prefix . $key;
		$result = get_transient( $transient_key );

		return get_transient( $transient_key );
	}

	/**
	 *	@param string $key_or_tag
	 *	@return int number of deleted transients
	 */
	public function clear( $key_or_tag = null ) {

		remove_action( 'deleted_transient', array( $this, 'deleted_transient' ) );

		$deleted = 0;

		$transient_key = $this->prefix . $key_or_tag;

		if ( ! delete_transient( $transient_key ) ) {
			$transient_tags = get_option('pp_calendar_tags_transient');
			if ( isset( $transient_tags[ $key_or_tag ] ) ) {
				foreach ( $transient_tags[ $key_or_tag ] as $tag_transient ) {
					if ( delete_transient( $tag_transient ) ) {
						$deleted++;
					}
				}
				unset( $transient_tags[ $key_or_tag ] );
				update_option( 'pp_calendar_tags_transient', $transient_tags );
			}
		} else {
			$deleted++;
		}
		add_action( 'deleted_transient', array( $this, 'deleted_transient' ) );
		return $deleted;
	}
	
	/**
	 *	@return int number of deleted transients
	 */
	public function purge( $prefix = null ) {
		global $wpdb;
		if ( is_null( $prefix ) ) {
			$prefix = $this->prefix;
		}
		$affected_rows = $wpdb->query( 
			$wpdb->prepare(
				"DELETE FROM $wpdb->options WHERE option_name LIKE %s", 
				$wpdb->esc_like( "_transient_{$prefix}" ).'%'
			)
		);
		pp_cal_debug( '[CALENDAR_IMPORTER] Purge site cache. # Entries '.$affected_rows );

		return $affected_rows;
	}
	/**
	 *	Upgrade
	 */
	// private function update_db(){
	// 	global $wpdb, $charset_collate;
    //
	// 	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    //
	// 	$sql = "CREATE TABLE $wpdb->pp_cache (
	// 		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	// 		`cache_key` bigint(20) NOT NULL,
	// 		`cache_content` text NOT NULL,
	// 		`cache_tags` datetime NOT NULL,
	// 		`cache_expires` datetime NOT NULL,
	// 		PRIMARY KEY (`id`),
	// 		KEY cache_key (cache_key),
	// 		KEY cache_tags (tags)
	// 	) $charset_collate;";
    //
	// 	// updates DB
	// 	dbDelta( $sql );
	// }

}
