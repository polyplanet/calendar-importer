<?php


namespace CalendarImporter\Model;

use CalendarImporter\Core;

/**
 *	How to Loop:
 *
 *	Loop Events
 *
 *		$schedules = pp_schedule(); // get instance
 *		$schedules->query_events( $args );
 *		while ( $schedules->have_days() ) {
 *	 		$schedules->the_day();
 *	 		$the_day = $schedules->get_day(); // array[] $date
 *			get_template_part('calendar-importer/events');
 * 		}
 *
 *		while ( $schedules->have_dates() ) {
 *	 		$schedules->the_dates();
 *			$the_event = $schedules->get_dates(); // object $date
 *			get_template_part( 'calendar-importer/event', $display, $period );
 * 		}
 */
class ModelSchedules extends Model {

	/**
	 *	@var	string	table name for this model
	 */
	protected $_table = 'pp_schedules';


	/**
	 *	@var	array	Arguments passed to query
	 */
	private $args = array();

	/**
	 *	@var	array	Default Arguments for query
	 */
	private $defaults = array(
		'calendars'			=> '',
		'events'			=> '',
		'dates'				=> '',
		'period'			=> '',

		'from_days'			=> 'today',
		'from_day'			=> '',
		'from_in_days'		=> '',
		'to_days'			=> '',
		'to_day'			=> '',
		'to_in_days'		=> '',

		'from_weeks'		=> 'this_week',
		'from_week'			=> '',
		'from_in_weeks'		=> '',
		'to_weeks'			=> '',
		'to_week'			=> '',
		'to_in_weeks'		=> '',

		'from_months'		=> 'this_month',
		'from_month'		=> '',
		'from_in_months'	=> '',
		'to_months'			=> '',
		'to_month'			=> '',
		'to_in_months'		=> '',

		'template'			=> 'list',
		'tile_size'			=> 'large',
		'list_size'			=> 'normal',

		'distinct-events'	=> '',
		'limit'				=> '',
	);

	/**
	 *	@var	array	queried events
	 */
	private $_dates	= array();


	/**
	 *	@var	object	queried day
	 */
	private $day		= false;

	/**
	 *	@var	array	queried days
	 */
	private $days		= array();



	/**
	 *	@var	object	queried date
	 */
	private $date		= false;

	/**
	 *	@var	array	queried days
	 */
	private $dates		= array();

	/**
	 *	@var	object	queried date
	 */
	private $recurring_date		= false;

	/**
	 *	@var	array	queried days
	 */
	private $recurring_dates		= array();

	/**
	 *	@var	array
	 */
	private $all_dates	= array();


	/**
	 *	@var	array	events cache
	 */
	private $_events		= array();




	/**
	 *	@var	array	queried dates
	 */
	private $schedule		= array();

	/**
	 *	@var	\DateTime	query from ...
	 */
	private $begin		= null;

	/**
	 *	@var	\DateTime	... query to
	 */
	private $end		= null;

	/**
	 *	@var \DateTimeZone
	 */
	private $timezone;

	/**
	 *	@inheritdoc
	 */
	protected function __construct( ) {

		try {
			$this->timezone = new \DateTimeZone( pp_get_timezone_option() );
		} catch ( \Exception $err ) {
			$this->timezone = new \DateTimeZone( date_default_timezone_get() );
		}

		$this->args = wp_parse_args( array(), $this->defaults );

		parent::__construct();

	}

	/**
	 *	@inheritdoc
	 */
	public function activate() {
		// create table
		$this->update_db();
	}

	/**
	 *	@inheritdoc
	 */
	public function upgrade( $new_version, $old_version ) {
		$this->update_db();
	}

	/**
	 *	Magic getter
	 *
	 *	@param	string	$arg
	 *	@return	mixed
	 */
	public function get( $arg ) {
		if ( $arg == 'begin' && $this->begin instanceof \DateTime ) {
			return clone( $this->begin );
		}
		if ( $arg == 'end' && $this->begin instanceof \DateTime ) {
			return clone( $this->end );
		}
		if ( isset($this->args[ $arg ])) {
			if ( in_array( $arg, array('calendars','events','dates','limit') ) ) {
				return $this->sanitize_numeric_array( $this->args[ $arg ] );
			}
			return $this->args[ $arg ];
		}
	}

	/**
	 *	@param array $args
	 *	@return array
	 */
	public function get_pages_args( $args ) {
		$return_args = array();

		$this->prepare_query( $args );

		$diff = date_diff( $this->end, $this->begin );
		$period = $this->get( 'period' );
		$one_second = new \DateInterval( 'P1M' );

		switch ( $period ) {
			case 'day':
				$add = new \DateInterval('P1D');
				$diff_num = $diff->days; /** @var Integer number of days to iterate */
				break;
			case 'week':
			case 'workweek':
				$add = new \DateInterval('P1W');
				$diff_num = ceil( $diff->days / 7 );
				$period = 'week';
				break;
			case 'month':
				$add = new \DateInterval('P1M');
				$diff_num = ceil( $diff->days / 30 );
				break;
		}
		$begin = clone( $this->begin );
		$end = clone( $this->begin );
//		$end->add( $add );
//		$end->sub( $one_second );

		for ( $i = 0; $i < $diff_num; $i++ ) {

			$return_args[] = wp_parse_args( array(
				"from_{$period}s"	=> $period,
				"from_{$period}"	=> $begin->format( \DateTime::W3C ),
				"to_{$period}s"		=> $period,
				"to_{$period}"		=> $end->format( \DateTime::W3C ),
			), $args );

			$begin->add( $add );
			$end->add( $add );
		}

		$this->reset();

		return $return_args;
	}


	public function sanitize_query( $args ) {
		/**
		 *		'calendars'			string		comma seprated list of calandars
		 *		'period'			string		day|week|workweek|month
		 *		'from_days'			string		today|tomorrow|plus_days|day
		 *		'from_day'			string		parsable datetime string
		 *		'from_in_days'		string|int	integer value
		 *		'to_days'			string		today|tomorrow|plus_days|day
		 *		'to_day'			string		parsable datetime string
		 *		'to_in_days'		string|int	integer value
		 *
		 *		'from_weeks'		string		this_week|next_week|plus_weeks|week
		 *		'from_week'			string		parsable datetime string
		 *		'from_in_weeks'		string|int	integer value
		 *		'to_weeks'			string		this_week|next_week|plus_weeks|week
		 *		'to_week'			string		parsable datetime string
		 *		'to_in_weeks'		string|int	integer value
		 *
		 *		'from_months'		string		this_month|next_month|plus_months|month
		 *		'from_month'		string		parsable datetime string
		 *		'from_in_months'	string|int	integer value
		 *		'to_months'			string		this_month|next_month|plus_months|month
		 *		'to_month'			string		parsable datetime string
		 *		'to_in_months'		string|int	integer value
		 *
		 *		'template'			string		list|tiles|calendar
		 *		'tile_size'			string		small|large
		 *		'list_size'			string		normal|compact
		 *		'events'			int			Event Post ID(s)
		 *		'dates'				int			Date ID(s)
		 *		'limit'				int|array	Limit
		 */

		 $args = array_filter( $args, function( $val ) {
			 return ! is_null($val) && $val !== '';
		 } );

		$args = wp_parse_args( $args, [
			'calendars' => [],
			'template' => $this->defaults['template'],
			'list_size' => $this->defaults['list_size'],
			'tile_size' => $this->defaults['tile_size'],

			'from_in_days'   => 0,
			'to_in_days'     => 1,
			'from_in_weeks'  => 0,
			'to_in_weeks'    => 1,
			'from_in_months' => 0,
			'to_in_months'   => 1,
		] );

		

		// sanitize calendars
		$args['calendars'] = $this->sanitize_numeric_array( $args['calendars'] );
		

		// sanitize template
		if ( ! in_array( $args['template'], [ 'list', 'tiles', 'calendar' ] ) ) {
			$args['template'] = $this->defaults['template'];
		}

		// sanitize list size
		if ( ! in_array( $args['list_size'], [ 'normal', 'compact' ] ) ) {
			$args['list_size'] = $this->defaults['list_size'];
		}

		// sanitize tile size
		if ( ! in_array( $args['tile_size'], [ 'small', 'large' ] ) ) {
			$args['tile_size'] = $this->defaults['list_size'];
		}

		return $args;
	}

	/**
	 *	Prepare Query
	 *
	 */
	public function prepare_query( $args ) {

		$args = $this->sanitize_query( $args );

		$this->args = wp_parse_args( $args, $this->defaults );

		if ( ! $this->args['calendars'] && ! $this->args['events'] && ! $this->args['dates'] ) {
			return false;
		}


		/*
		sanitize
		 - from_weeks  // week diff
		 - from_in_weeks // week diff
		 - from_week // week no.
		*/

		switch ( $this->get( 'period' ) ) {
			case 'day':
				$this->begin	= $this->day_to_date( $this->get('from_days'), $this->get('from_in_days'), $this->get('from_day'), false );
				$this->end		= $this->day_to_date( $this->get('to_days'), $this->get('to_in_days'), $this->get('to_day'), true );
				break;
			case 'week':
			case 'workweek':
				$this->begin	= $this->week_to_date( $this->get('from_weeks'), $this->get('from_in_weeks'), $this->get('from_week'), false );
				$this->end		= $this->week_to_date( $this->get('to_weeks'), $this->get('to_in_weeks'), $this->get('to_week'), true );
				break;
			case 'month':
				$this->begin	= $this->month_to_date( $this->get('from_months'), $this->get('from_in_months'), $this->get('from_month'), false );
				$this->end		= $this->month_to_date( $this->get('to_months'), $this->get('to_in_months'), $this->get('to_month'), true );
				break;
			// default:
			// 	$this->begin	= new \DateTime();
			// 	$this->end		= new \DateTime();
			// 	// MUST set begin + end here or die...
			// 	break;
		}
	}

	/**
	 *	Run Query
	 *
	 *	@param	array	$args	array(
	 *		'calendars'			string		comma seprated list of celandars
	 *		'period'			string		day|week|workweek|month
	 *		'from_days'			string		today|tomorrow|plus_days|day
	 *		'from_day'			string		parsable datetime string
	 *		'from_in_days'		string|int	integer value
	 *		'to_days'			string		today|tomorrow|plus_days|day
	 *		'to_day'			string		parsable datetime string
	 *		'to_in_days'		string|int	integer value
	 *
	 *		'from_weeks'		string		this_week|next_week|plus_weeks|week
	 *		'from_week'			string		parsable datetime string
	 *		'from_in_weeks'		string|int	integer value
	 *		'to_weeks'			string		this_week|next_week|plus_weeks|week
	 *		'to_week'			string		parsable datetime string
	 *		'to_in_weeks'		string|int	integer value
	 *
	 *		'from_months'		string		this_month|next_month|plus_months|month
	 *		'from_month'		string		parsable datetime string
	 *		'from_in_months'	string|int	integer value
	 *		'to_months'			string		this_month|next_month|plus_months|month
	 *		'to_month'			string		parsable datetime string
	 *		'to_in_months'		string|int	integer value
	 *
	 *		'template'			string		list|tiles|calendar
	 *		'tile_size'			string		small|large
	 *		'list_size'			string		normal|compact
	 *		'events'			int			Event Post ID(s)
	 *		'dates'				int			Date ID(s)
	 *		'limit'				int|array	Limit
	 *	)
	 *	@return	bool	whether there are results
	 */
	public function query_events( $args ) {

		global $wpdb;

		$this->reset();

		$this->prepare_query( $args );
		// if ( is_null( $this->begin ) ) {
		// 	$this->begin = new \DateTime('today');
		// }

		// build query
		// -----------

		// select calendars
		$where		= array();
		if ( $this->args['calendars'] ) {
			$terms		= $this->get( 'calendars' );
			$format		= implode( ',', array_fill( 0, count($terms),'%d' ) );
			$where[]	= $wpdb->prepare( "term_id IN($format)", $terms );
		} else if ( $this->args['events'] ) {
			$post_ids	= $this->get( 'events' );
			$format		= implode( ',', array_fill( 0, count($post_ids),'%d' ) );
			$where[]	= $wpdb->prepare( "post_id IN($format)", $post_ids );
		} else if ( $this->args['dates'] ) {
			$date_ids	= $this->get( 'dates' );
			$format		= implode( ',', array_fill( 0, count($date_ids),'%d' ) );
			$where[]	= $wpdb->prepare( "id IN($format)", $date_ids );
		}

		if ( ! is_null( $this->begin ) && ! is_null( $this->end ) ) {


			$interval = $this->end->diff($this->begin);

			// maximum 1 year!
			if ( $interval->days > 365 ) {
				$this->end = clone( $this->begin );
				$this->end->add( new \DateInterval( 'P1Y' ) );
			}

			$begin_str = $this->begin->format('Y-m-d H:i:s');
			$end_str = $this->end->format('Y-m-d H:i:s');
			$where[]	= $wpdb->prepare(
				'(
					( NOT all_day AND dtstart >= %s AND dtend < %s)
						OR
						(all_day AND
							(
								( dtstart BETWEEN %s AND %s ) OR
								( DATE_SUB(dtend, INTERVAL 1 SECOND) BETWEEN %s AND %s )
							)
						)
					)',
				$begin_str,
				$end_str,

				$begin_str,
				$end_str,
				$begin_str,
				$end_str

		 	);
		}


		$select = '*';
		$group = '';
		// distinct events
		if ( $this->get('distinct-events') ) {
			$select = 'MIN(id) AS id, post_id, MIN(dtstart) AS dtstart, MIN(dtend) AS dtend, MIN(all_day) AS all_day, MIN(term_id) AS term_id';
			$group = 'GROUP BY post_id';
		}

		$limit = '';
		// distinct events
		if ( $limit_arr = $this->get('limit') ) {
			$limit_len = count( $limit_arr );
			if ( $limit_len > 0 && $limit_len < 3 ) {
				$format	= implode( ',', array_fill( 0, $limit_len,'%d' ) );
				$limit	= $wpdb->prepare( " LIMIT {$format}", $limit_arr );

			}
		}

		$where_str = trim( implode( ' AND ', $where ) );
		if ( ! empty( $where_str ) ) {
			$where_str = " WHERE {$where_str}";
		}
		$sql = "SELECT {$select} FROM $wpdb->pp_schedules "
			. " {$where_str}"
			. " {$group}"
			. " ORDER BY dtstart ASC"
			. " {$limit}";

		// run query
		// -----------

		$schedule = $wpdb->get_results( $sql );

		//$wpdb->flush(); // + ~70000 ! Why?
		if ( 'workweek' === $this->get( 'period' ) ) {
			// filter schedule
			$this->schedule = array();
			foreach ( $schedule as $i => $schedule_item ) {
				$dtstart = new \DateTime( $schedule_item->dtstart );
				if ( intval( $dtstart->format('N') ) < 6 ) { // only mo-fr
					$this->schedule[] = $schedule_item;
				}
			}
		} else {
			$this->schedule = $schedule;
		}


		// parse results
		// -------------
		$this->setup();

		$this->dates = $this->all_dates;
		$this->day = current( $this->days );
		$this->date = current( $this->dates );

		if ( $this->args['events'] ) {
			$this->the_date( true );
		}

		return count( $this->schedule ) > 0;
	}

	/**
	 *	setup 
	 */
	private function setup() {

		// setup days
		$this->days = array();
		$this->all_dates = array();
		$this->recurring_dates = array();

		$this->fill_days();


		foreach ( $this->schedule as $i => $date ) {

			if ( ! $date = $this->setup_date( $date ) ) {
				continue;
			}
			if ( $diff_days = $this->get_date_duration_days( $date ) ) {
				// multiple day event
				$days = array();
				$begin = clone( $date->dtstart_dt );
				$end = clone( $date->dtend_dt );
				$day = clone( $date->dtstart_dt );

				for ( $i=0;$i<=$diff_days-1;$i++ ) {
					$day_end = clone( $day );
					$day_end->add( new \DateInterval( 'P1D' ) );
					if ( $this->get('period') === 'day' ) {
						if ( $day <= $this->begin && $day_end >= $this->end ) {
							$days[] = clone( $day );
						}
					} else {
						if ( $day >= $this->begin && $day_end <= $this->end ) {
							$days[] = clone( $day );
						}

					}
					$day->add( new \DateInterval( 'P1D' ) );
				}
			} else {
				$days = array( clone( $date->dtstart_dt ) );
			}
			foreach ( $days as $day ) {
				$day_key = $day->format( 'Ymd' );
				if ( ! isset( $this->days[$day_key] ) ) {
					$dayobj = (object) array(
						'date'	=> $day,
						'dates'	=> array(),
					);
					$this->days[$day_key] = $dayobj;
				}
				$this->days[$day_key]->dates[] = $date;
			}
			$this->all_dates[] = $date;
		}
	}

	private function get_date_duration_days( $date ) {
		$diff = $date->dtstart_dt->diff( $date->dtend_dt );
		return $diff->days;
	}

	/**
	 *	Setup days structure.
	 *
	 *	@return bool Whether begin and end were set.
	 */
	private function fill_days() {
		if ( is_null( $this->begin ) || is_null( $this->end ) ) {
			return false;
		}
		$curr = clone( $this->begin );
		$end = intval($this->end->format( 'Ymd' ));
		while ( ($day_key = $curr->format( 'Ymd' )) && intval($day_key) < $end ) {

			$this->days[$day_key] = (object) array(
				'date'	=> clone( $curr ),
				'dates'	=> array(),
				'is_first' => false,
				'is_last' => false,
			);

			$curr->add( new \DateInterval( 'P1D' ) );
		}
		$cnt = count( $this->days );
		if ( $cnt ) {
			$keys = array_keys($this->days);
			$this->days[ $keys[0] ]->is_first = true;
			$this->days[ $keys[$cnt-1] ]->is_last = true;
		}
		return true;
	}

	/**
	 *	setup a date
	 *
	 *	@param	object	$date	Single result object from $wpdb
	 *	@return object
	 */
	private function setup_date( $date ) {
		$date->all_day = intval( $date->all_day );
		if ( $event = $this->setup_event( $date->post_id ) ) {
			if ( $date->all_day ) {
				$date->dtstart_dt	= new \DateTime( $date->dtstart, $this->timezone );
				$date->dtend_dt		= new \DateTime( $date->dtend, $this->timezone );
			} else {
				$utc = new \DateTimeZone( 'UTC' );

				$date->dtstart_dt	= new \DateTime( $date->dtstart, $utc );
				$date->dtend_dt		= new \DateTime( $date->dtend, $utc );

				$date->dtstart_dt->setTimezone( $this->timezone );
				$date->dtend_dt->setTimezone( $this->timezone );				
			}
			$date->event		= $event;
			return $date;
		}
	}

	/**
	 *	setup an event, add a date to it
	 *
	 *	@param	int	$post_id
	 *	@param	object	$add_date	Single result object from $wpdb
	 *	@return object
	 */
	private function setup_event( $post_id ) {
		global $wpdb;

		if ( ! isset( $this->_events[ $post_id ] ) ) {


			if ( ( $post = get_post( $post_id ) ) && 'publish' === $post->post_status ) {
				$this->_events[ $post_id ] = $post;
			} else {
				$this->_events[ $post_id ] = null;
			}
		}

		return $this->_events[ $post_id ];
	}

	private function get_recurring_for_post_id( $post_id ) {
		global $wpdb;
		$recurring = array();
		// setup recurring
		$sql = $wpdb->prepare("SELECT * FROM $wpdb->pp_schedules WHERE post_id = %d AND dtstart >= CURDATE() ORDER BY dtstart ASC LIMIT 50", $post_id );
		$dates = $wpdb->get_results( $sql );
		$wpdb->flush(); // 8 byte ... lame!

		foreach ( $dates as $i => $date ) {
			$recurring[] = $this->setup_date( $date );
		}
		return $recurring;
	}


	/**
	 *	Reset all vars
	 */
	public function reset() {
		$this->args = array();
		$this->dates = array();
		$this->recurring_dates = array();
		$this->all_dates = array();
		$this->days = array();
		$this->schedule = array();
		$this->begin = null;
		$this->end = null;
		$this->date = false;
		$this->day = false;
		$this->recurring_date = null;
		$this->_events = array();
	}

	/**
	 *	@return array containing the Schedule objects for the day
	 */
	// public function next_day( ) {
	// 	if ( is_null( $this->_days ) ) {
	// 		$this->setup_days();
	// 	}
	// 	$day = $this->the_day();
	// 	next( $this->_days );
	// 	return $day;
	// }

	/**
	 *	@return bool
	 */
	public function have_days( ) {
		return current( $this->days ) !== false;
	}
	public function reset_days() {
		reset( $this->days );
	}
	/**
	 *	@return array containing the Schedule objects for the day
	 */
	public function the_day() {
		$this->day = current( $this->days );
		$this->dates = $this->day->dates;
		$this->date = current( $this->dates );
		next( $this->days );
	}

	/**
	 *	@return array containing the Schedule objects for the day
	 */
	public function get_the_day() {
		return $this->day;
	}


	/**
	 *	@return bool
	 */
	public function have_dates( $all = false ) {
		if ( $all ) {
			return current( $this->all_dates ) !== false;
		}
		return current( $this->dates ) !== false;
	}
	public function reset_dates( $all = false ) {
		if ( $all ) {
			reset( $this->all_dates );
			return;
		}
		reset( $this->dates );
	}

	/**
	 *	@return array containing the Schedule objects for the day
	 */
	public function the_date( $all = false ) {
		if ( $all ) {
			$this->date = current( $this->all_dates );
			next( $this->all_dates );
		} else {
			$this->date = current( $this->dates );
			next( $this->dates );
		}
		$this->recurring_date = null;
		//*
		if ( $this->date ) {
			$this->recurring_dates = $this->get_recurring_for_post_id( $this->date->event->ID );
		} else {
			$this->recurring_dates = array();			
		}
		/*/
		if ( isset( $this->date->event->dates ) ) {
			$this->recurring_dates = $this->date->event->dates;
		} else {
			$this->recurring_dates = array();
		}
		//*/
	}

	/**
	 *	@return array containing the Schedule objects for the day
	 */
	public function get_the_date( ) {
		return $this->date;
	}




	/**
	 *	@return bool
	 */
	public function have_recurring_dates() {
		return current( $this->recurring_dates ) !== false;
	}
	public function reset_recurring_dates() {
		reset( $this->recurring_dates );
	}
	public function count_recurring_dates() {
		return count( $this->recurring_dates );
	}

	/**
	 *	@return array containing the Schedule objects for the day
	 */
	public function the_recurring_date() {
		$this->recurring_date = current( $this->recurring_dates );
		next( $this->recurring_dates );
	}

	/**
	 *	@return array containing the Schedule objects for the day
	 */
	public function get_the_recurring_date( ) {
		return $this->recurring_date;
	}





/*
function _q() {
	global $arr, $el;
	$arr = array(1,2,3,4,5,6);
	$el = current($arr);
}
function _have() {
	global $arr;
	return current($arr) !== false;
}
function _the() {
	global $arr, $el;
	$el = current($arr);
	next($arr);
}
function _get() {
	global $el;
	return $el;
}
*/






	/**
	 *	Create DateTime object out of shortcode attr values
	 *
	 *	@param	string	$days
	 *	@param	int		$plus_days
	 *	@param	string	$date
	 *	@param	bool 	$return_end
	 *	@return \DataTime
	 */
	private function day_to_date( $days, $plus_days, $date, $return_end = false ) {
		$datetime = null;
		if ( $days == 'tomorrow' ) {
			$datetime = new \DateTime( 'today +1 days' );
		} else if ( $days == 'plus_days' ) {
			$datetime = new \DateTime( 'today +' . $plus_days . ' day');
		} else if ( $days == 'day' ) {
			// make sure we add
			$date = $this->cleanupDateString( $date );
			$datetime = new \DateTime( $date );
		} else {
			$datetime = new \DateTime( 'today' );
		}
		if ( is_null( $datetime ) ) {
			return $datetime;
		}
		$datetime->setTimezone( $this->timezone );
		$datetime->setTime( 0, 0, 0 );
		if ( $return_end ) {
			$datetime->add( new \DateInterval( 'P1D' ) );
		}
		return $datetime;
	}

	/**
	 *	Create DateTime object out of shortcode attr values
	 *$this->week_to_date( $this->get('from_weeks'), $this->get('from_in_weeks'), $this->get('from_week'), false );
	 *	@param	string	$weeks		Attr (from|to)_weeks
	 *	@param	int		$plus_weeks	Attr (from|to)_in_weeks
	 *	@param	string	$week		Attr (from|to)_week number (if $weeks=week)
	 *	@param	bool 	$return_end	WHether to return end data of week
	 *	@return \DataTime
	 */
	private function week_to_date( $weeks, $plus_weeks, $week, $return_end = false ) {
		$datetime = null;
		
		if ( $weeks == 'next_week' ) {
			$datetime = new \DateTime( 'monday this week +2 weeks' );
		} else if ( $weeks == 'plus_weeks' ) {
			$datetime = new \DateTime( 'monday this week +' . $plus_weeks . ' weeks');
		} else if ( $weeks == 'week' ) {
			$week = $this->cleanupDateString( $week );
			$datetime = new \DateTime( $week );
		} else {
			$datetime = new \DateTime( 'monday this week' );
		}
		if ( is_null( $datetime ) ) {
			return $datetime;
		}
		$datetime->setTimezone( $this->timezone );
		$datetime->setTime( 0, 0, 0 );
		if ( $return_end ) {
			$datetime->add( new \DateInterval( 'P1W' ) );
		}
		return $datetime;
	}


	/**
	 *	Create DateTime object out of shortcode attr values
	 *
	 *	@param	string	$months
	 *	@param	int		$plus_months
	 *	@param	string	$month			Start of month at 00:00 UTC
	 *	@param	bool 	$return_end
	 *	@return \DataTime
	 */
	private function month_to_date( $months, $plus_months, $month, $return_end = false ) {

		$datetime = null;

		if ( $months == 'next_month' ) {
			$datetime = new \DateTime('midnight first day of next month');
		} else if ( $months == 'plus_months' ) {
			$datetime = new \DateTime('midnight first day of this month');
			$plus_months = intval( $plus_months );
			$interval = new \DateInterval( sprintf( "P%dM", abs( $plus_months ) ) );
			if ( $plus_months < 0 ) {
				$datetime->sub( $interval );
			} else {
				$datetime->add( $interval );
			}
		} else if ( $months == 'month' ) {
			$month = $this->cleanupDateString( $month );
			$datetime = new \DateTime( $month ); // add day, date, time, whatever?

		} else {
			$datetime = new \DateTime('midnight first day of this month');
		}
		if ( is_null( $datetime ) ) {
			return $datetime;
		}
		$datetime->setTimezone( $this->timezone );
		$datetime->setTime( 0, 0, 0 );
		if ( $return_end ) {
			$datetime->add( new \DateInterval( 'P1M' ) );
		}
		return $datetime;
	}
	/**
	 *	Possibly JS generated string
	 */
	private function cleanupDateString( $date_string ) {
		return preg_replace('/\s(\w+\s+)$/imsu', '', $date_string );
	}

	/**
	 *	Upgrade
	 */
	private function update_db(){
		global $wpdb, $charset_collate;

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

		$sql = "CREATE TABLE $wpdb->pp_schedules (
			`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			`post_id` bigint(20) NOT NULL,
			`term_id` bigint(20) NOT NULL,
			`dtstart` datetime NOT NULL,
			`dtend` datetime NOT NULL,
			`all_day` tinyint(1) NOT NULL,
			PRIMARY KEY (`id`),
			KEY dtstart (dtstart),
			KEY dtend (dtend)
		) $charset_collate;";

		// updates DB
		dbDelta( $sql );
	}
}
