<?php


namespace CalendarImporter\Model;

use CalendarImporter\Core;

abstract class Model extends Core\PluginComponent {

	protected $_table = null;

	protected function __construct() {
		// setup wpdb
		global $wpdb;
		$wpdb->tables[] = $this->table;
		$wpdb->set_blog_id( get_current_blog_id() );

		parent::__construct();
	}

	public function __get( $what ) {
		if ( $what === 'table' ) {
			return $this->_table;
		}
	}

	public function deactivate() {
	}

	public static function uninstall() {
		// drop table
		global $wpdb;
		$tbl = $this->table;
		$wpdb->query("DROP TABLE {$wpdb->$tbl}");

	}

	public function insert( $data, $format = null ) {
		global $wpdb;
		$table = $this->table;
		return $wpdb->insert( $wpdb->$table, $data, $format );
	}

	public function update( $data, $where, $format = null, $where_format = null ) {
		global $wpdb;
		$table = $this->table;
		return $wpdb->update( $wpdb->$table, $data, $where, $format, $where_format );
	}

	public function replace( $data, $format = null ) {
		global $wpdb;
		$table = $this->table;
		return $wpdb->replace( $wpdb->$table, $data, $format );
	}

	public function delete( $where, $where_format = null ) {
		global $wpdb;
		$table = $this->table;
		return $wpdb->delete( $wpdb->$table, $where, $where_format );
	}

	public function sanitize_numeric_array( $value ) {

		if ( is_string( $value ) ) {
			$array = explode( ',', $value );
		} else if ( is_numeric( $value ) ) {
			$array = array( $value );
		} else if ( is_object( $value ) ) {
			$array = get_object_vars( $value );
		} else if ( is_array( $value ) ) {
			$array = $value;
		} else if ( is_null( $value ) || false === $value ) {
			 $array = [];
		} 
		$array = array_map('absint', $array );
		$array = array_filter( $array );
		return $array;
	}

}
