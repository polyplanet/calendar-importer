<?php

namespace CalendarImporter\Compat;

use CalendarImporter\ACF as CoreACF;
use CalendarImporter\ACF\Block;
use CalendarImporter\Core;
use CalendarImporter\PostType;


class ACF extends Core\Singleton {

	/**
	 *	called during plugins loaded
	 */
	protected function __construct() {
		// only in DEV mode!
		CoreACF\I18N::instance();
		CoreACF\JSON::instance();


		add_action( 'acf/init', array( $this, 'acf_init' ) );

	}
	
	/**
	 *	@action acf/init
	 */
	public function acf_init( ) {
		if ( function_exists( 'acf_add_options_sub_page' ) ) {
			// add settings panel
			acf_add_options_sub_page( array(
				'page_title' 	=> __('Calendar Options','calendar-importer'),
				'menu_title' 	=> __('Calendar','calendar-importer'),
				'parent_slug'	=> 'options-general.php',
				'capability'	=> 'administrator',
				'post_id'		=> 'pp_calendar_options',
				'menu_slug'		=> 'acf-options-calendar',
				'autoload'		=> true,
			));
		}
		if ( function_exists( 'acf_register_block_type' ) ) {
			Block\Calendar::instance();
		}
	}



}
