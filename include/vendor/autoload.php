<?php

namespace CalendarImporter\vendor;

function __autoload( $class ) {

	$ds = DIRECTORY_SEPARATOR;

	if ( in_array( $class, array( 'ZCiCalDataNode', 'ZCiCalNode', 'ZCiCal', 'ZDateHelper', 'ZCRecurringDate', 'ZCTimeZoneHelper' ) ) ) {
		$file = CALENDAR_IMPORTER_DIRECTORY . 'include' . $ds . 'vendor' . $ds . 'icalendar' . $ds . 'zapcallib.php';
	} else if ( strpos( $class, 'ICal\\' ) !== false ) {
		$include_path = CALENDAR_IMPORTER_DIRECTORY . 'include' . $ds . 'vendor' . $ds . 'ics-parser' . $ds .'src' . $ds;
		$file = $include_path . str_replace( '\\', $ds, $class ) . '.php';
	} else {
		// not our plugin.
		return;
	}


	if ( file_exists( $file ) ) {
		require_once $file;
	} else {
		throw new \Exception( sprintf( 'Class `%s` could not be loaded. File `%s` not found.', $class, $file ) );
	}
}


spl_autoload_register( 'CalendarImporter\vendor\__autoload' );
