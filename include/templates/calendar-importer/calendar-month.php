<?php

$one_day	= new \DateInterval( 'P1D' );
$begin		= pp_schedule()->get('begin');
$next		= pp_schedule()->get('begin')->add( $one_day );
$end		= clone( pp_schedule()->get('end') );
$end->sub( new DateInterval('PT1S') );
//$begin->format('N');

$current_month = false;
setlocale( LC_TIME, 'de_DE' );

$max = 4;

?>
<table class="pp-calendar-month">
	<thead>
		<tr>
			<th class="pp-calendar-month-title" colspan="7">
				<h3 class="pp-title"><?php
				if ( $begin->format('Ym') == $end->format('Ym') ) {
					printf('%s %s', 
						// long monthname
						esc_html__( $begin->format( 'F' ) ), 
						esc_html( $begin->format( 'Y' ) )
					);
				} else {
					if ( $begin->format('Y') == $end->format('Y') ) {
						// WP monthnames
						printf('%s – %s %s',
							_x( 
								$begin->format( 'M' ),
								sprintf( '%s abbreviation', $begin->format( 'F' ) )
							),
							_x( 
								$end->format( 'M' ),
								sprintf( '%s abbreviation', $end->format( 'F' ) )
							),
							$end->format( 'Y' )
						);
					} else {
						printf('%s %s – %s %s',
							_x( 
								$begin->format( 'M' ),
								sprintf( '%s abbreviation', $begin->format( 'F' ) )
							),
							$begin->format( 'Y' ),
							_x( 
								$end->format( 'M' ),
								sprintf( '%s abbreviation', $end->format( 'F' ) )
							),
							$end->format( 'Y' )
						);
					}
				}

				?></h3>
			</th>
		</tr>
		<tr>
			<th data-weekday="1"><?php esc_html_e( 'Mon' ); ?></th>
			<th data-weekday="2"><?php esc_html_e( 'Tue' ); ?></th>
			<th data-weekday="3"><?php esc_html_e( 'Wed' ); ?></th>
			<th data-weekday="4"><?php esc_html_e( 'Thu' ); ?></th>
			<th data-weekday="5"><?php esc_html_e( 'Fri' ); ?></th>
			<th data-weekday="6"><?php esc_html_e( 'Sat' ); ?></th>
			<th data-weekday="7"><?php esc_html_e( 'Sun' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php


			while ( pp_have_days() ) {
				pp_the_day();

				//get_template_part( 'calendar-importer/calendar', pp_calendar_info( 'period' ) );
				$the_day = pp_get_the_day();

				$current_weekday = intval( $the_day->date->format('N') );
				if ( $the_day->is_first || $current_weekday == 1 ) {
					// monday
					?><tr data-weeknumber="<?php echo intval( $the_day->date->format('W') ); ?>"><?php
				}

				if ( $the_day->is_first ) {
					if ( $current_weekday > 1 ) {
						?>
						<td colspan="<?php echo intval( $current_weekday - 1 ) ?>"></td>
						<?php
					}

				}

				?>
					<td data-weekday="<?php echo intval( $current_weekday ); ?>" data-day="<?php echo $the_day->date->format('j') ?>">
						<div class="pp-month-day pp-more-box" data-more="false">
							<div class="pp-day">
								<?php
								$mon = $the_day->date->format('M');

								esc_html_e( $the_day->date->format( 'd' ) );
								if ( $current_month != $mon ) {
									printf(
										'<span class="pp-mon">%s</span>',
										esc_html__( $the_day->date->format( 'M' ) ) 
									);
									$current_month = $mon;
								}
								?>
							</div>

							<?php

							$i = 0;
							$more_i = 0;

							while ( pp_have_dates() ) {
								pp_the_date();
								$i++;
								// lists up events
								if ( $i == 1+$max ) {
									printf('<div class="pp-more-cut"></div>');
								}
								get_template_part( 'calendar-importer/event-calendar', pp_calendar_info( 'period' ) );
								if ( $i > $max ) {
									$more_i++;
								}
							}
							?>
							<?php
								if ( $more_i ) {
									printf( '<a class="pp-more pp-icon-plus" href="#">%s</a>',
										esc_html( 
											sprintf( 
												_n( '%d more', '%d more', $more_i, 'calendar-importer'), 
												$more_i
											) 
										)
									);
								}
							?>
						</div>
					</td>
				<?php


				if ( $the_day->is_last ) {
					if ( $current_weekday < 7 ) {
						// fill
						?>
							<td colspan="<?php echo intval( 7 - $current_weekday ); ?>"></td>
						<?php
					}
				}

				if ( $the_day->is_last || $current_weekday == 7 ) {
					// sunday
					?>
					</tr>
					<?php
				}

			}

		?>
	</tbody>
</table>
<?php
