<?php

$the_date = pp_get_the_date();

$time_format = get_option( 'time_format' );

if ( ! $the_date->all_day ) {
?>
<div class="pp-time-short">
	<?php
	printf( __('%1$s—%2$s','time from, time until','calendar-importer'),

		sprintf('<time datetime="%s">%s</time>',
			esc_attr( $the_date->dtstart_dt->format( 'c' ) ),
			esc_html( $the_date->dtstart_dt->format( $time_format ) )
		),
		sprintf('<time datetime="%s">%s</time>',
			esc_attr( $the_date->dtend_dt->format( 'c' ) ),
			esc_html( $the_date->dtend_dt->format( $time_format ) )
		)
	);
	?></div>
<?php
}
