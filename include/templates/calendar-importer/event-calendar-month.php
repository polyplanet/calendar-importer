<?php
$the_date = pp_get_the_date();

if ( is_null( $the_date->dtstart_dt ) ||  is_null( $the_date->dtend_dt ) ) {
	trigger_error(sprintf( "Calendar Importer: date is NULL. URL: %s, Blog-ID: %d, Date-ID: %d ", add_query_arg([]), get_current_blog_id(), $the_date->id ), E_USER_WARNING );
	return;
}

$color_key = pp_event_color_key( $the_date );
$event_id = pp_get_unique_event_id( $the_date->event );

$classes = array(
	'pp-event',
	'pp-background-'.$color_key,
);
if ( $the_date->all_day ) {
	$classes[] = 'pp-all-day';
}

$classes = array_map( 'sanitize_html_class', $classes );

?>

<div class="<?php echo implode( ' ', $classes ); ?>" id="<?php echo intval( $event_id ); ?>" data-event-id="<?php echo intval( $the_date->id ); ?>" data-event-open="false">
	<?php
		printf('<a class="pp-event-title" title="%s" href="%s" data-event-show="%s" data-event-id="%d">%s</a>',
			esc_attr( $the_date->event->post_title ),
			esc_url( get_permalink( $the_date->event->ID ) ),
			esc_attr( pp_calendar_info('show_event') ),
			absint( $the_date->id ),
			esc_html( $the_date->event->post_title )
		);
	?>
	<?php if ( ! $the_date->all_day ) { ?>
		<span class="pp-icon-clock">
			<span class="pp-screen-reader-text" title="<?php esc_attr_e('All Day Event','calendar-importer') ?>">
				<?php esc_html_e('All Day Event','calendar-importer') ?>
			</span>
		</span>
	<?php } ?>
</div>
<?php



// pp_do_event_js();


unset($the_date);
unset($color_key);
unset($event_id);
unset($classes);



?>
