<?php

$current = pp_schedule()->get('begin');
$period = pp_calendar_info( 'period' );
$prev_month = '';
$current_month = false;

if ( $period === 'day' ) {
	add_filter( 'pp_calendar_max_all_day_events', '__return_false' );
}

// day and weeks

?>
<div class="pp-calendar-days pp-clearfix">
	<?php


	while ( pp_have_days() ) {

		pp_the_day();
		$day = pp_get_the_day();
		$weekday = $day->date->format('N' );

		if ( 'workweek' === $period && $weekday > 5 ) {
			continue;
		}

		$month = sprintf('%s %s',__( $day->date->format( 'F' ) ), $day->date->format( 'Y' ) );
		$mon = $day->date->format('M');

		// if ( $month != $prev_month ) {
		// 	printf( '<h3 class="pp-title">%s</h3>', $month);
		// 	$prev_month = $month;
		// }
		$is_new_month = $current_month != $mon;
		?>
		<div class="pp-calendar-day" data-weekday="<?php echo absint( $weekday ); ?>">

			<div class="pp-calendar-day-name-short">
				<?php esc_html_e( $day->date->format( 'D' ) ) ?>
			</div>
			<div class="pp-schedule">
				<div class="pp-schedule-title pp-clearfix <?php echo $is_new_month ? ' pp-new-month' : ''; ?>">
					<?php

					if ( $is_new_month ) {
						printf('<span class="pp-mon">%s</span>',
							_x(
								$day->date->format( 'M' ),
								sprintf( '%s abbreviation', $day->date->format( 'F' ) )
							)
						);
						$current_month = $mon;
					}

					?>
					<div class="pp-calendar-day-name-long">
						<?php esc_html_e( $day->date->format( 'l' ) ) ?>
					</div>
					<time class="pp-day" datetime="<?php esc_attr_e( $day->date->format( 'c' ) ) ?>">
						<?php esc_html_e( $day->date->format( 'd' ) ) ?>
					</time>
				</div>

				<?php
					pp_print_schedule( pp_calendar_info( 'period' ) !== 'day' );
				?>
			</div>
		</div>
		<?php
	} // while
	pp_reset_days();

	?>
</div>
<?php

if ( $period === 'day' ) {
	remove_filter( 'pp_calendar_max_all_day_events', '__return_false' );
}
