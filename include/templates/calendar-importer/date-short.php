<?php

$the_date = pp_get_the_date();

$dtstart = clone( $the_date->dtstart_dt );
$dtend = clone( $the_date->dtend_dt );
$dtend2 = clone( $the_date->dtend_dt );
$dtend2->sub( new \DateInterval('PT1S'));
$now = date_create();
$is_multiple = pp_date_is_multiple( $the_date );

if ( $the_date->all_day ) {
	$dtend->sub( new \DateInterval('P1D') );
}

?>
<div class="pp-date-short <?php echo $is_multiple ? ' pp-multiple-days' :''; ?>">
	<?php

	$this_year = $dtstart->format( 'Y' ) === $now->format( 'Y' );
	$this_month = $dtstart->format( 'Ym' ) === $now->format( 'Ym' );
	$this_day = $dtstart->format( 'Ymd' ) === $now->format( 'Ymd' );

	?>
	<div class="pp-date-from <?php echo $this_day ? 'today':''; ?>">
		<span class="year <?php echo $this_year ? 'this-year':''; ?>"><?php
			printf('<time datetime="%s">%s</time>',
				esc_attr( $dtstart->format( 'c' ) ),
				esc_html( pp_date_i18n( 'Y', $dtstart ) )
			);
		?></span>
		<span class="day pp-color-day <?php echo $this_day ? 'this-day':''; ?>"><?php
			printf('<time datetime="%s">%s</time>',
			esc_attr( $dtstart->format( 'c' ) ),
			esc_html( pp_date_i18n( 'd', $dtstart ) )
			);
		?></span>
		<span class="month <?php echo $this_month ? 'this-month':''; ?>"><?php
			printf('<time datetime="%s">%s</time>',
				esc_attr( $dtstart->format( 'c' ) ),
				esc_html( pp_date_i18n( 'M', $dtstart ) )
			);
		?></span>
	</div>
	<?php
	if ( $is_multiple ) {
		?>
		<?php

//		$this_year = $dtend->format( 'Y' ) === $now->format( 'Y' );
		$this_month = $dtend->format( 'Ym' ) === $now->format( 'Ym' );
		$this_day = $dtend->format( 'Ymd' ) === $now->format( 'Ymd' );

		$until_dash = _x('–','until','calendar-importer');

		?>
		<div class="pp-date-to <?php echo $this_day ? 'today':''; ?>">
			<span class="until pp-color-day"><?php esc_html_e( $until_dash ); ?></span>
			<span class="day <?php echo $this_day ? 'this-day':''; ?>"><?php
				printf('<time datetime="%s">%s</time>',
					esc_attr( $dtend->format( 'c' ) ),
					esc_html( pp_date_i18n( 'd', $dtend ) )
				);
			?></span>
			<span class="month <?php echo $this_month ? 'this-month':''; ?>"><?php
				printf('<time datetime="%s">%s</time>',
				esc_attr( $dtend->format( 'c' ) ),
				esc_html( pp_date_i18n( 'M', $dtend ) )
				);
			?></span>
		</div>
		<?php
	}
	?>
</div>
