<?php
$classes = array(
	'pp-events',
	'pp-events-'.pp_calendar_info('template'),
	'pp-colors-'.pp_calendar_info('colors'),
);

if ( 'tiles' === pp_calendar_info('template') ) {
	$classes[] = 'pp-tiles-'.pp_calendar_info( 'tile_size' );
} else if ( 'list' === pp_calendar_info('template') ) {
	$classes[] = 'pp-list-'.pp_calendar_info( 'list_size' );
}

$classes = array_map( 'sanitize_html_class', $classes );

?>
<div class="<?php echo implode( ' ', $classes ) ?>" data-pp-colors="<?php echo pp_calendar_info('colors'); ?>">
	<?php


	while ( pp_have_dates(true) ) {

		pp_the_date(true);

		get_template_part( 'calendar-importer/event', pp_calendar_info('template') );

		// pp_do_event_js();

	}

	?>
</div>
