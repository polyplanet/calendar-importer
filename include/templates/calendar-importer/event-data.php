<?php


$the_date = pp_get_the_date();

if ( is_null( $the_date->dtstart_dt ) ||  is_null( $the_date->dtend_dt ) ) {
	trigger_error(sprintf( "Calendar Importer: date is NULL. URL: %s, Blog-ID: %d, Date-ID: %d ", add_query_arg([]), get_current_blog_id(), $the_date->id ), E_USER_WARNING );
	return;
}

$the_event = $the_date->event;
$event_link = pp_event_link( $the_event );
$address = pp_event_location_address( $the_event );
$is_singular = is_singular('pp_event');
$show_thumbnail = ! $is_singular || get_field('show_thumbnail','pp_calendar_options');

trigger_error(sprintf( "Calendar Importer: data is NULL. URL: %s, Blog-ID: %d ", add_query_arg([]), get_current_blog_id() ) );

?>
<div class="pp-event-inline-content">
	<div class="pp-event-body">
		<?php if ( $show_thumbnail && ( $thumbnail = get_the_post_thumbnail( $the_event, 'full' ) )) { ?>
			<div class="pp-event-thumbnail">
				<?php echo wp_kses_post( $thumbnail ); ?>
			</div>
		<?php } ?>
		<?php if ( ! $is_singular ) { ?>
			<?php
				$event_title = $the_event->post_title;
				$content = apply_filters( 'the_content', $the_event->post_content );
			?>
			<?php if ( $event_title || $content ) { ?>

				<div class="pp-event-content">
					<?php if ( $event_title ) { ?>
						<h3 class="pp-title">
							<?php asc_html_e( $event_title ); ?>
						</h3>
					<?php } ?>
					<?php echo wp_kses_post( $content ); ?>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
	<?php if ( $event_link ) { ?>
		<div class="pp-event-link">
			<span class="pp-icon-share"></span>
			<?php echo wp_kses_post( $event_link ); ?>

		</div>
	<?php } ?>
	<div class="pp-event-info">
		<div class="pp-event-datetime">
			<span class="pp-icon-clock"></span>
			<?php
				if ( $the_date->all_day ) {
					get_template_part( 'calendar-importer/date','long' );
				} else {
					get_template_part( 'calendar-importer/time','long' );
					?>
					<div class="pp-date-long">
						<?php get_template_part( 'calendar-importer/date','long' ); ?>
					</div>
					<?php
				}
			?>
		</div>

		<?php if ( $address ) { ?>
			<div class="pp-event-address">
				<span class="pp-icon-location"></span>
				<?php echo wp_kses_post( $address ); ?>
			</div>
		<?php } ?>


	</div>

	<?php get_template_part('calendar-importer/part','map'); ?>

	<?php get_template_part('calendar-importer/part','recurring'); ?>

	<?php get_template_part('calendar-importer/part','sharing'); ?>
</div>
<?php
// reset vars
$the_date =
$the_event =
$event_link =
$address =
$is_singular =
$show_thumbnail = null;
