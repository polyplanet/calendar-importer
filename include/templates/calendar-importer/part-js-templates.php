<script type="text/html" id="pp-tpl-event-lightbox">
	<div class="pp-event-lightbox">
		<div class="pp-event">
			<div class="pp-box">
				<div class="pp-event-inline-content">
				</div>
			</div>
			<a href="#" class="pp-icon-close pp-close">
				<span class="pp-screen-reader-text"><?php esc_html_e('Close','calendar-importer') ?></span>
			</a>
		</div>
	</div>
</script>

<script type="text/html" id="pp-tpl-event-inline">
	<div class="pp-event">
		<div class="pp-box">
			<div class="pp-event-inline-content">
			</div>
		</div>
		<a href="#" class="pp-icon-close pp-close">
			<span class="pp-screen-reader-text"><?php esc_html_e('Close','calendar-importer') ?></span>
		</a>
	</div>
</script>
