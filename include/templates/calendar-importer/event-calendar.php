<?php
$the_date = pp_get_the_date();

if ( is_null( $the_date->dtstart_dt ) ||  is_null( $the_date->dtend_dt ) ) {
	trigger_error(sprintf( "Calendar Importer: date is NULL. URL: %s, Blog-ID: %d, Date-ID: %d ", add_query_arg([]), get_current_blog_id(), $the_date->id ), E_USER_WARNING );
	return;
}

$color_key = pp_event_color_key( $the_date );
$event_id = pp_get_unique_event_id( $the_date->event );

$classes = array(
	'pp-event',
	'pp-background-'.$color_key,
);
$style = '';

if ( $the_date->all_day ) {
	$classes[] = 'pp-all-day';
} else {
	$a_day = 60 * 60 * 24;
	$minute = intval( $the_date->dtstart_dt->format('i') );
	$duration = round( ( ($the_date->dtend_dt->getTimestamp() % $a_day) - ( $the_date->dtstart_dt->getTimestamp() % $a_day) ) / 60 );
// var_dump($duration);
// var_dump($the_date->dtstart_dt,$the_date->dtend_dt);
	$height = 100 * ( $duration / 60 );
	$offset = 100 * ( $minute / 60 );
	$style = sprintf('top:%d%%;height:%d%%', $offset, $height );

}

$classes = array_map( 'sanitize_html_class', $classes );

printf('<a
	href="%s"
	id="%s"
	class="%s"
	style="%s"
	title="%s"

	data-event-id="%d"
	data-event-show="%s"
	data-event-open="false">',
		esc_url( get_permalink( $the_date->event->ID ) ),
		intval( $event_id ),
		implode( ' ', $classes ),
		$style,
		esc_attr( $the_date->event->post_title ),

		intval( $the_date->id ),
		pp_calendar_info('show_event')
);
printf('<span class="pp-event-title">%s</span>', sanitize_text_field( $the_date->event->post_title ) );

?>
<?php if ( ! $the_date->all_day ) { ?>
	<span class="pp-icon-clock pp-time">
		<?php get_template_part('calendar-importer/time','short'); ?>
	</span>
<?php } ?>
</a>

<?php /*

<div class="<?php echo implode(' ',$classes ); ?>" <?php echo $style; ?> id="<?php echo $event_id; ?>" data-event-id="<?php echo $the_date->id; ?>" data-event-open="false">
	<?php
		printf('<a class="pp-event-title" title="%s" href="%s" data-event-show="%s" data-event-id="%d">%s</a>',
			esc_attr( $the_date->event->post_title ),
			get_permalink( $the_date->event->ID ),
			pp_calendar_info('show_event'),
			$the_date->id,
			$the_date->event->post_title
		);
	?>
	<?php if ( ! $the_date->all_day ) { ?>
		<span class="pp-icon-clock pp-time">
			<?php get_template_part('calendar-importer/time','short'); ?>
		</span>
	<?php } ?>
</div>
<?php

pp_do_event_js();

*/ ?>
