<?php

$the_date = pp_get_the_date();

$dtstart = $the_date->dtstart_dt;
$dtend = clone( $the_date->dtend_dt );

//$dtend->sub( new \DateInterval('P1D') );
$date_format = get_option('date_format');
$is_multiple = pp_date_is_multiple( $the_date );

?>

<div class="pp-date-long">
	<?php
	if ( $is_multiple ) {
		// 12.Okt.2017 - 14.Okt.2017
		$dtend->sub( new \DateInterval('PT1S') );
		printf( _x('%1$s until %2$s','datetime from, datetime to','calendar-importer'),
			sprintf('<time datetime="%s">%s</time>',
				esc_attr( $dtstart->format( 'c' ) ),
				esc_html( pp_date_i18n( $date_format, $dtstart ) )
			),
			sprintf('<time datetime="%s">%s</time>',
				esc_attr( $dtend->format( 'c' ) ),
				esc_html( pp_date_i18n( $date_format, $dtend ) )
			)
		);
	} else {
		// 12.Okt.2017
		//*
		printf('<time datetime="%s">%s</time>',
			esc_attr( $dtstart->format( 'c' ) ),
			esc_html( pp_date_i18n( $date_format, $dtstart ) )
		);
		/*/
		echo ;
		//*/
	}
	?></div>
<?php

// reset vars
$the_date =
$dtstart =
$dtend =
$date_format =
$is_one_day = null;
