<?php

$the_date = pp_get_the_date();

?>
<?php if ( $event_map = pp_event_location_map( $the_date->event ) ) { ?>
	<div class="pp-event-location-map">
		<?php echo $event_map; ?>
	</div>
<?php } ?>
