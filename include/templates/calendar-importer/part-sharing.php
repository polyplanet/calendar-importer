<?php

$the_date = pp_get_the_date();
$sharing_settings = get_field('sharing','pp_calendar_options');

if ( $sharing_settings && count( $sharing_settings ) && isset( $the_date->event ) ) {
	$permalink = get_permalink($the_date->event);

	$replace = array(
		'{url}'	=> get_permalink($the_date->event),
		'{title}'	=> $the_date->event->post_title,
	);

	$sharing_patterns = array(
		'facebook'	=> 'https://www.facebook.com/sharer/sharer.php?u={url}',
		'twitter'	=> 'https://twitter.com/home?status={url}',
		'gplus'		=> 'https://plus.google.com/share?url={url}',
		'linkedin'	=> 'https://www.linkedin.com/shareArticle?mini=true&url={url}&title={title}',
		'email'		=> 'mailto:?&subject={title}&body={url}',
	);
	?>
		<div class="pp-event-sharing">
			<?php
				foreach ( $sharing_settings as $share ) {
					if ( ! isset( $sharing_patterns[$share] ) ) {
						continue;
					}
					$url = str_replace( array_keys( $replace ), array_values( $replace ), $sharing_patterns[ $share ] );
					/* translators: %s sharing service */
					printf(
						'<a target="_blank" rel="noopener noreferrer" href="%s" class="pp-icon-%s"><span class="screen-reader-text">%s</span></a>', 
						esc_url( $url ), // ??? working ???
						esc_html( $share ), 
						sprintf( 
							esc_html__('Share via %s','calendar-importer'), 
							esc_html( $share )
						) 
					);
				}
			?>
		</div>
	<?php

}
