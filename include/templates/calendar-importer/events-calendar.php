<?php
$classes = array(
	'pp-events',
	'pp-events-'.pp_calendar_info('period'),
);

if ( pp_calendar_info('paging') !== 'scroll' ) {
	
}

$classes = array_map( 'sanitize_html_class', $classes );

?>
<div class="<?php echo implode( ' ', $classes ) ?>" data-pp-colors="<?php echo pp_calendar_info('colors'); ?>">
	<?php

	get_template_part( 'calendar-importer/calendar', pp_calendar_info( 'period' ) );

	?>
</div>
