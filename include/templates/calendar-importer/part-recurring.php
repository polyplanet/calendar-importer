<?php

global $pp_recurring_date;
$the_date = pp_get_the_date();


$now = new DateTime();

if ( pp_count_recurring_dates() > 1 ) {

	add_filter('pp_the_date','pp_get_the_recurring_date');

	$more = pp_count_recurring_dates() > 15;

	?>
	<div class="pp-event-recurring <?php echo $more ? 'pp-more-box' : '' ?>" data-more="false">
		<h4 class="pp-title">
			<?php esc_html_e('Upcoming Recurrences','calendar-importer'); ?>
		</h4>
		<div class="pp-clearfix">
			<?php
			while ( pp_have_recurring_dates() ) {
				pp_the_recurring_date();
				$date = pp_get_the_recurring_date();

				if ( intval($date->dtend_dt->format('YmdHM')) <= intval($the_date->dtstart_dt->format('YmdHM')) ) {
				//	continue;
				}

				?>
					<div class="pp-recurring-date" tabindex="1">
						<?php
							get_template_part('calendar-importer/date','short');
						?>
					</div>
				<?php
			}
			if ( $more ) {
				printf( 
					'<a href="#" class="pp-more pp-icon-plus"><span class="pp-screen-reader-text">%s</span></a>', 
					esc_html__('More','calender-importer') 
				);
			}
			?>
		</div>
	</div>
	<?php

	remove_filter('pp_the_date','pp_get_the_recurring_date');

	//remove_filter('the_pp_date','the_pp_recurring_date');
}
