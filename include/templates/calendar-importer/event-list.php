<?php

$the_date = pp_get_the_date();

if ( is_null( $the_date->dtstart_dt ) ||  is_null( $the_date->dtend_dt ) ) {
	trigger_error(sprintf( "Calendar Importer: date is NULL. URL: %s, Blog-ID: %d, Date-ID: %d ", add_query_arg([]), get_current_blog_id(), $the_date->id ), E_USER_WARNING );
	return;
}

$event_link = pp_event_link( $the_date->event );
$address = pp_event_location_address( $the_date->event );
$color_key = pp_event_color_key( $the_date );
$event_id = pp_get_unique_event_id( $the_date->event );//sprintf( 'event-%d-%d', $the_date->id, $the_date->event->ID );

$classes = array(
	'pp-border-' . $color_key,
	'pp-event-list-item',
	'pp-event',
);
if ( $address = pp_event_location_address( $the_date->event ) ) {
	$classes[] = 'pp-event-has-address';
}

$classes = array_map( 'sanitize_html_class', $classes );
?>
<article class="<?php echo implode( ' ', $classes ) ?>" id="<?php esc_attr_e( $event_id ); ?>" data-event-id="<?php echo intval( $the_date->id ); ?>" data-event-open="false">
	<div class="pp-event-head">
		<div class="pp-event-date pp-color-day-<?php echo sanitize_key( $color_key ); ?>">
			<?php get_template_part( 'calendar-importer/date','short' ); ?>
		</div>
		<div class="pp-event-heading">
			<h3 class="pp-event-title pp-title" title="<?php esc_attr_e( $the_date->event->post_title ); ?>"><?php
				printf('<a href="%s" data-event-show="%s" data-event-id="%d">%s</a>',
					esc_url( get_permalink( $the_date->event->ID ) ),
					pp_calendar_info('show_event'),
					intval( $the_date->id ),
					esc_html( $the_date->event->post_title )
				);
			?></h3>
			<div class="pp-event-meta"><?php

				if ( ! $the_date->all_day ) {
					?>
					<div class="pp-event-time pp-icon-clock">
						<?php get_template_part( 'calendar-importer/time', 'short' ); ?>
					</div>
					<?php
				}

				if ( $address ) {
					printf( 
						'<span class="pp-event-address pp-icon-location">%s</span>', 
						wp_kses_post( $address )
					);
				}

				if ( $event_link ) {
					printf( 
						'<span class="pp-event-link pp-icon-share pp-link-color-%s">%s</span>', 
						sanitize_key( $color_key ), 
						wp_kses_post( $event_link )
					);
				}

				?>
			</div>
		</div>
	</div>
</article>
<div class="pp-event-inline" data-event-open="false"></div>
<?php
