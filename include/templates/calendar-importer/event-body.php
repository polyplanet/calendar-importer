<?php


$the_date = pp_get_the_date();
if ( ! $the_date ) {
	trigger_error(sprintf( "Calendar Importer: no Date. URL: %s, Blog-ID: %d ", add_query_arg([]), get_current_blog_id() ), E_USER_WARNING );
	return;	
}
if ( is_null( $the_date->dtstart_dt ) ||  is_null( $the_date->dtend_dt ) ) {
	trigger_error(sprintf( "Calendar Importer: Date is NULL. URL: %s, Blog-ID: %d, Date-ID: %d ", add_query_arg([]), get_current_blog_id(), $the_date->id ), E_USER_WARNING );
	return;
}

$the_event = $the_date->event;
$event_link = pp_event_link( $the_event );
$address = pp_event_location_address( $the_event );
$is_singular = is_singular( 'pp_event' );
$show_thumbnail = ! $is_singular || get_field( 'show_thumbnail', 'pp_calendar_options' );
$event_title = $content = '';

if ( ! $is_singular ) {
	$event_title = esc_html( $the_event->post_title );
	$content = $the_event->post_content;
}

$show_body = $show_thumbnail || (! $is_singular && ($event_title || $content));

?>
<div class="pp-event-inline-content">
	<?php if ( $show_body ) { ?>
		<?php

		$evt_body_classes = array();
		$thumbnail = false;

		if ( $show_thumbnail && ( $thumbnail = get_the_post_thumbnail( $the_event, 'full' ) ) ) {
			$evt_body_classes[] = 'has-thumbnail';
		} else {
			$evt_body_classes[] = 'has-no-thumbnail';
		}
		if ( $event_title ) {
			$evt_body_classes[] = 'has-title';
		} else {
			$evt_body_classes[] = 'has-no-title';
		}
		if ( $content ) {
			$evt_body_classes[] = 'has-content';
		} else {
			$evt_body_classes[] = 'has-no-content';
		}
		$evt_body_classes = array_map( 'sanitize_html_class', $evt_body_classes );

		?>
		<div class="pp-event-body <?php echo implode( ' ', $evt_body_classes ); ?>">
			<?php if ( $thumbnail ) { ?>
				<div class="pp-event-thumbnail">
					<?php echo wp_kses_post( $thumbnail); ?>
				</div>
			<?php } ?>
			<?php if ( $event_title || $content ) { ?>

				<div class="pp-event-content">
					<?php if ( $event_title ) { ?>
						<h3 class="pp-title"><?php
							esc_html_e( $event_title );
						?></h3>
					<?php } ?>
					<?php echo wp_kses( $content, [
						'strong' => [],
						'em' => [],
						'br' => [],
					] ); ?>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<?php if ( $event_link ) { ?>
		<div class="pp-event-link">
			<span class="pp-icon pp-icon-share"></span>
			<?php echo wp_kses_post( $event_link ); ?>
		</div>
	<?php } ?>
	<div class="pp-event-info">
		<div class="pp-event-datetime">
			<span class="pp-icon pp-icon-clock"></span>
			<?php
				if ( ! $the_date->all_day ) {
					get_template_part( 'calendar-importer/time','long' );
				}
				get_template_part( 'calendar-importer/date','long' );
			?>
		</div>

		<?php if ( $address ) { ?>
			<div class="pp-event-address">
				<span class="pp-icon pp-icon-location"></span>
				<?php echo wp_kses_post( $address ); ?>
			</div>
		<?php } ?>


	</div>

	<?php get_template_part('calendar-importer/part','map'); ?>

	<?php get_template_part('calendar-importer/part','recurring'); ?>

	<?php get_template_part('calendar-importer/part','sharing'); ?>
</div>
<?php
// reset vars
$the_date =
$the_event =
$event_link =
$address =
$is_singular =
$show_thumbnail = null;
